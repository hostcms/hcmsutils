<?php
namespace HCMS\Core\Xml\Arr;
//use LSS\Array2XML as ax;
use HCMS\Utils as utl;

/**
 * XML entity
 */
class Entity
{
	protected $_arConverts;
	protected $_rootPath;

	function __construct($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '') {
		$this->_rootPath = $rootPath;

		if($key != '') {
			if($attrkey != '') {
				foreach ($inArray as $arKey=>$arVal){
					if(is_array($attrkey)) {
						foreach ($attrkey as $attr) {
							if(isset($arVal[$attr])) {
								$inArray[$arKey]['@attributes'][$attr] = $arVal[$attr];
							}
						}
					} else {
						$inArray[$arKey]['@attributes'] = array($attrkey => $arVal[$attrkey]);
					}
				}
			}
			$this->_arConverts[$key] = $inArray;
		} else {
			$this->_arConverts = $inArray;
		}
	}

	/**
	 * @return array
	 */
	public function getArConverts()
	{
		return $this->_arConverts;
	}

	public static function createInstance($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '')
	{
		return new self($inArray, $key, $rootPath, $attrkey);
	}

	/**
	 * Build entity XML
	 *
	 * @return string
	 */
	public function getXml()
	{
/*		$xml = str_replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "", ax::createXML($this->_rootPath, $this->_arConverts)->saveXML());*/
		$xml = preg_replace("/<\?xml version=\"1.0\"( encoding=\"UTF-8\")?\?>/ui", "", \Spatie\ArrayToXml\ArrayToXml::convert(
			$this->_arConverts,
			$this->_rootPath,
			false,
			"UTF-8",
			"1.0",
			[]
		)
		);
//		$xml = '<hello>Mike</hello>';

		return $xml;
	}
}