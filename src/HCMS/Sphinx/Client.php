<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 31.05.2015
 * Time: 11:21
 */
namespace HCMS\Sphinx;

use NilPortugues\Sphinx as sphinx;
use HCMS\Utils as utl;

class Client
{
	protected $_connectionHost;
	protected $_connectionPort;
	protected $_connection;
	protected $_index;

	protected $_limits;
	protected $_pageCount;
	protected $_currentPage;

	protected $_matchMode;
	protected $_rankingMode;

	protected $_filters = array();
	protected $_groupBy = array();
	protected $_filterRange = array();
//	protected $_queries = array("");

	protected $_result = array();

	function __construct($index, $pLimits, $pPageCount, $pCurrentPage, $pOptions, $pConnectionHost, $pConnectionPort)
	{
		$this->_connectionHost = $pConnectionHost;
		$this->_connectionPort = $pConnectionPort;
		$this->_index = $index;
		$this->_limits = $pLimits;
		$this->_pageCount = $pPageCount;
		$this->_currentPage = $pCurrentPage;

		$this->_connection = new sphinx\SphinxClient();
		$this->_connection->setServer($this->_connectionHost, $this->_connectionPort);
		$this->_connection->setLimits(($this->_pageCount*$this->_currentPage), $this->_pageCount, $this->_limits);
		$this->_connection->SetMatchMode( isset($pOptions['matchMode']) ? $pOptions['matchMode'] : 6 );
		$this->_connection->SetRankingMode( isset($pOptions['rankingMode']) ? $pOptions['rankingMode'] : 5 );
//		$this->_connection->SetSortMode(
//			isset($pOptions['sortMode']) ? $pOptions['sortMode'] : SPH_SORT_RELEVANCE,
//			isset($pOptions['sortModeField']) ? $pOptions['sortModeField'] : ""
//		);
//		utl::p($pOptions);
	}

	public static function createInstance($index, $limits=10, $pageCount=10, $currentPage=0, $options=array(), $connectionHost='localhost', $connectionPort=9312)
	{
		return new self($index, $limits, $pageCount, $currentPage, $options, $connectionHost, $connectionPort);
	}

	public function getConnection() {
		return $this->_connection;
	}

	public function clearFilters() {
		$this->_filters = array();
		return $this;
	}

	public function addFilter($filter) {
		if(is_array($filter) && !array_key_exists(key($filter), $this->_filters) ) {
			$this->_filters[key($filter)] = $filter[key($filter)];
		}
		return $this;
	}

	public function addFilters($arFilter) {
		if(is_array($arFilter) ) {
			foreach($arFilter as $filter) {
				$this->addFilter($filter);
			}
		}
		return $this;
	}

	public function clearGroupBy() {
		$this->_filters = array();
		return $this;
	}

	public function addGroupBy($groupBy) {
		if(is_array($groupBy) && !array_key_exists(key($groupBy), $this->_groupBy) ) {
			$this->_groupBy[key($groupBy)] = $groupBy[key($groupBy)];
		}
		return $this;
	}

	public function addGroupByes($arGroupBy) {
		if(is_array($arGroupBy) ) {
			foreach($arGroupBy as $groupBy) {
				$this->addGroupBy($groupBy);
			}
		}
		return $this;
	}

	public function addFilterRange($attr, $min, $max) {
		if($attr !=''&& $min <= $max) {
			$this->_filterRange['attr'] = $attr;
			$this->_filterRange['min'] = $min;
			$this->_filterRange['max'] = $max;
		}elseif($attr !=''&& $max < $min){
			$this->_filterRange['attr'] = $attr;
			$this->_filterRange['min'] = $max;
			$this->_filterRange['max'] = $min;
		}
		return $this;
	}

//	public function clearQueries() {
//		$this->_queries = array("");
//		return $this;
//	}
//
//	public function addQuery($query) {
//		$this->_queries[] = $query;
//		return $this;
//	}
	public function execute($query) {
		if( count($this->_filters)>0 ) {
			foreach($this->_filters as $filterKey => $filter) {
				$this->_connection
					->setFilter($filterKey, $filter);
			}
		}
		if( count($this->_groupBy)>0 ) {
			foreach($this->_groupBy as $gbKey => $groupBy) {
				$this->_connection
					->setGroupBy($gbKey, $groupBy);
			}
		}
		if( count($this->_filterRange)>0 ) {
			$this->_connection
				->setFilterRange($this->_filterRange['attr'], $this->_filterRange['min'], $this->_filterRange['max']);
		}

		$result = $this->_connection->query($query, $this->_index);
		return $result;
	}
}