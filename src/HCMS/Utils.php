<?php

namespace HCMS;

use Core_Entity;
use Core_File;
use Core_Image;
use Core_QueryBuilder;
use Core_Str;
use Core_Mail;
use HCMS\Core\Xml\Arr\Entity;
use Shop_Cart_Controller;

class Utils {

    const MAX_COUNTER_RETRYES = 5;

	private static function colorize($text, $status) {
		$out = "";
		switch($status) {
			case "S":
				$out = "[32m"; //Green color
				break;
			case "E":
				$out = "[31m"; //Red color
				break;
			case "W":
				$out = "[33m"; //Yellow color
				break;
			case "N":
				$out = "[36m"; //Blue color
				break;
//			default:
//				throw new Exception("Invalid status: " . $status);
		}
		return ($out!='' ? chr(27)."$out" : "") . "$text" . ($out!='' ? chr(27) . "[0m" : "");
	}

	/**
	 * Функция выводит print_r аргумента-объекта в блоке <pre> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function p($object, $title='', $lastCaret=true, $showHeader=TRUE, $status='NONE')
	{
		echo isset($_SERVER['SERVER_NAME']) ? "<pre style='text-align: left;'>" : ($showHeader ? "\n" : "");
		if($showHeader) {
			echo str_pad((strlen($title)>0 ? ' ' : '').$title.(strlen($title)>0 ? ' ' : ''), 100, '=', STR_PAD_BOTH).($lastCaret && !$showHeader ? "\n" : "");
		}
		echo ($showHeader ? "\n" : "");
		print_r( $object );
//		print_r( isset($_SERVER['SERVER_NAME']) ? $object : self::colorize($object, $status));
		echo isset($_SERVER['SERVER_NAME']) ? "</pre>\n" : "\n";
	}

	/**
	 * Функция выводит var_dump аргумента-объекта в блоке <pre> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function v($object, $title='', $lastCaret=true)
	{
		echo isset($_SERVER['SERVER_NAME']) ? "<pre style='text-align: left;'>" : "\n";
		echo str_pad((strlen($title)>0 ? ' ' : '').$title.(strlen($title)>0 ? ' ' : ''), 100, '=', STR_PAD_BOTH).($lastCaret ? "\n" : "");
		var_dump($object);
		echo isset($_SERVER['SERVER_NAME']) ? "</pre>\n" : "\n";
	}

	/**
	 * Функция выводит print_r аргумента-объекта в блоке <textarea> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function tp($object)
	{
		echo "<textarea style='width: 100%; height: 400px; overflow: auto; text-align: left'>";
		print_r($object);
		echo "</textarea>";
	}

	/**
	 * Функция выводит var_dump аргумента-объекта в блоке <textarea> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function tv($object)
	{
		echo "<textarea style='width: 100%; height: 400px; text-align: left'>";
		var_dump($object);
		echo "</textarea>";
	}

	/**
	 * Функция обрезает строку $string на заданное количество символов $limit до ближайшего пробела после $limit
	 *
	 * @param $string
	 * @param $limit
	 */
	public static function crop_str($string, $limit)
	{
		if (strlen($string) >= $limit ) {
			$substring_limited = substr($string,0, $limit);
			return substr($substring_limited, 0, strrpos($substring_limited, ' ' ));
		} else {
			//Если количество символов строки меньше чем задано, то просто возращаем оригинал
			return $string;
		}
	}

	/**
	 * Функция удаляет в строке $string пробелы в начале и конце строки
	 *
	 * @param $string
	 */
	public static function mb_trim( $string )
	{
		$string = preg_replace( "/(^\s+)|(\s+$)/us", "", $string );

		return $string;
	}

	/**
	 * Функция проверяет папку по пути $pathToFolder на существование, и при необходимости создает данную папку по
	 * указанному пути с правами $rules
	 *
	 * @param $string
	 * @param $rules
	 */
	public static function checkFolder($pathToFolder, $rules=0755) {
		if (!file_exists($pathToFolder)) {
			mkdir($pathToFolder, 0755, true);
		}
		return $pathToFolder;
	}

	public static function array_insert(&$array,$element,$position=null) {
		if (count($array) == 0) {
			$array[] = $element;
		}
		elseif (is_numeric($position) && $position < 0) {
			if((count($array)+position) < 0) {
				$array = array_insert($array,$element,0);
			}
			else {
				$array[count($array)+$position] = $element;
			}
		}
		elseif (is_numeric($position) && isset($array[$position])) {
			$part1 = array_slice($array,0,$position,true);
			$part2 = array_slice($array,$position,null,true);
			$array = array_merge($part1,array($position=>$element),$part2);
			foreach($array as $key=>$item) {
				if (is_null($item)) {
					unset($array[$key]);
				}
			}
		}
		elseif (is_null($position)) {
			$array[] = $element;
		}
		elseif (!isset($array[$position])) {
			$array[$position] = $element;
		}
		$array = array_merge($array);
		return $array;
	}

	/**
	 * Функция удаляет папку с файлами внутри нее
	 *
	 * @param $string
	 */
	public static function deleteFolderWithFiles($dirPath) {
		if (! is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		$files == '' && $files = array();
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}

	/**
	 * Функция получает все родительские группы от $gid и записывает их в переменную $groups
	 * которую необходимо передавать в функцию при вызове функции
	 *
	 * @param $int
	 * @param &$array
	 */
	public static function getAllParenGroups($gid, &$groups, $model = 'Shop_Group')
	{
		$group = Core_Entity::factory($model, $gid);
		$groups[] = $group;
		$parent = $group->getParent();
		if ($parent) {
			self::getAllParenGroups($parent->id, $groups,$model);
		} else {
			return false;
		}
	}

	/**
	 * Функция преобразует массив объектов ORM в массив, содержащий массивы, полученные методом toArray() ORM модели
	 *
	 * @param $array
	 */
	public static function setArrayOfObjectsToArray($arrayOfObjects, $fields=[]){
		$oArray = array();
		if(count($arrayOfObjects)>0) {
			$arrayOfObjects = array_values($arrayOfObjects);
			$entityColumns = [];
			if($arrayOfObjects[0] instanceof Core_Entity) {
				$entityColumns = $arrayOfObjects[0]->getTableColumns();
			}
			foreach($arrayOfObjects as $key=>$object) {
				if(is_array($fields) && count($fields)==0) {
					$oArray[$key] = $object->toArray();
				} elseif (count($fields)>0) {
					$fieldObject = [];
					foreach ($fields as $fieldKey => $field) {
						if(array_key_exists($field, $entityColumns)) {
							$fieldObject[$field] = $object->$field;
						}
					}
					$oArray[$key] = $fieldObject;
				}
			}
		}
		return $oArray;
	}

	/**
	 * Функция преобразует массив объектов ORM в массив, содержащий массивы, полученные методом toArray() ORM модели
	 *
	 * @param $array
	 * @param $string
	 */
	public static function getArrayKeyValuesFromArrays($iArray, $key='id') {
		$oArray = array();
		foreach($iArray as $keyIndex => $iValue ) {
			if(is_array($iValue)) {
				$oArray[$iValue[$key]][] = $iValue;
			} elseif (is_object($iValue)) {
				$oArray[$iValue->$key][] = $iValue;
			}
		}
		return $oArray;
	}

	/**
	 * Функция преобразует массив объектов ORM в массив, содержащий массивы, полученные методом toArray() ORM модели
	 *
	 * @param $array
	 * @param $string
	 */
	public static function getArrayAssocValuesFromArrays($iArray, $key='item', $addKeyIndex=false) {
		$oArray = array();
		foreach($iArray as $keyIndex => $iValue ) {
			$oArray[$key.($addKeyIndex ? $keyIndex : '')][] = $iValue;
		}
		return $oArray;
	}
	/**
	 * Функция получает массив объектов/массивов по модели, указанной в аргументе
	 *
	 * @param $string
	 * @param $array
	 * @param $string
	 * @param $boolean
	 */
	public static function getModelItemsByArrayIds($modelName, $ids, $field="id", $toArray=FALSE, $getField='', $getFieldAsArray=FALSE, $addFieldWhere=array(), $fields=array(), $orderBy=array(), $deletedInclude=false) {
		(!is_array($ids) && $ids>0 ) && $ids = array($ids);
		(!is_array($ids) || (is_array($ids) && count($ids)==0)) && $ids=array(-1);
		$iModel = Core_Entity::factory($modelName);
		$iModel->queryBuilder()
			->where($field, 'IN', $ids);
		if(is_array($addFieldWhere) && isset($addFieldWhere['field']) && isset($addFieldWhere['operation']) && isset($addFieldWhere['value'])) {
			$iModel->queryBuilder()
				->where($addFieldWhere['field'], $addFieldWhere['operation'], $addFieldWhere['value'])
			;
		} elseif (is_array($addFieldWhere) && count($addFieldWhere)) {
			foreach($addFieldWhere as $fw) {
				if(is_array($fw) && isset($fw['field']) && isset($fw['operation']) && isset($fw['value'])) {
					$iModel->queryBuilder()
						->where($fw['field'], $fw['operation'], $fw['value'])
					;
				}
			}
		}
		if(is_array($orderBy) && count($orderBy)) {
			foreach($orderBy as $obKey => $ob) {
			$iModel->queryBuilder()
				->orderBy($obKey, $ob)
			;
			}
		}

		if($deletedInclude) {
			$iModel->setMarksDeleted(NULL);
		}
		$iModelValues = $iModel->findAll(FALSE);
		if($toArray===true) {
			$iArrays = self::setArrayOfObjectsToArray($iModelValues);
			if($getField != '') {
				$iArraysField = self::getArrayKeyValuesFromArrays($iArrays, $getField);
				if($getFieldAsArray===TRUE) {
					return array_keys($iArraysField);
				}
				return $iArraysField;
			}
			return $iArrays;
		} else {
			if($getField != '') {
				$iArraysField = self::getArrayKeyValuesFromArrays($iModelValues, $getField);
				return $iArraysField;
			}

		}
		return $iModelValues;
	}

	/**
	 * Функция добавляет изображение к элементу или группе моделей
	 *   Shop_Group_Model
	 *   Shop_Item_Model
	 *
	 * @param $Shop_Group_Model, $Shop_Item_Model
	 * @param $sourceFile
	 * @param $uploadFolder
	 */
	public static function setShopImage($entity, $sourceFile, $currentShop, $destinationFolder='', $uploadFolder='', $ext='', $realFilename='', $saveOriginalFolder='') {
		$uploadFolder == '' && $uploadFolder = CMS_FOLDER . 'upload' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
		$error = array(
			'status' => FALSE,
			'message' => 'Неизвестная ошибка',
		);
		$folderAddDir = '';
		$entityClass = get_class($entity);
		//-- Разрешенные типы изображений для конвертации --------------------------------------------------------------
		$imageTypes = array(
			IMAGETYPE_JPEG => 'jpg',
			IMAGETYPE_PNG => 'png',
			IMAGETYPE_GIF => 'gif',
		);
		//-- Проверка на существование файла ---------------------------------------------------------------------------
		if( preg_match('/^http(s)?:\/\/.*$/', $sourceFile) > 0 ) {
			$tmpDirName = sys_get_temp_dir()."/".microtime(TRUE)."/";
			$tmpFileName = basename($sourceFile);
//			self::p($tmpDirName.$tmpFileName);
			if( ($fileContents = file_get_contents($sourceFile)) !== FALSE ) {
				self::checkFolder($tmpDirName);
				if(file_put_contents($tmpDirName.$tmpFileName, $fileContents)!==FALSE) {
					$sourceFile = $tmpDirName.$tmpFileName;
				}
			}
		}
//		self::p($sourceFile);
		if (is_string($sourceFile) && $sourceFile != '' && is_file($sourceFile)) {
			!defined('CHMOD_FILE') && define('CHMOD_FILE', octdec(644));
			!defined('CHMOD') && define('CHMOD', octdec(777));

			if (is_object($entity)
				&& ($entityClass == 'Shop_Item_Model'
					|| $entityClass == 'Shop_Group_Model'
					|| $entityClass == 'Property_Value_File_Model'
					|| $entityClass == 'Informationsystem_Item_Model'
				)
			) {
				$modelName = strtolower($entityClass);
				$processing = false;
				if (filesize($sourceFile) > 11) {
					$imageType = exif_imagetype($sourceFile);
					try {
						if(!is_null($imageType) && $imageType!==FALSE) {
							$processing = array_key_exists($imageType, $imageTypes);
							if($processing) {
								$picsize = Core_Image::instance()->getImageSize($sourceFile);
								$processing = isset($picsize['width'])&&isset($picsize['height'])&&(($picsize['width'])*1>0)&&(($picsize['height'])*1>0);
							}
						}
					} catch (Exception $exc) {
						$processing = false;
					}
				}
				$fileInfo = pathinfo($sourceFile);
				$sFileExt = $ext!='' ? $ext : (isset($imageTypes[$imageType]) ? $imageTypes[$imageType] : '');
				$sFileName = $fileInfo['filename'];
				$sFileDir = $fileInfo['dirname'];
				$uploadFolder = "{$uploadFolder}{$modelName}_{$currentShop->id}" . DIRECTORY_SEPARATOR . "{$entity->id}" . DIRECTORY_SEPARATOR;
				self::checkFolder($uploadFolder);
				$srcFullFile = $uploadFolder . $entity->id . '.' . $sFileExt;
				$tgtLargeFile = $uploadFolder . $entity->id . '_large.' . $sFileExt;
				$tgtSmallFile = $uploadFolder . $entity->id . '_small.' . $sFileExt;
				copy($sourceFile, $srcFullFile);
				$aPicturesParam = array();
				$aPicturesParam['large_image_source'] = $sourceFile;
				$aPicturesParam['large_image_name'] = $entity->id . '.' . $sFileExt;
				$aPicturesParam['create_small_image_from_large'] = FALSE;
				$aPicturesParam['small_image_target'] = $tgtSmallFile;
				$aPicturesParam['large_image_target'] = $tgtLargeFile;
				if ($processing) {

					$aPicturesParam['large_image_isset'] = TRUE;
					$aPicturesParam['watermark_file_path'] = $currentShop->getWatermarkFilePath();
					$aPicturesParam['watermark_position_x'] = $currentShop->watermark_default_position_x;
					$aPicturesParam['create_small_image_from_large'] = TRUE;
					$aPicturesParam['watermark_position_y'] = $currentShop->watermark_default_position_y;
					$aPicturesParam['large_image_preserve_aspect_ratio'] = $currentShop->preserve_aspect_ratio;
					$aPicturesParam['small_image_max_width'] = $currentShop->image_small_max_width;
					$aPicturesParam['small_image_max_height'] = $currentShop->image_small_max_height;
					$aPicturesParam['small_image_watermark'] = $currentShop->watermark_default_use_small_image;
					$aPicturesParam['small_image_preserve_aspect_ratio'] = $aPicturesParam['large_image_preserve_aspect_ratio'];
					$aPicturesParam['large_image_max_width'] = $currentShop->image_large_max_width;
					$aPicturesParam['large_image_max_height'] = $currentShop->image_large_max_height;
					$aPicturesParam['large_image_watermark'] = $currentShop->watermark_default_use_large_image;
				}
//				self::p($aPicturesParam);
				try {
					$error['result'] = Core_File::adminUpload($aPicturesParam);
//-- TODO - доделать удаление файлов из /upload/temp/
//					unlink($tgtLargeFile);
//					unlink($tgtSmallFile);
				} catch (Exception $exc) {
					$error = array(
						'message' => 'Ошибка обработки файла',
						'result' => array(
							'large_image' => FALSE,
							'small_image' => FALSE,
						)
					);
					return $error;
				}
//				self::p($error['result']);
				$folderAddDir = strtolower($entityClass).'/';
				switch (true) {
					case $entityClass == 'Informationsystem_Item_Model':
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'informationsystem_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setLargeImageSizes();
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_informationsystem_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setSmallImageSizes();
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entityClass == 'Shop_Item_Model':
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'shop_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setLargeImageSizes();
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_shop_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setSmallImageSizes();
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entityClass == 'Shop_Group_Model':
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'shop_groups_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setLargeImageSizes();
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_shop_groups_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setSmallImageSizes();
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entityClass == 'Property_Value_File_Model':
//						self::p($error);
						if ($destinationFolder == '') {
							$error['message'] = "Параметр DestinationFolder не задан";
							return $error;
						}
						$largeFileName = "shop_property_file_{$entity->property_id}_{$entity->id}.{$sFileExt}";
						$smallFileName = "small_" . $largeFileName;
//						utl::p($aPicturesParam['large_image_target']);
						if ($error['result']['large_image']) {
							if(file_exists($aPicturesParam['large_image_target'])) {
								Core_File::upload($aPicturesParam['large_image_target'], $destinationFolder . $largeFileName);
							} else {
								if(file_exists($newFileName = str_replace('_large', '', $aPicturesParam['large_image_target']))) {
									Core_File::upload($newFileName, $destinationFolder . $largeFileName);
								}
							}
						}
						if ($error['result']['small_image']) {
							Core_File::upload($aPicturesParam['small_image_target'], $destinationFolder . $smallFileName);
						}
						$entity->file = $largeFileName;
						$entity->file_name = ($realFilename!='') ? $realFilename : $fileInfo['basename'];
						$entity->file_small = $smallFileName;
						$entity->file_small_name = $fileInfo['basename'];
						$entity->save();
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
				}

				if($saveOriginalFolder!='' && file_exists($saveOriginalFolder) && is_dir($saveOriginalFolder)) {
					$saveOriginalFolder = self::checkFolder($saveOriginalFolder.$folderAddDir);
					$fileName = basename($sourceFile);
					copy($sourceFile, $saveOriginalFolder.$entity->id.'_'.$fileName);
				}
				$error['status'] = TRUE;
				$error['message'] = 'OK';
			} else {
				$error['message'] = "Параметр shopItem не является объектом Shop_Item_Model";
			}
		} else {
			$error['message'] = "Файл не существует";
		}
		return $error;
	}

	public static function setPropertyItemPathByItemID($propertyId, $entityId, $value) {
		$propertyModel = Core_Entity::factory('Property', $propertyId);
		if(!is_null($propertyModel->type)) {
			$propertyValues = $propertyModel->getValues($entityId);
			if(count($propertyValues)==0) {
				$propertyValues[] = $propertyModel->createNewValue($entityId);
			}
			if(count($propertyValues)==1) {
				$propertyValues[0]->value = $value;
				$propertyValues[0]->save();
			}
		}
	}

	public static function getShopItemPathByItemID($itemId, $Shop) {
		return $Shop->getPath() . '/' . Core_File::getNestingDirPath($itemId, $Shop->Site->nesting_level) . '/item_' . $itemId . '/';
	}

	public static function compareFiles($path1, $path2) {
		return md5_file($path1) == md5_file($path2);
	}

	public static function saveArrayToCSV($filename, $array, $delimiter=';', $mode = 'rw') {
		if(count($array)>0) {
			// Open the output stream
			$fh = fopen('php://output', 'w');
			ob_start();
			if (!empty($array)) {
				foreach ($array as $item) {
					fputcsv($fh, $item, $delimiter);
				}
			}
			$string = ob_get_clean();

			if($filename===FALSE || $filename=='') {
				echo $string;
			} else {
				if($mode == 'a'){
					file_put_contents($filename, $string, FILE_APPEND);
				}else{
					file_put_contents($filename, $string);
				}
			}
		}
	}

	public static function getShopItemByIDs($IDs = array(-1), $shopId = 3)
	{
		(is_null($IDs) || (is_array($IDs) && count($IDs)==0)) && $IDs = array(-1);
		$mShopItems = Core_Entity::factory('Shop_Item');
		$mShopItems
			->queryBuilder()
			->where('shop_id', '=', $shopId)
			->where('id', 'IN', $IDs)
			->where('deleted', '=', 0)
			->where('active', '=', 1)
		;
		$oShopItems = $mShopItems->findAll();
		return $oShopItems;
	}

	public static function getLocationDataForAlias($alias = '')
	{
		$mShopCountryLocations = Core_Entity::factory('shop_country_location');
		$mShopCountryLocations
			->queryBuilder()
			->join(array('site_aliases', 'sa'), 'sa.id', '=', 'shop_country_locations.alias_id')
			->where('sa.name', '=', '*.'. $alias)
			->where('sa.deleted', '=', 0)
			->where('sa.redirect', '=', 0)
		;
		$oShopCountryLocations = $mShopCountryLocations->findAll();
		return $oShopCountryLocations;
	}

	public static function getAliasForLocastion($location_id = 0)
	{
		$select = Core_QueryBuilder::select()
			->from(array('site_aliases','sa'))
			// ->columns('*')
			->where('sa.id', '=', $location_id);
		$select = $select
			->execute()
			->asAssoc()
			->result();
		if (isset($select[0])) {
			return substr($select[0]['name'],2);
		}
		return '';
	}

	public static function getGroupItemByIDs($IDs = array(-1), $shopId = 3)
{
	(is_null($IDs) || (is_array($IDs) && count($IDs)==0)) && $IDs = array(-1);
	$mShopGroups = Core_Entity::factory('Shop_Group');
	$mShopGroups
		->queryBuilder()
		->where('shop_id', '=', $shopId)
		->where('id', 'IN', $IDs)
		->where('deleted', '=', 0)
		->where('active', '=', 1)
	;
	$oShopGroups = $mShopGroups->findAll();
	return $oShopGroups;
}
/*
	public static function getSiteusersItemByIDs($IDs = array(-1), $siteId = 2)
	{
		(is_null($IDs) || (is_array($IDs) && count($IDs)==0)) && $IDs = array(-1);
		$mSiteUsers = Core_Entity::factory('SiteUser');
		$mSiteUsers
			->queryBuilder()
			->where('site_id', '=', $siteId)
			->where('id', 'IN', $IDs)
			->where('deleted', '=', 0)
			->where('active', '=', 1)
		;
		$oShopGroups = $mShopGroups->findAll();
		return $oShopGroups;
	}
*/
	public static function getArrayValuesFromArrays($iArray, $key='id') {
		$oArray = array();
		foreach($iArray as $iValue ) {
			if($iValue instanceof Core_Entity) {
				$iValue = $iValue->toArray();
			}
			$oArray[] = (is_numeric($iValue[$key]) ? $iValue[$key]*1 : $iValue[$key]);
		}
		return $oArray;
	}

	public static function updateColumnTable($column, $table, $value)
	{
		$update = Core_QueryBuilder::update($table)
			->columns(array($column => $value))
			->where('shop_id', '=', 3)
			->execute();
		return $update;
	}

	public static function getPropertyItemByPropertyId($entityId, $propertyId, $type = 'ints', $field = '', $keyField = '')
	{
		$select = Core_QueryBuilder::select()
			->from('property_value_' . $type)
			// ->columns('*')
			->where('entity_id', '=', $entityId);
		if(is_string($propertyId)) {
			$select
				->join(array('properties', 'p'), 'p.id', '=', 'property_id')
				->where('p.tag_name', '=', $propertyId)
			;
		} else {
			$select
				->where('property_id', (is_array($propertyId) ? 'IN' : '='), $propertyId);
		}
		$select = $select
			->execute()
			->asAssoc()
			->result();
		if (isset($select[0])) {
			if(is_array($propertyId)) {
				$oArray = array();
				if ($field === '') {
					foreach($select as $sItem) {
						if($keyField=='') {
							$oArray[] = $sItem;
						} else {
							$oArray[$sItem[$keyField]] = $sItem;
						}
					}
				} else {
					foreach($select as $sItem) {
						if($keyField=='') {
							$oArray[] = $sItem[$field];
						} else {
							$oArray[$sItem[$keyField]] = $sItem[$field];
						}
					}
				}
				return $oArray;
			} else {
				if ($field === '') {
					return $select[0];
				} else {
					if (isset($select[0][$field])) {
						return $select[0][$field];
					} else {
						return NULL;
					}
				}
			}
		}
		return NULL;
	}

	public static function getGroupsByTwoLevels()
	{
		$rq2 = Core_QueryBuilder::select('id')
			->from('shop_groups')
			->where('parent_id', '=', 0);

		$rq = Core_QueryBuilder::select('id')
			->from('shop_groups')
			->where('parent_id', '=', 0)
			->setOr()
			->where('parent_id', 'IN', $rq2);
		$selectTwoLevels = $rq
			->execute()
			->asAssoc()
			->result();
		$groups_id = array();
		if (isset($selectTwoLevels)) {
			foreach ($selectTwoLevels as $key => $data) {
				$groups_id[] = $data['id'];
			}
			return $groups_id;
		}
		return NULL;
	}

    public static function generateMegaMarkingCode($shopGroupId, $currentValue=0, $counterRetryes=0, $currentShopId=5) {
        if(strlen($currentValue)==15) {
            $mShopItem = Core_Entity::factory('Shop_Item');
            $foundedItem = $mShopItem->getByMarking($currentValue);
            if( is_null($foundedItem) ) {
                return $currentValue;
            }
        }
        if($counterRetryes > self::MAX_COUNTER_RETRYES) {
            return $currentValue;
        }

        if ($shopGroupId > 0) {
            mt_srand(make_seed());
            $randval = mt_rand();
            $randval = strval($randval);
            if (strlen($randval) > 9) {
                $randval = substr($randval, 0, 9);
            }
            if (strlen($randval) < 9) {
                $randval = '0' . $randval;
            }
            $param1 = '';

            $parentGroups = array();
            self::getAllParenGroups($shopGroupId, $parentGroups);
            $parentGroups = array_reverse($parentGroups);
            $parGroup = $parentGroups[0]->toArray();
            $GetGroupRoot = $parGroup['id'];

            $oProperty = Core_Entity::factory('Property');

            $oProperty = $oProperty->getByTag_Name('kod_group');
            if (isset($oProperty)) {
                $aPropertyValues = $oProperty->getValues($GetGroupRoot, FALSE);
                if (isset($aPropertyValues[0])) {
                    $param1 = '3M'.$aPropertyValues[0]->value;
                }
            }
            $retValue = 0;
            strlen($param1.$randval)==15 && $retValue = $param1 . $randval;
            return self::generateMegaMarkingCode($shopGroupId, $retValue, $counterRetryes+1, $currentShopId);
        }
        return 0;
    }


    function generateMarkingCode($name,$marking){
		return str_replace(' ', '-', Core_Str::translate($name)).'-'.$marking;
	}

	public static function getStatusSearchStr($str,$search){
		$pos = strpos($str, $search);
		if ($pos === false) {
			return 0;
		}else{
			return 1;
		}
	}

	public static function getMimeFileType($fileName){
		$fileExtension = Core_File::getExtension($fileName);
		$mimeTypes = array(
			'hqx'	=>	array('application/mac-binhex40', 'application/mac-binhex', 'application/x-binhex40', 'application/x-mac-binhex40'),
			'cpt'	=>	'application/mac-compactpro',
			'csv'	=>	array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain'),
			'bin'	=>	array('application/macbinary', 'application/mac-binary', 'application/octet-stream', 'application/x-binary', 'application/x-macbinary'),
			'dms'	=>	'application/octet-stream',
			'lha'	=>	'application/octet-stream',
			'lzh'	=>	'application/octet-stream',
			'exe'	=>	array('application/octet-stream', 'application/x-msdownload'),
			'class'	=>	'application/octet-stream',
			'psd'	=>	array('application/x-photoshop', 'image/vnd.adobe.photoshop'),
			'so'	=>	'application/octet-stream',
			'sea'	=>	'application/octet-stream',
			'dll'	=>	'application/octet-stream',
			'oda'	=>	'application/oda',
			'pdf'	=>	array('application/pdf', 'application/force-download', 'application/x-download', 'binary/octet-stream'),
			'ai'	=>	array('application/pdf', 'application/postscript'),
			'eps'	=>	'application/postscript',
			'ps'	=>	'application/postscript',
			'smi'	=>	'application/smil',
			'smil'	=>	'application/smil',
			'mif'	=>	'application/vnd.mif',
			'xls'	=>	array('application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 'application/excel', 'application/download', 'application/vnd.ms-office', 'application/msword'),
			'ppt'	=>	array('application/powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-office', 'application/msword'),
			'pptx'	=> 	array('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/x-zip', 'application/zip'),
			'wbxml'	=>	'application/wbxml',
			'wmlc'	=>	'application/wmlc',
			'dcr'	=>	'application/x-director',
			'dir'	=>	'application/x-director',
			'dxr'	=>	'application/x-director',
			'dvi'	=>	'application/x-dvi',
			'gtar'	=>	'application/x-gtar',
			'gz'	=>	'application/x-gzip',
			'gzip'  =>	'application/x-gzip',
			'php'	=>	array('application/x-httpd-php', 'application/php', 'application/x-php', 'text/php', 'text/x-php', 'application/x-httpd-php-source'),
			'php4'	=>	'application/x-httpd-php',
			'php3'	=>	'application/x-httpd-php',
			'phtml'	=>	'application/x-httpd-php',
			'phps'	=>	'application/x-httpd-php-source',
			'js'	=>	array('application/x-javascript', 'text/plain'),
			'swf'	=>	'application/x-shockwave-flash',
			'sit'	=>	'application/x-stuffit',
			'tar'	=>	'application/x-tar',
			'tgz'	=>	array('application/x-tar', 'application/x-gzip-compressed'),
			'z'	=>	'application/x-compress',
			'xhtml'	=>	'application/xhtml+xml',
			'xht'	=>	'application/xhtml+xml',
			'zip'	=>	array('application/x-zip', 'application/zip', 'application/x-zip-compressed', 'application/s-compressed', 'multipart/x-zip'),
			'rar'	=>	array('application/x-rar', 'application/rar', 'application/x-rar-compressed'),
			'mid'	=>	'audio/midi',
			'midi'	=>	'audio/midi',
			'mpga'	=>	'audio/mpeg',
			'mp2'	=>	'audio/mpeg',
			'mp3'	=>	array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
			'aif'	=>	array('audio/x-aiff', 'audio/aiff'),
			'aiff'	=>	array('audio/x-aiff', 'audio/aiff'),
			'aifc'	=>	'audio/x-aiff',
			'ram'	=>	'audio/x-pn-realaudio',
			'rm'	=>	'audio/x-pn-realaudio',
			'rpm'	=>	'audio/x-pn-realaudio-plugin',
			'ra'	=>	'audio/x-realaudio',
			'rv'	=>	'video/vnd.rn-realvideo',
			'wav'	=>	array('audio/x-wav', 'audio/wave', 'audio/wav'),
			'bmp'	=>	array('image/bmp', 'image/x-bmp', 'image/x-bitmap', 'image/x-xbitmap', 'image/x-win-bitmap', 'image/x-windows-bmp', 'image/ms-bmp', 'image/x-ms-bmp', 'application/bmp', 'application/x-bmp', 'application/x-win-bitmap'),
			'gif'	=>	'image/gif',
			'jpeg'	=>	array('image/jpeg', 'image/pjpeg'),
			'jpg'	=>	array('image/jpeg', 'image/pjpeg'),
			'jpe'	=>	array('image/jpeg', 'image/pjpeg'),
			'png'	=>	array('image/png',  'image/x-png'),
			'tiff'	=>	'image/tiff',
			'tif'	=>	'image/tiff',
			'css'	=>	array('text/css', 'text/plain'),
			'html'	=>	array('text/html', 'text/plain'),
			'htm'	=>	array('text/html', 'text/plain'),
			'shtml'	=>	array('text/html', 'text/plain'),
			'txt'	=>	'text/plain',
			'text'	=>	'text/plain',
			'log'	=>	array('text/plain', 'text/x-log'),
			'rtx'	=>	'text/richtext',
			'rtf'	=>	'text/rtf',
			'xml'	=>	array('application/xml', 'text/xml', 'text/plain'),
			'xsl'	=>	array('application/xml', 'text/xsl', 'text/xml'),
			'mpeg'	=>	'video/mpeg',
			'mpg'	=>	'video/mpeg',
			'mpe'	=>	'video/mpeg',
			'qt'	=>	'video/quicktime',
			'mov'	=>	'video/quicktime',
			'avi'	=>	array('video/x-msvideo', 'video/msvideo', 'video/avi', 'application/x-troff-msvideo'),
			'movie'	=>	'video/x-sgi-movie',
			'doc'	=>	array('application/msword', 'application/vnd.ms-office'),
			'docx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword', 'application/x-zip'),
			'dot'	=>	array('application/msword', 'application/vnd.ms-office'),
			'dotx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword'),
			'xlsx'	=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip', 'application/vnd.ms-excel', 'application/msword', 'application/x-zip'),
			'word'	=>	array('application/msword', 'application/octet-stream'),
			'xl'	=>	'application/excel',
			'eml'	=>	'message/rfc822',
			'json'  =>	array('application/json', 'text/json'),
			'pem'   =>	array('application/x-x509-user-cert', 'application/x-pem-file', 'application/octet-stream'),
			'p10'   =>	array('application/x-pkcs10', 'application/pkcs10'),
			'p12'   =>	'application/x-pkcs12',
			'p7a'   =>	'application/x-pkcs7-signature',
			'p7c'   =>	array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
			'p7m'   =>	array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
			'p7r'   =>	'application/x-pkcs7-certreqresp',
			'p7s'   =>	'application/pkcs7-signature',
			'crt'   =>	array('application/x-x509-ca-cert', 'application/x-x509-user-cert', 'application/pkix-cert'),
			'crl'   =>	array('application/pkix-crl', 'application/pkcs-crl'),
			'der'   =>	'application/x-x509-ca-cert',
			'kdb'   =>	'application/octet-stream',
			'pgp'   =>	'application/pgp',
			'gpg'   =>	'application/gpg-keys',
			'sst'   =>	'application/octet-stream',
			'csr'   =>	'application/octet-stream',
			'rsa'   =>	'application/x-pkcs7',
			'cer'   =>	array('application/pkix-cert', 'application/x-x509-ca-cert'),
			'3g2'   =>	'video/3gpp2',
			'3gp'   =>	array('video/3gp', 'video/3gpp'),
			'mp4'   =>	'video/mp4',
			'm4a'   =>	'audio/x-m4a',
			'f4v'   =>	'video/mp4',
			'webm'	=>	'video/webm',
			'aac'   =>	'audio/x-acc',
			'm4u'   =>	'application/vnd.mpegurl',
			'm3u'   =>	'text/plain',
			'xspf'  =>	'application/xspf+xml',
			'vlc'   =>	'application/videolan',
			'wmv'   =>	array('video/x-ms-wmv', 'video/x-ms-asf'),
			'au'    =>	'audio/x-au',
			'ac3'   =>	'audio/ac3',
			'flac'  =>	'audio/x-flac',
			'ogg'   =>	'audio/ogg',
			'kmz'	=>	array('application/vnd.google-earth.kmz', 'application/zip', 'application/x-zip'),
			'kml'	=>	array('application/vnd.google-earth.kml+xml', 'application/xml', 'text/xml'),
			'ics'	=>	'text/calendar',
			'ical'	=>	'text/calendar',
			'zsh'	=>	'text/x-scriptzsh',
			'7zip'	=>	array('application/x-compressed', 'application/x-zip-compressed', 'application/zip', 'multipart/x-zip'),
			'cdr'	=>	array('application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw', 'image/cdr', 'image/x-cdr', 'zz-application/zz-winassoc-cdr'),
			'wma'	=>	array('audio/x-ms-wma', 'video/x-ms-asf'),
			'jar'	=>	array('application/java-archive', 'application/x-java-application', 'application/x-jar', 'application/x-compressed'),
			'svg'	=>	array('image/svg+xml', 'application/xml', 'text/xml'),
			'vcf'	=>	'text/x-vcard',
			'srt'	=>	array('text/srt', 'text/plain'),
			'vtt'	=>	array('text/vtt', 'text/plain'),
			'ico'	=>	array('image/x-icon', 'image/x-ico', 'image/vnd.microsoft.icon')
		);

		return (array_key_exists($fileExtension, $mimeTypes) ? array($fileExtension=>$mimeTypes[$fileExtension]) : NULL);
	}

	public static function sendMail($from, $to, $subject, $message, $contentType = 'text/plain'){
		Core_Mail::instance()
			->to($to)
			->from($from)
			->subject($subject)
			->message(trim($message))
			->contentType($contentType)
			->header('X-HostCMS-Reason', 'User-Registration')
			->header('Precedence', 'bulk')
			->send();
	}

	public static function getListByPath($list_id = 0, $path = '')
	{
		$list = Core_Entity::factory('List_Item');
		$list
			->queryBuilder()
			->where('list_items.list_id','=',$list_id)
			->where('list_items.deleted', '=', 0)
			->where('list_items.path', '=', $path)
		;
		$olist = $list->findAll();
		return $olist;
	}

	public static function getGroupPropertyById($entityId, $propertyId, $type = 'ints', $field = '', $keyField = '')
	{
		$select = Core_QueryBuilder::select()
			->from('property_value_' . $type)
			// ->columns('*')
			->where('property_id', (is_array($propertyId) ? 'IN' : '='), $propertyId)
			->where('entity_id', '=', $entityId);
		$select = $select
			->execute()
			->asAssoc()
			->result();
		if (isset($select[0])) {
			if(is_array($propertyId)) {
				$oArray = array();
				if ($field === '') {
					foreach($select as $sItem) {
						if($keyField=='') {
							$oArray[] = $sItem;
						} else {
							$oArray[$sItem[$keyField]] = $sItem;
						}
					}
				} else {
					foreach($select as $sItem) {
						if($keyField=='') {
							$oArray[] = $sItem[$field];
						} else {
							$oArray[$sItem[$keyField]] = $sItem[$field];
						}
					}
				}
				return $oArray;
			} else {
				if ($field === '') {
					return $select[0];
				} else {
					if (isset($select[0][$field])) {
						return $select[0][$field];
					} else {
						return NULL;
					}
				}
			}
		}
		return NULL;
	}

	public static function buildTree($items) {
		$childs = array();
		foreach($items as &$item) {
			$item['checked'] = true;
			$childs[$item['parent_id']][] = &$item;
		}
		unset($item);
		foreach($items as &$item) {
			if (isset($childs[$item['id']])) {
				$item['childs'] = $childs[$item['id']];
			}
		}
		return isset($childs[0]) ? $childs[0] : $childs;
	}

	public static function clearCart($oShop)
	{
		$oShop_Cart_Controller = Shop_Cart_Controller::instance();
		foreach ($oShop_Cart_Controller->getAll($oShop) as $k) {
			$oShop_Cart_Controller
				->shop_item_id($k->shop_item_id)
				->delete();
		}
	}

}