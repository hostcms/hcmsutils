<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Sitemap;

use Core_Entity;
use Core_File;
use Core_QueryBuilder;
use HCMS\Utils as utl;

use HCMS\Sphinx\Client as sphinx;

use HCMS\Shop\Groups as group;
use LSS\Array2XML as axml;
use Shop_Controller_Show;


class Show {
	protected $_shop;
    private $hostname;
    private $ts;
    private $debug = false;
    private $preGroup = true;
    private $protocol = 'http';

	/**
	 * @return string
	 */
	public function getProtocol()
	{
		return $this->protocol;
	}

	/**
	 * @param string $protocol
	 */
	public function setProtocol($protocol)
	{
		$this->protocol = $protocol;
		return $this;
	}

	function __construct($shopId=3, $shop=null) {
		$this->_shop = $shop;
        $this->hostname = $_SERVER['HTTP_HOST'];
	}

	public static function createInstance($shopId=3, $shop=null) {
		if(is_null($shop)) {
			$shop =  Core_Entity::factory('Shop', $shopId);
		}
		return new self($shopId, $shop);
	}

    private function preRender(){
        header ("Content-Type:text/xml");
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
    }

    private function preRenderIndex(){
        header ("Content-Type:text/xml");
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
    }

    private function postRender(){
        echo"</urlset>";
    }

    private function postRenderIndex(){
        echo"</sitemapindex>";
    }

    private function renderUrl($url){
        if(!$this->debug) echo "<url>\n<loc>".$this->protocol."://{$this->hostname}{$url}</loc>\n<changefreq>daily</changefreq>\n<priority>0.5</priority>\n</url>\n";
    }

    private function renderSitemap($url){
        if(!$this->debug) echo "<sitemap>\n<loc>".$this->protocol."://{$this->hostname}/{$url}/sitemap.xml</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n</sitemap>\n";
    }

    private function tstart($comment=''){
        if($this->debug) echo "Start:$comment\n";
        $this->ts = microtime(true);
    }

    private function tcheck($comment=''){
        if($this->debug) echo "Check:" . (microtime(true) - $this->ts) . ' ' . $comment ."\n";
    }

    private function getLinks($group_id){
        $client = sphinx::createInstance('i_sitemap_data', 1000000, 1000000, 0, array('matchMode'=>0, 'sortMode'=>2/*, 'sortModeField'=>'@count'*/));
        $client->addFilter(array('group_id'=>array($group_id)));
        $result = $client->execute("");
        $urls = array();
		for($i=1; $i<=5; $i++) {
			$clientGroups = sphinx::createInstance('i_sitemap_data', 1000000, 1000000, 0, array('matchMode'=>0, 'sortMode'=>2/*, 'sortModeField'=>'@count'*/));
			$clientGroups->addFilter(array('group_id'=>array($group_id)));
			$clientGroups->addGroupBy(array("u{$i}"=>SPH_GROUPBY_ATTR));
			$resultGroups = $clientGroups->execute("");
			foreach($resultGroups['matches'] as $resultGroup) {
				$subUrl = '/';
				for($k=1; $k<=$i; $k++) {
					if($resultGroup['attrs']['u'.$k]!='') {
						$subUrl .= $resultGroup['attrs']['u'.$k].'/';
					}
				}
				$urls[] = $subUrl;
			}
			unset($clientGroups);
		}
		foreach($result['matches'] as $item){
			$urls[] = $item['attrs']['url'];
		}
        return($urls);
    }

    public function showSitemapIndex(){
        $this->preRenderIndex();
        $client = sphinx::createInstance('i_sitemap_data', 1000000, 1000000, 0, array('matchMode'=>0, 'sortMode'=>2/*, 'sortModeField'=>'@count'*/));
        $client->addGroupBy(array("u1"=>SPH_GROUPBY_ATTR));
        $result = $client->execute("");
        foreach($result['matches'] as $currentItem){
            $this->renderSitemap($currentItem['attrs']['u1']);
        }
        $this->renderSitemap('staticpages');
        $this->postRenderIndex();
    }

	public function showShopTopCategory($category) {
        if(intval($category)>0) {
			$category = utl::getModelItemsByArrayIds('Shop_Group', array($category*1));
		}
		if(is_object($category[0]) && get_class($category[0])=='Shop_Group_Model') {
            $this->preRender();
            $this->tstart('get links');
            $urls = $this->getLinks($category[0]->id);
            $this->tcheck('links finish');
            foreach($urls as $url){
                $this->renderUrl($url);
            }
            $this->tcheck('render finish');
       }
       $this->postRender();
       $this->tcheck('showShopTopCategory finish');
    }

    protected function getParentsPath($currentObj, $parentsObj, $path=''){
        if ($currentObj->parent_id != 0 ){
            $pid = -1;
            foreach($parentsObj as $prnt){
                if($prnt->id == $currentObj->parent_id){
                    $obj = $prnt;
                    $pid = $prnt->id;
                    $path = "/{$currentObj->path}{$path}";
                }
            }
            if ($pid >= 0){
                $path = $this->getParentsPath($obj, $parentsObj, $path);
                return($path);
            }
        } else {
            $path = "{$currentObj->path}{$path}";
            return($path);
        }
    }

    public function showSitemapStaticPages($site_id=2){
        $this->preRender();
        $objStructure = Core_Entity::factory('Structure');
        $objStructure->queryBuilder()
            ->where('changefreq', '<>', 6)
			->where('site_id', '=', $site_id)
            ->where('show', '=', 1)
            ->where('active', '=', 1)
            ->where('deleted', '=', 0)
            ;
        $struct = $objStructure->findAll(true);
//        utl::p(utl::setArrayOfObjectsToArray($struct));
        $urls = array();
        foreach($struct as $currItem){
            if($currItem->parent_id != 0){
                $path = $this->getParentsPath($currItem, $struct);
                if (!$path){
                    continue;
                }
                $urls[] = "/{$path}/";
            } else {
                if ($currItem->path != '/'){
                    $urls[] = "/{$currItem->path}/";
                } else {
                    $urls[] = "/";
                }
            }
        }
        foreach( $urls as $url){
            $this->renderUrl($url);
        }
        $this->postRender();
    }
}