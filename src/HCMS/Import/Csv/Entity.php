<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 16.04.2015
 * Time: 9:11
 */
namespace HCMS\Import\Csv;

use HCMS\Utils as utl;
use HCMS\Shop\Items as utlItems;
use Core_Servant_Properties;
use Core_Str;
use Core_Entity;

abstract class Entity extends Core_Servant_Properties {
	const OBJECT_NEW = 'new_object';
	const OBJECT_FOUNDED = 'founded_object';
	const OBJECT_NULL = 'object_not_found';

	protected $isEmpty;
	protected $_foundedEntity=NULL;
	protected $_options=array();
	protected $_fileIterations=array();
	protected $_globalKeyField=NULL;
	protected $_properties=NULL;
	protected $_settings=NULL;
	protected $_errorMessage="";
	protected $_errorCode="";

	abstract public function getKeyField();

	abstract public function getEntity($keyField);

	abstract public function createFields();

	abstract public function updateFields($item);

	function __construct($dataItem=array(), $options=array(), $fileIterations=array('current'=>0, 'part'=>0, 'prev'=>0), $properties=NULL, $classEntitySettings=array()) {
		$this->isEmpty = true;
		$this->_options = $options;
		$this->_fileIterations = $fileIterations;

		if(is_array($properties) && count($properties)==0 && is_array($dataItem) && count($dataItem)>0 ) {
			foreach($dataItem as $data) {
				$this->addAllowedProperty(str_replace('-', '_', Core_Str::transliteration($data)));
			}
		} elseif (is_array($properties) && count($properties)>0) {
			$this->addAllowedProperties($properties);
			if(count($dataItem)==count($this->getAllowedProperties())) {
				foreach($this->getAllowedProperties() as $key => $property) {
					$this->$property = $dataItem[$key];
				}
				$this->isEmpty = false;
			}
		}
		$this->_settings = $classEntitySettings;
	}

	/**
	 * @return boolean
	 */
	public function isEmpty()
	{
		return $this->isEmpty;
	}

	/**
	 * @param mixed $isEmpty
	 */
	public function setEmpty($isEmpty)
	{
		$this->isEmpty = $isEmpty;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage()
	{
		return $this->_errorMessage;
	}

	/**
	 * @param string $errorMessage
	 */
	public function setErrorMessage($errorMessage)
	{
		$this->_errorMessage = $errorMessage;
	}

	/**
	 * @return string
	 */
	public function getErrorCode()
	{
		return $this->_errorCode;
	}

	/**
	 * @param string $errorCode
	 */
	public function setErrorCode($errorCode)
	{
		$this->_errorCode = $errorCode;
	}

	/**
	 * @return null
	 */
	public function getFoundedEntity()
	{
		return $this->_foundedEntity;
	}

	/**
	 * @param null $foundedItem
	 */
	public function setFoundedEntity($foundedEntity)
	{
		$this->_foundedEntity = $foundedEntity;
	}

	public function getEntityByField($field) {
		$this->_foundedEntity = utlItems::createInstance()->getByMarking($field);
		return $this->_foundedEntity;
	}

	public function getAllowedProperties() {
		return array_values($this->_allowedProperties);
	}

//	public function getEntity($keyField) {
//		return $this;
//	}

//	public function createFields() {
//		return $this;
//	}
//
//	public function updateFields() {
//		return $this;
//	}

	/**
	 * @return array
	 */
	public function getFileIterations()
	{
		return $this->_fileIterations;
	}

	/**
	 * @param array $fileIterations
	 */
	public function setFileIterations($fileIterations)
	{
		$this->_fileIterations = $fileIterations;
	}

	/**
	 * @return null
	 */
	public function getGlobalKeyField()
	{
		return $this->_globalKeyField;
	}

	/**
	 * @param null $globalKeyField
	 */
	public function setGlobalKeyField($globalKeyField)
	{
		$this->_globalKeyField = $globalKeyField;
	}

	public function valuesArray() {
		$arr = array();
		foreach($this->getAllowedProperties() as $property) {
			$arr[$property] = $this->$property;
		}
		return $arr;
	}

	public function toArray() {
		$arr = array();
		foreach(get_object_vars($this) as $k=>$v) {
			$arr[preg_replace('/^_/', '', $k)] = $v;
		}
		return $arr;
	}

	protected function checkPropertyValues($property, $values) {
		if($values==='') {
			$values = array();
		}
		$currentItem = $this->getEntity();
		$currentPropertyValues = $property->getValues($currentItem->id);

//		utl::p($values, $property->name.', то что в файле');
//		utl::p($currentPropertyValues, $property->name.', то что в БД');
		$currentPropertyValObjects = array();
		foreach($currentPropertyValues as $currentPropertyValue) {
			switch ($property->type) {
				case 1:
				case 11:
					$currentPropertyValObjects[$currentPropertyValue->id] = (object) array('value' => $currentPropertyValue->value, 'path' => '');
					break;
				case 3:
					$currentPropertyValObjects[$currentPropertyValue->id] = (object) array('value' => $currentPropertyValue->list_item->value, 'path' => $currentPropertyValue->list_item->path);
					break;
			}
		}
//		utl::p($currentPropertyValObjects, $property->name.' в БД, то что после преобразования');
		$valuesEqual = array();
		$valuesForAdd = array();
		$valuesForDelete = array();
		$valuesForAddValid = array();
		if(count($values)>0) {
			//-- Разделили по значениям, нашли те которые не надо трогать - $valuesEqual
			//-- и те, которые надо ДОБАВИТЬ в БД - $valuesForAdd
			foreach($values as $valueKey => $value) {
				if(($k = array_search($value, $currentPropertyValObjects))!==FALSE) { // Значение свойства из файла найдено как свойство в БД
					$valuesEqual[] = $value;
				} else { // Значение свойства из файла не найдено как свойство в БД
					$valuesForAdd[] = $value;
				}
				if(count($currentPropertyValObjects)>0) {
					foreach($currentPropertyValObjects as $valueKey => $currentPropertyValObject) {
						if(($k = array_search($currentPropertyValObject, $values))!==FALSE) { // Значение свойства из файла найдено как свойство в БД
							$valuesForAddValid[] = $currentPropertyValObject;
						} else { // Значение свойства из файла не найдено как свойство в БД
							$valuesForDelete[$valueKey] = $currentPropertyValObject;
						}
					}
				} else {
					$valuesForAdd = $values;
				}
			}
		} else {
			$valuesForDelete = $currentPropertyValObjects;
		}
//		utl::p(($valuesEqual), $property->name.' равные в БД и в файле $valuesEqual');
//		utl::p(($valuesForAddValid), $property->name.' равные в БД. Проверка $valuesForAddValid');
//		utl::p(($valuesForDelete), $property->name.' для удаления из БД $valuesForDelete');
		foreach($currentPropertyValues as $currentPropertyValue) {
			$foundBy = $currentPropertyValue->id;
			if(array_key_exists($foundBy, $valuesForDelete)) {
				$valueForReplace = NULL;
				if(count($valuesForAdd)>0) {
					$valueForReplace = array_pop($valuesForAdd);
					$replacedValue = NULL;
					switch ($property->type) {
						case 1:
						case 11:
							$currentPropertyValue->value = $valueForReplace->value;
							$currentPropertyValue->save();
							break;
						case 3:
							$listItemValueObject = $this->getListItem($property->list_id, $valueForReplace);
							if( isset($listItemValueObject->id) && $listItemValueObject->id>0) {
								$currentPropertyValue->value = $listItemValueObject->id;
								$currentPropertyValue->save();
							} else {
								//-- Ошибка. Свойство не привязано к списку ------------------------------------------------
							}
							break;
					}
				} else {
					$currentPropertyValue->delete();
				}
			}
		}

//		utl::p(($valuesForAdd), $property->name.' для дополнения в БД $valuesForAdd');
		foreach($valuesForAdd as $currentPropertyValue) {
			$aPropertyValueNew = $property->createNewValue($currentItem->id);
//			utl::p($aPropertyValueNew->toArray(), '-- New value --');
			switch($property->type) {
				case 1:
				case 11:
					$aPropertyValueNew->value = $currentPropertyValue->value;
					$aPropertyValueNew->save();
					break;
				case 3:
					$listItemValueObject = $this->getListItem($property->list_id, $currentPropertyValue);
					if( isset($listItemValueObject->id) && $listItemValueObject->id>0) {
						$aPropertyValueNew->value = $listItemValueObject->id;
						$aPropertyValueNew->save();
					} else {
						//-- Ошибка. Свойство не привязано к списку ------------------------------------------------
					}
				}
			}
	}

	protected function getListItem($list_id, $object) {
		if($list_id>0) {
			$listItemValue = isset($object->value) ? $object->value : '';
			$listItemPath = isset($object->path) ? $object->path : '';
			$list = Core_Entity::factory("List", $list_id);
			if($listItemValue!='' && isset($list->id) && $list->id>0) {
				$listItemValueObject = $list->List_items->getByValue($object->value, FALSE);
				if (is_null($listItemValueObject)) {
					$listItemValueObject = Core_Entity::factory('List_Item');
					$listItemValueObject->list_id = $list_id;
					$listItemValueObject->value = $listItemValue;
					isset($listItemValueObject->path) && $listItemValueObject->path = $listItemPath;
					$listItemValueObject->save();
				}
				//-- Костыль для проекта зипик. Так делать не рекомендуется здесь --
				if ($listItemValueObject->list_id == 216 &&
					isset($this->_prop_parts_types_group) && isset($listItemValueObject->groupby)
				) {
					$listItemValueObject->groupby = $this->_prop_parts_types_group[0]->path;
					$listItemValueObject->save();
				}
				//-- Конец костыля --
				return $listItemValueObject;
			}
		}
		return NULL;
	}
}