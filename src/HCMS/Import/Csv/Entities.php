<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 16.04.2015
 * Time: 9:11
 */
namespace HCMS\Import\Csv;

use HCMS\Utils as utl;
use Core_Event;

class Entities extends Settings
{
	private $_fileOperationAfterMake;
	private $_globalKeyField = NULL;
	private $_processedItems=array();
	private $_infoItems=array();

	function __construct($_fileOperationAfterMake = 'rename', $includeInitHandbook = true, $delimiter = ';', $readFromLine=0)
	{
		parent::__construct($includeInitHandbook, $delimiter, $readFromLine);
		$this->_fileOperationAfterMake = $_fileOperationAfterMake;
	}

	public function importRecords($foldername, $classEntity, $limit = 1000000, $page = 1000, $classEntitySettings=array())
	{
		$fileIteration = 0;
		$fileIterationPrev = 0;
		$allEntitieFiles = glob("{$foldername}*.[cC][sS][vV]");
		$allEntitieFiles=='' && $allEntitieFiles=array();
		usort($allEntitieFiles, function ($a, $b) {
			return filemtime($b) - filemtime($a);
		});
//		utl::p($foldername);
//		set_time_limit(10);
		set_time_limit(36000);
		foreach ($allEntitieFiles as $file) {
			$fileIteration++;
			Core_Event::notify('Import_CSV_Entities.onBeforeImportFile', $this, array($file));
			$allRecords = $this->import($file, $classEntity, $limit, false, $fileIterations=array('current'=>0, 'part'=>0, 'prev'=>0), $classEntitySettings);

			$newRecords = array();
			$updatedRecords = array();
			if ($allRecords !== FALSE && is_array($allRecords) && count($allRecords) > 0) {
				Core_Event::notify('Import_CSV_Entities.onEntitiesBeforeAll', $this, array($file, $allRecords[0]));
				$splitRecords = array_chunk($allRecords, $page);
				$pages = count($splitRecords);
				foreach ($splitRecords as $pageNum => $records) {
					$onPageRecords = count($records);
					foreach ($records as $recordNum=>$record) {
						$isNewRecord = FALSE;
						$record->setFileIterations(array('current'=>$fileIteration, 'part'=>$recordNum, 'prev'=>$fileIterationPrev));
						if($recordNum==0) {
							$this->_globalKeyField = $record->getGlobalKeyField();
						}
						$record->setGlobalKeyField($this->_globalKeyField);
						$keyValue = $record->getKeyField();
						$item = $record->getEntity($keyValue);
						Core_Event::notify('Import_CSV_Entity.onBeforeProcess', $this, array($item, $record, $file));
						$keyValueTitle = $keyValue;
						if( is_array($keyValue) ) {
							$keyValueTitle = implode(',', $keyValue);
						}
						utl::p("", "({$pages})[{$pageNum} - {$recordNum}], {$keyValueTitle}", false);

						if (is_null($item)) {
							// -- Создать запись --
							$item = $record->createFields();
							$isNewRecord = TRUE;
						}
						$processed = NULL;
						if (!is_null($item)) {
							// -- Обновить запись --
							$processed = $record->updateFields($item);
							$this->_processedItems[] = $processed;
							if($isNewRecord) {
								$newRecords[] = $keyValue;
							} else {
								$updatedRecords[] = $keyValue;
							}
							echo "Элемент {$keyValueTitle} обработан...\n";
						} else {
							echo "Элемент {$keyValueTitle} не создан...\n";
						}
						Core_Event::notify('Import_CSV_Entity.onAfterProcess', $this, array($processed, $record, $file));
					}
				}
			}
			Core_Event::notify('Import_CSV_Entities.onEntitiesAfterAll', $this, array(
				  $classEntity
				, array('error'=>$this->_erorRecords, 'new'=>$newRecords, 'updated'=>$updatedRecords)
				, $file
			));

			switch ($this->_fileOperationAfterMake) {
				case 'rename':
					$arFilePath = explode('/', $file);
					array_splice($arFilePath, count($arFilePath)-2, 0, array('processed'));
					array_splice($arFilePath, count($arFilePath)-1, 0, array(date('Y'), date('m')));
					$fileNew = implode('/', $arFilePath);
					utl::checkFolder(dirname($fileNew));
					copy($file, $fileNew .".".date('Y-m-d-H-i-s').".done");
					unlink($file);
					break;
				case 'delete':
					unlink($file);
					break;
			}
			$fileIterationPrev = $fileIteration;
		}
	}
}