<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 16.04.2015
 * Time: 9:11
 */
namespace HCMS\Import\Csv;

use HCMS\Utils as utl;

class Settings {
	private $_delimiter;
	protected $_erorRecords=array();
	protected $_readFromLine=0;

	function __construct($includeInitHandbook = true, $delimiter=';', $readFromLine=0) {
		$this->_delimiter = $delimiter;
		$this->_readFromLine = $readFromLine;
	}

	/*-- Чтение файла из папки импорта. Передается filename - только имя файла --*/
	public function readFile($filename) {
		$rows = array();
		if (file_exists($filename)) {
			$t = file_get_contents($filename);
			$fileEncoding =  mb_detect_encoding($t, array('utf-8', 'cp1251'));
			if (($handle = fopen($filename, "r")) !== FALSE) {
				if( $fileEncoding!='UTF-8' ) {
					$handle = fopen('php://memory', 'w+');
					fwrite($handle, iconv('CP1251', 'UTF-8', file_get_contents($filename)));
					rewind($handle);
				}
				$index = 0;
				while (($data = fgetcsv($handle, null, $this->_delimiter)) !== FALSE) {
					if($index>=$this->_readFromLine && ($this->limitRecords==0 || $index<$this->limitRecords)) {
						$rows[] = $data;
					}
					$index++;
				}
				fclose($handle);
			} else {
				$rows = false;
			}
		} else {
			$rows = false;
		}
		return $rows;
	}

	/*-- Преобразование массива свойств из файла в массив объектов переданного класса --*/
	public function import($importFile, $importClass, $limit, $writeLog=false, $fileIterations=array('current'=>0, 'part'=>0, 'prev'=>0), $classEntitySettings=array()) {
		$returnArray = false;
		$this->limitRecords = $limit;
		//-- Инициализация переменных --------------------------------------
		$sections = array();
		//-- Код -----------------------------------------------------------
		$data = $this->readFile($importFile);

		$sections = array();
		if ($data !== false) {
			try {
				$properties = array();
				foreach( $data as $index => $item ) {
					$dataItem = new $importClass( $item, array(
						'importFilePath' => $importFile,
						'importFilePathSplitted' => explode('/', $importFile),
						'importFilePathInfo' => pathinfo($importFile),
					), $fileIterations, $properties, $classEntitySettings);

					$properties = $dataItem->getAllowedProperties();

					if (is_object($dataItem) && get_class($dataItem)==$importClass && method_exists($dataItem, 'isEmpty') && !$dataItem->isEmpty()) {
						$sections[] = $dataItem;
					} else {
						$this->_erorRecords[] = array('item'=>$dataItem->valuesArray(), 'message'=>$dataItem->getErrorMessage(), 'code'=>$dataItem->getErrorCode());
					}
				}
				$returnArray = $sections;
			} catch (\Exception $e) {
				$mess = "Элемент {$item[$index]} не может быть преобразован классу {$importClass}\n".$e->getMessage();
				utl::p($mess, "Ошибка преобразования {$e->getCode()}");
				$this->_erorRecords[] = array('item'=>$item, 'message'=>$mess, 'code'=>$e->getCode());
			}
		} else {
//            $this->pushLog( sprintf('Reading file "%s". Error', $importFile) );
		}
		return $returnArray;
	}

	/**
	 * @return string
	 */
	public function getDelimiter()
	{
		return $this->_delimiter;
	}

	/**
	 * @param string $_delimiter
	 */
	public function setDelimiter($delimiter)
	{
		$this->_delimiter = $delimiter;
	}
}