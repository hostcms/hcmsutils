<?php

namespace HCMS\Images;

use HCMS\Utils as utl;
use WideImage\Exception\Exception;
use WideImage\WideImage as wi;
//use Imagine\Gd\Imagine as thumbs;
//use Imagine\Image\Point as tpoint;
//use Imagine\Image\Box as tbox;
//use Imagine\Image\ImageInterface as tFace;
use ImageOptimizer\OptimizerFactory as opt;

class XResize {
	protected $_watermarks = array();
	protected $_renameFileNameSize = true;
	protected $_noWaterMarks = array();
	protected $_debug = false;

	/**
	 * @return boolean
	 */
	public function isDebug()
	{
		return $this->_debug;
	}

	/**
	 * @param boolean $debug
	 */
	public function setDebug($debug)
	{
		$this->_debug = $debug;
	}

	/**
	 * @return array
	 */
	public function getNoWaterMarks()
	{
		return $this->_noWaterMarks;
	}

	/**
	 * @param array $noWaterMarks
	 */
	public function setNoWaterMarks($noWaterMarks)
	{
		$this->_noWaterMarks = $noWaterMarks;
	}

	/**
	 * @param array $watermarks
	 */
	public function addNoWatermark($wmPath)
	{
		$this->_noWaterMarks[] = $wmPath;
		return $this;
	}

	/**
	 * @param array $watermarks
	 */
	public function clearNoWatermarks()
	{
		$this->_noWaterMarks = array();
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isRenameFileNameSize()
	{
		return $this->_renameFileNameSize;
	}

	/**
	 * @param boolean $renameFileNameSize
	 */
	public function setRenameFileNameSize($renameFileNameSize)
	{
		$this->_renameFileNameSize = $renameFileNameSize;
	}

	/**
	 * @return array
	 */
	public function getWatermarks()
	{
		return $this->_watermarks;
	}

	/**
	 * @param array $watermarks
	 */
	public function setWatermarks($watermarks)
	{
		$this->_watermarks = $watermarks;
		return $this;
	}

	/**
	 * @param array $watermarks
	 */
	public function addWatermark($key, $wmPath)
	{
		$this->_watermarks[$key] = $wmPath;
		return $this;
	}

	/**
	 * @param array $watermarks
	 */
	public function clearWatermarks()
	{
		$this->_watermarks = array();
		return $this;
	}

	function checkCRC($item, $dir, $tmbdir, $sizes, $watermark='', $crop=false, $forceupdate = 0, $thumbName='') {
		//$crc = 0;
//		if($forceupdate == 1) {
//			$foundedFiles = glob($dir."/*.crc");
//			$foundedFiles=='' && $foundedFiles=array();
//			foreach ($foundedFiles as $filename) { // удаляем старые crc если есть
//				unlink($filename);
//			}
//			$tmpFiles = glob($tmbdir."/*.*");
//			$tmpFiles=='' && $tmpFiles=array();
//			foreach ($tmpFiles as $filename2) { // удаляем старые фотки
//				unlink($filename2);
//			}
//		}
		if (filesize ($dir.'/'.$item )) { // исходный файл существует и больше нуля
			if ( $this->_renameFileNameSize ) {
				$crc = crc32(file_get_contents($dir.'/'.$item)); // crc от исходного файла
				$tmbdir = utl::checkFolder($tmbdir);
				if(!file_exists($item.'/'.$item.'_'.$crc.'.crc')) // crc изменени или не существует
				{
					$crcFiles = glob($tmbdir."/{$item}_*.crc");
					$crcFiles=='' && $crcFiles=array();
					foreach ($crcFiles as $filename) { // удаляем старые crc если есть
						if(is_file($filename)) {
							unlink($filename);
						}
					}

					$aCrcFiles = glob($tmbdir."/*{$item}.*");
					$aCrcFiles=='' && $aCrcFiles=array();
					foreach ($aCrcFiles as $filename2) { // удаляем старые фотки
						if(is_file($filename2)) {
							unlink($filename2);
						}
					}

					$f = fopen($tmbdir.'/'.$item.'_'.$crc.'.crc', "w"); // создаем новый crc
					fclose ($f);
				}
			}
			return $this->checkSizes($item, $dir, $tmbdir, $sizes, $watermark, $crop, $thumbName);
		}
	}

	function checkSizes($item, $dir, $tmbdir, $sizes, $watermark='', $crop=false, $thumbName='') {
		foreach ( $sizes as $key => $val ) {
			$tmbdir = utl::checkFolder($tmbdir);
			$filename = $tmbdir.'/' . ($this->_renameFileNameSize ? ('s_'.$val['x'].'x'.$val['y'].'_') : "").($thumbName!='' ? $thumbName : $item);
			if($this->_debug || !file_exists($filename) || $thumbName!='') {
				$wImageNew = $val['x']*1;
				$hImageNew = $val['y']*1;
				$img = wi::load($dir.'/'.$item);
				$imgSizeW = $img->getWidth();   //-- ширина watermark
				$imgSizeH = $img->getHeight();  //-- высота watermark
				if($imgSizeW>$wImageNew || $imgSizeH>$hImageNew ) {
					$img = $img->resize($val['x'], $val['y']);
				} else {
					$wImageNew = $imgSizeW;
					$hImageNew = $imgSizeH;
				}
				$areaNew = $wImageNew*$hImageNew;
				$whAttitudeNew = $wImageNew/$hImageNew;

				$checkedArray = array($val['x']*1, $val['y']*1);
				if(isset($this->_watermarks[$watermark])
					&& $areaNew>62500
//					&& !$crop
					&& ( array_search($checkedArray, $this->_noWaterMarks)===FALSE )
				) {
					$wmImagePath = ((!is_array($this->_watermarks[$watermark])) ? $this->_watermarks[$watermark] : ((isset($this->_watermarks[$watermark]['path']) ? $this->_watermarks[$watermark]['path'] : '')));
					$wmSize = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['size'])) ? $this->_watermarks[$watermark]['size'][0] : array(50, '%') );
					$wmPositionY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][0] : 'center' );
					$wmPositionX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][1] : 'center' );
					$wmOffsetY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsety'])) ? ($this->_watermarks[$watermark]['offsety'][0]*1<0 ? '' : '+').$this->_watermarks[$watermark]['offsety'][0].$this->_watermarks[$watermark]['offsety'][1] : array('+0px') );
					$wmOffsetX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsetx'])) ? ($this->_watermarks[$watermark]['offsetx'][0]*1<0 ? '' : '+').$this->_watermarks[$watermark]['offsetx'][0].$this->_watermarks[$watermark]['offsetx'][1] : array('+0px') );

					$watermark = wi::load($wmImagePath); //-- watermark
					$wmSizeW = $watermark->getWidth();   //-- ширина watermark
					$wmSizeH = $watermark->getHeight();  //-- высота watermark

					if($wmSizeW>=$wmSizeH) {
						$wmSizeWnew = ($img->getWidth()*$wmSize)/100;
						$wmSizeHnew = $wmSizeWnew/($wmSizeW/$wmSizeH);
					} else {
						$wmSizeHnew = ($img->getHeight()*$wmSize)/100;
						$wmSizeWnew = $wmSizeHnew/($wmSizeH/$wmSizeW);
					}
					$watermark = $watermark
						->resize($wmSizeWnew, $wmSizeHnew)
					;

//					$wmAreaOrig = $wmSizeW*$wmSizeH;
//					$wmAreaWished = $areaNew*($wmSize/100);
//					$wmSizeWnew = $wImageNew*($wmSize)/100;
//					$wmSizeHnew = $wmAreaWished/$wmSizeWnew;
//					$watermark = $watermark
//						->resize($wmSizeWnew, $wmSizeHnew)
//					;

//					$wmWishedKoeff = $wmAreaWished/$wmAreaOrig;
////					if($wmAreaOrig>$wmAreaWished) {
//						$wmSizeWishedW = $wmSizeW*$wmWishedKoeff;
//						$wmSizeWishedH = $wmSizeH*$wmWishedKoeff;
//						$watermark = $watermark
//							->resize($wmSizeWishedW, $wmSizeWishedH)
//						;
////					}
					$img = $img->merge($watermark, "{$wmPositionX}{$wmOffsetX}", "{$wmPositionY}{$wmOffsetY}", 100);
				}

				$img
					->saveToFile($filename);

				$phMagick = new \phMagick\Core\Runner();
				$action = new \phMagick\Action\Convert($filename, $filename);
				// optimize the image
				$action->optimize();
				// sets image quality
				$action->quality(85);
				// execute the convert action
				$phMagick->run($action);

//				$factory = new opt();
//				$optimizer = $factory->get();
//				$optimizer->optimize($filename);

				return array(
					'sourceFilePath' => $dir.'/'.$item,
					'destinationFilePath' => $filename,
				);
			} else {
				return array(
					'sourceFilePath' => $dir.'/'.$item,
					'destinationFilePath' => $filename,
				);
			}
		}
	}

	function bak_new_checkSizes($item, $dir, $tmbdir, $sizes, $watermark='', $crop=false, $thumbName='') {
		foreach ( $sizes as $key => $val ) {
			$tmbdir = utl::checkFolder($tmbdir);
			$filename = $tmbdir.'/' . ($this->_renameFileNameSize ? ('s_'.$val['x'].'x'.$val['y'].'_') : "").($thumbName!='' ? $thumbName : $item);
			if($this->_debug || !file_exists($filename) || $thumbName!='') {
				$wImageNew = $val['x']*1;
				$hImageNew = $val['y']*1;
				$img = wi::load($dir.'/'.$item);
				$imgSizeW = $img->getWidth();
				$imgSizeH = $img->getHeight();
				if($imgSizeW>$wImageNew || $imgSizeH>$hImageNew ) {
					$img = $img->resize($val['x'], $val['y']);
				} else {
					$wImageNew = $imgSizeW;
					$hImageNew = $imgSizeH;
				}
				$areaNew = $wImageNew*$hImageNew;
				$whAttitudeNew = $wImageNew/$hImageNew;

				$checkedArray = array($val['x']*1, $val['y']*1);
				if(isset($this->_watermarks[$watermark])
					&& $areaNew>62500
					&& ( array_search($checkedArray, $this->_noWaterMarks)===FALSE )
				) {
					$wmImagePath = ((!is_array($this->_watermarks[$watermark])) ? $this->_watermarks[$watermark] : ((isset($this->_watermarks[$watermark]['path']) ? $this->_watermarks[$watermark]['path'] : '')));
					$wmSize = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['size'])) ? $this->_watermarks[$watermark]['size'][0] : array(50, '%') );
					$wmPositionY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][0] : 'center' );
					$wmPositionX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][1] : 'center' );
					$wmOffsetY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsety'])) ? ($this->_watermarks[$watermark]['offsety'][0]*1<0 ? '' : '+').$this->_watermarks[$watermark]['offsety'][0].$this->_watermarks[$watermark]['offsety'][1] : array('+0px') );
					$wmOffsetX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsetx'])) ? ($this->_watermarks[$watermark]['offsetx'][0]*1<0 ? '' : '+').$this->_watermarks[$watermark]['offsetx'][0].$this->_watermarks[$watermark]['offsetx'][1] : array('+0px') );

					$watermark = wi::load($wmImagePath); //-- watermark
					$wmSizeW = $watermark->getWidth();   //-- ширина watermark
					$wmSizeH = $watermark->getHeight();  //-- высота watermark

					if($wmSizeW>=$wmSizeH) {
						$wmSizeWnew = ($img->getWidth()*$wmSize)/100;
						$wmSizeHnew = $wmSizeWnew/($wmSizeW/$wmSizeH);
					} else {
						$wmSizeHnew = ($img->getHeight()*$wmSize)/100;
						$wmSizeWnew = $wmSizeHnew/($wmSizeH/$wmSizeW);
					}
					$watermark = $watermark
						->resize($wmSizeWnew, $wmSizeHnew)
					;

//					$img = $img->merge($watermark, "{$wmPositionX}{$wmOffsetX}", "{$wmPositionY}{$wmOffsetY}", 100);
				}

				$phMagick = new \phMagick\Core\Runner();

				$resizeAction = new \phMagick\Action\Resize\Proportional($dir.'/'.$item, $filename);
				$resizeAction->setWidth($wImageNew);
				$resizeAction->setHeight($hImageNew);

				$phMagick->run($resizeAction);

				$action = new \phMagick\Action\Convert($filename, $filename);
				// optimize the image
				$action->optimize();
				// sets image quality
				$action->quality(85);
				// execute the convert action
				$phMagick
					->run($action);

//				$img
//					->saveToFile($filename, 80);
//				$factory = new opt();
//				$optimizer = $factory->get();
//				$optimizer->optimize($filename);

				return array(
					'sourceFilePath' => $dir.'/'.$item,
					'destinationFilePath' => $filename,
				);
			} else {
				return array(
					'sourceFilePath' => $dir.'/'.$item,
					'destinationFilePath' => $filename,
				);
			}
		}
	}

	function bak_checkSizes($item, $dir, $tmbdir, $sizes, $watermark='', $crop=false, $thumbName='') {
		foreach ( $sizes as $key => $val ) {
			$tmbdir = utl::checkFolder($tmbdir);
			$filename = $tmbdir.'/' . ($this->_renameFileNameSize ? ('s_'.$val['x'].'x'.$val['y'].'_') : "").($thumbName!='' ? $thumbName : $item);
			if(!file_exists($filename) || $thumbName!='') {
				$wImageNew = $val['x']*1;
				$hImageNew = $val['y']*1;
				$areaNew = $wImageNew*$hImageNew;
				$whAttitudeNew = $wImageNew/$hImageNew;
				$tSize = new tbox($wImageNew, $hImageNew);
				$tMode = tFace::THUMBNAIL_INSET;
				$imagine = new thumbs();
				$thumb = $imagine->open($dir.'/'.$item);
				$sizeImage = $thumb->getSize();
				$wImage = $sizeImage->getWidth();
				$hImage = $sizeImage->getHeight();
				$areaOrig = $wImage*$hImage;
				// >1 - вертикальное изображение
				// <1 - горизонтальное отображение изображение
				// =1 - квадратное отображение
				$whAttitude = $wImage/$hImage;
//				switch(true) {
//					case $whAttitude>1:
//						$cropSize =
//						break;
//					case $whAttitude<1:
//						break;
//					default:
//
//				}
//
//				utl::p($whAttitude);
//				utl::p($whAttitudeNew);
//				exit;

				if(isset($this->_watermarks[$watermark])
					&& $areaNew>62500
//					&& !$crop
					&& (($val['x']*1)!=460 && ($val['y'])*1!=460)
				) {
					$wmImagePath = ((!is_array($this->_watermarks[$watermark])) ? $this->_watermarks[$watermark] : ( (isset($this->_watermarks[$watermark]['path']) ? $this->_watermarks[$watermark]['path'] : '')));
					$wmPositionY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][0] : 'top' );
					$wmPositionX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][1] : 'left' );
					$wmSize = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['size'])) ? $this->_watermarks[$watermark]['size'] : array(100, '%') );
					$wmOffsetY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsety'])) ? $this->_watermarks[$watermark]['offsety'] : array(0, 'px') );
					$wmOffsetX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsetx'])) ? $this->_watermarks[$watermark]['offsetx'] : array(0, 'px') );

					if(file_exists($wmImagePath)) {
						$watermark = $imagine->open($wmImagePath);
						$wmOriginalSize = $watermark->getSize();
						$wmSizeW = $wmOriginalSize->getWidth();
						$wmSizeH = $wmOriginalSize->getHeight();
						$wmAreaOrig = $wmSizeW*$wmSizeH;

//						if( $areaOrig < $wmAreaOrig ) {
							$wmAreaNew = $wmAreaOrig*$wmSize/100;
							$wmSizeNewW =
//							 thumbnail(new Box(1280, 1024))
//						}
						utl::p($wmImagePath, 'wmImagePath');
						utl::p($wmPositionY, 'wmPositionY');
						utl::p($wmPositionX, 'wmPositionX');
						utl::p($wmSize, 'wmSize');
						utl::p($wmOffsetY, 'wmOffsetY');
						utl::p($wmOffsetX, 'wmOffsetX');
						exit;
					}
//					$watermark = $imagine->open($this->_watermarks[$watermark]);
//					$size      = $thumb->getSize();
//					$wSize     = $watermark->getSize();
//					$bottomRight = new tpoint($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());
//					$thumb->paste($watermark, $bottomRight);
				}
//				$thumb = $thumb->thumbnail($tSize, $tMode);
				if($crop) {
//					$pointLeft = 10; // ($wImage-$wImageNew)/2;
//					$pointTop = 10; //($hImage-$hImageNew)/2;
////					utl::p( round($pointLeft) );
////					utl::p( round($pointTop) );
////					utl::p( $tSize );
////					utl::p( round($pointTop) );
////					exit;
//					$tSize = new tbox(100, 100);
//					$thumb = $thumb->crop(new tpoint($pointLeft, $pointTop), $tSize);
				}
				$thumb
					->save($filename)
				;
				return array(
					'sourceFilePath' => $dir.'/'.$item,
					'destinationFilePath' => $filename,
				);
			} else {
				return array(
					'sourceFilePath' => $dir.'/'.$item,
					'destinationFilePath' => $filename,
				);
			}
		}
	}
	//-- Не удалять функцию. Авось пригодится
	function Resize($file, $dir, $tn_w, $tn_h, $folderthumb='')
	{
		$quality = 100;
		$wmsource = true;

		$DOCUMENT_ROOT = CMS_FOLDER; // серверный путь до корня сайта
		$HTTP_FILES_PATH = $dir . '/';
//		$FILES_FOLDER = $DOCUMENT_ROOT . $HTTP_FILES_PATH; // полный путь до папки с картинками

		if(!is_dir($HTTP_FILES_PATH)) {
			if(is_dir( $newDir = str_replace('//', '/', $DOCUMENT_ROOT.$HTTP_FILES_PATH))) {
				$HTTP_FILES_PATH  = $newDir;
			}
		}
		if(is_file($dir.DIRECTORY_SEPARATOR.$file)) {
			$source_image = $dir.DIRECTORY_SEPARATOR .$file;
		} else {
			if(is_file($HTTP_FILES_PATH.$file)) {
				$source_image = $HTTP_FILES_PATH.$file;
			} else {
				$source_image = $file;
			}
		}
		$destFolder = utl::checkFolder( $folderthumb !='' ? $folderthumb :  $HTTP_FILES_PATH . "thumb");
		$destination = $destFolder . "/s_" . $tn_w . 'x' . $tn_h . '_' . $file; // путь до новой картинки в кэш-папке

		$info = getimagesize ( $source_image );
		$imgtype = image_type_to_mime_type ( $info[2] );

		// ssuming the mime type is correct
		switch ($imgtype) {
			case 'image/jpeg' :
				$source = imagecreatefromjpeg ( $source_image );
				break;
			case 'image/gif' :
				$source = imagecreatefromgif ( $source_image );
				break;
			case 'image/png' :
				$source = imagecreatefrompng ( $source_image );
				break;
			//default :
			//die ( 'Invalid image type.' );
		}

		// figure out the dimensions of the image and the dimensions of the desired thumbnail
		$src_w = imagesx ( $source );
		$src_h = imagesy ( $source );

		$areaReal = $src_w*$src_h;
		$areaNew = $tn_w*$tn_h;
		if($areaNew < $areaReal) {
			// o some math to figure out which way we'll need to crop the image
			// o get it proportional to the new size, then crop or adjust as needed

			$x_ratio = $tn_w / $src_w;
			$y_ratio = $tn_h / $src_h;

			// die($x_ratio);

			if (($src_w <= $tn_w) && ($src_h <= $tn_h)) {
				$new_w = $src_w;
				$new_h = $src_h;
			} elseif (($x_ratio * $src_h) < $tn_h) {
				$new_h = ceil($x_ratio * $src_h);
				$new_w = $tn_w;
			} else {
				$new_w = ceil($y_ratio * $src_w);
				$new_h = $tn_h;
			}

			//die();

			$newpic = imagecreatetruecolor(round($new_w), round($new_h));
			imagecopyresampled($newpic, $source, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
			$final = imagecreatetruecolor($tn_w, $tn_h);
//utl::p($imgtype);
//exit;
			if ($imgtype == 'image/png') { //  для png
				//imagealphablending($source, false);
				//imagesavealpha($source, true);

				//imagealphablending($newpic, false);
				//imagesavealpha($newpic, true);

				imagealphablending($final, false);
				imagesavealpha($final, true);
			}

			$backgroundColor = imagecolorallocate($final, 255, 255, 255);

			if ($imgtype == 'image/png') {
				$backgroundColor = imagecolorallocatealpha($final, 255, 255, 255, 0);
			}

			imagefill($final, 0, 0, $backgroundColor);
			// imagecopyresampled($final, $newpic, 0, 0, ($x_mid - ($tn_w / 2)), ($y_mid - ($tn_h / 2)), $tn_w, $tn_h, $tn_w, $tn_h);
			imagecopy($final, $newpic, (($tn_w - $new_w) / 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);
		} else {
			$tn_w = $src_w;
			$tn_h = $src_h;
			$final = $source;
		}

		$wmsource = CMS_FOLDER.'upload/shop_3/watermarks/shop_watermark_3.png';
//		\HCMS\Utils::p($wmsource, '1');
		// f we need to add a watermark
		if ($wmsource && $areaNew>62500) {
			// ind out what type of image the watermark is
			$info = getimagesize ( $wmsource );
			$imgtype = image_type_to_mime_type ( $info [2] );

			// ssuming the mime type is correct
			switch ($imgtype) {
				case 'image/jpeg' :
					$watermark = imagecreatefromjpeg ( $wmsource );
					break;
				case 'image/gif' :
					$watermark = imagecreatefromgif ( $wmsource );
					break;
				case 'image/png' :
					$watermark = imagecreatefrompng ( $wmsource );
					break;
				//default :
				//die ( 'Invalid watermark type.' );
			}

			// f we're adding a watermark, figure out the size of the watermark
			// nd then place the watermark image on the bottom right of the image
			$wm_w = imagesx ( $watermark );
			$wm_h = imagesy ( $watermark );
			imagecopy ( $final, $watermark, $tn_w - $wm_w, $tn_h - $wm_h, 0, 0, $tn_w, $tn_h );
		}
		/*
		if (imagejpeg ( $final, $destination, $quality )) {
			return true;
		}
		*/
		if ($imgtype == 'image/png') {
			imagepng($final, $destination); // новое png
		} else {
			imagejpeg($final, $destination, $quality); // или jpg
		}

		return array(
			'sourceFilePath' => $source_image,
			'destinationFilePath' => $destination,
		);
	}

}

?>