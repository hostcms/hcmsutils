<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\FB;

use HCMS\Utils as utl;
use HCMS\Images\XResize as resize;
use Facebook\Facebook as Fb;

class Post
{
	protected $_app_id;
	protected $_app_secret;
	protected $_token;
	protected $_name;
	protected $_default_graph_version = 'v2.5';
	protected $_comment = '';
	protected $_hashtags = array();
	protected $_fb;
	protected $_http_link = '';

	function __construct($appID, $appSecret, $token)
	{
		$this->_app_id = $appID;
		$this->_app_secret = $appSecret;
		$this->_token = $token;
		$this->_fb = new Fb([
			'app_id' => $this->_app_id,
			'app_secret' => $this->_app_secret,
			'default_graph_version' => $this->_default_graph_version,
			'default_access_token' => $this->_token,
		]);
	}

	public static function createInstance($appID, $appSecret, $token)
	{
		return new self($appID, $appSecret, $token);
	}

	/**
	 * @return string
	 */
	public function getHttpLink()
	{
		return $this->_http_link;
	}

	/**
	 * @param string $http_link
	 */
	public function setHttpLink($http_link)
	{
		$this->_http_link = $http_link;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->_name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->_name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->_comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment($comment)
	{
		$this->_comment = $comment;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getHashtags()
	{
		return $this->_hashtags;
	}

	public function addHashTag($hTag)
	{
		$this->_hashtags[] = str_replace(' ', '', $hTag);
		return $this;
	}

	public function addHashTags($hTags)
	{
		foreach ($hTags as $hTag) {
			$this->addHashTag($hTag);
		}
		return $this;
	}

	public function sendPhoto($filename, $filepath, $href)
	{
		$rs = new resize();
//		$fResult = $rs->Resize($filename, $filepath, 640, 640);
		$fResult = array(
//			'sourceFilePath' => CMS_FOLDER.'upload/shop_3/2/9/4/item_294816/shop_items_catalog_image294816.jpg',
			'destinationFilePath' => $filepath . '/' . $filename
		);
		if (is_array($fResult) && isset($fResult) && is_file($fResult['destinationFilePath'])) {
			try {
				$res = $this->_fb->post('/me/feed',
					array(
						'message' => $this->_getText(),
						'name' => $this->_name,
						'picture' => $href,
					),
					$this->_token
				);
				utl::p(json_decode($res->getBody()));
			} catch (Exception $e) {
				utl::p($e->getMessage());
			}
		}
	}

	private function _getText()
	{
		$oText = $this->_comment;
		foreach ($this->_hashtags as $hTag) {
			$oText .= ' #' . $hTag;
		}
		$this->_http_link != '' && $oText .= "\nПосмотреть подробнее и купить данный товар Вы можете по адресу\nhttps://universalmotors.ru/" . $this->_http_link;
		return $oText;
	}
}