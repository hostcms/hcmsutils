<?php

/**
 * Created by PhpStorm.
 * User: AlexeyKuznetsov
 * Date: 21.05.2015
 * Time: 8:44
 */
namespace HCMS\GeoIP;

use Core_QueryBuilder;
use Core_Valid;
use Core_Cache;
use Core;
use Core_Http;
use stdClass;
use Core_Array;
use HCMS\Utils as utl;
use Sonrisa\Component\RestfulClient\RestfulClient;
use HCMS\Sphinx\Client as sphinx;
use Core_Entity;


class Geo
{
	/**
	 * The singleton instances.
	 * @var mixed
	 */
	static public $instance = NULL;


	/**
	 * Cache name
	 * @var string
	 */
	protected $_cacheName = 'default';

	/**
	 * Run-time кеш гео-данных
	 * @var array
	 */
	protected $_geodata = array();

	/**
	 * Run-time кеш гео-данных
	 * @var array
	 */
	protected $_geoDataLocal = null;

	function __construct()
	{
		$this->_geoDataLocal = (object) array(
//			  'countryId' => 175
//			, 'countryName' => 'Россия'
		//	, 'locationId' => 7700000000000
			 'locationName' => 'Москва'
	//		, 'cityId' => 1
	//		, 'cityName' => 'Москва'
			, 'centerMaps' => '[55.7540,37.6204]'
			, 'zoom' => 7
//			, 'zip' => 101000
//			, 'zip_from' => 101000
//			, 'zip_to' => 143350
			, 'data' => 'SELECT'
			, 'url' => 'msk.buyonfarm.ru'
			, 'aliace_id' => 360
			, 'aliace_parent_id' => 6
			, 'aliace_type' =>2 //0-страна, 1-область, 2-город
			, 'aliace_meta' => '45a9d0a3e58f687d'
		);
	}

	public function setCookie($url= ''){
		if ($url !=""){
			$cookieName = 'geoips_'.implode('_', array_slice(explode('.',$url), -2));
			$cookieDomain = implode('.', array_slice(explode('.',$url), -2));

			$oAliases = Core_Entity::factory('Site_Alias');
			$oAlias = $oAliases->getByName('*.'.$url);
			//utl::v($oAlias);

			if (isset($oAlias)){
				$aliasId = $oAlias->id *1;
				$aliasParentId = $oAlias->parent_id *1;
				$aliace_meta = $oAlias->meta;
//				utl::v($aliasId);
//				utl::v($aliasParentId);

				$oCities = Core_Entity::factory('Shop_Country_Location_City');
				$oCities
					->queryBuilder()
					->where('shop_country_location_cities.aliace_id', '=', $aliasId)
				;

				$oCity = $oCities->findAll(FALSE);
//				utl::p($oCity);

				//проверим города
				if(isset($oCity[0])){
					$aCity = $oCity[0]->toArray();
//					utl::p($aCity);
					$this->_geoDataLocal->locationName = $aCity['name'];
					$this->_geoDataLocal->centerMaps = $aCity['center_maps'];
					$this->_geoDataLocal->url = $aCity['aliace'];
					$this->_geoDataLocal->zoom = intval($aCity['zoom_map']);
					$this->_geoDataLocal->aliace_id = intval($aCity['aliace_id']);
					$this->_geoDataLocal->aliace_parent_id = $aliasParentId;
					$this->_geoDataLocal->data = 'SELECT';
					$this->_geoDataLocal->aliace_type = 2;
					$this->_geoDataLocal->aliace_meta = $aliace_meta;


				//проверим области
				}else{
					$oLocations = Core_Entity::factory('Shop_Country_Location');
					$oLocations
						->queryBuilder()
						->where('shop_country_locations.alias_id', '=', $aliasId)
					;

					$oLocation = $oLocations->findAll(FALSE);
//					utl::p($oLocation);

					if(isset($oLocation[0])){
						$aLocation = $oLocation[0]->toArray();
//						utl::p($aLocation);
						$this->_geoDataLocal->locationName = $aLocation['name'];
						$this->_geoDataLocal->centerMaps = $aLocation['center_maps'];
						$this->_geoDataLocal->url = $url;
						$this->_geoDataLocal->zoom = intval($aLocation['zoom_map']);
						$this->_geoDataLocal->aliace_id = intval($aLocation['alias_id']);
						$this->_geoDataLocal->aliace_parent_id = $aliasParentId;
						$this->_geoDataLocal->data = 'SELECT';
						$this->_geoDataLocal->aliace_type = 1;
						$this->_geoDataLocal->aliace_meta = $aliace_meta;
					}
				}
			}


//			utl::p($this->_geoDataLocal);
			$_COOKIE[$cookieName] = serialize($this->_geoDataLocal);
			SetCookie($cookieName, $_COOKIE[$cookieName] , time() + 31536000, '/', $cookieDomain);
			//показывать или нет окно подтверждения выбора города
			SetCookie('geoconfirm_'.implode('_', array_slice(explode('.',$url), -2)), 0 , time() + 31536000, '/', $cookieDomain);
		}
		return $this->_geoDataLocal;
	}

	public function getDataCookie($dns= ''){
		$array_cook = array();
		if ($dns !=""){
			$coockie_name = 'geoips_'.implode('_', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2));
			$coockie_domaine = implode('.', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2));

			if (isset($_COOKIE[$coockie_name])){
				$array_cook = unserialize($_COOKIE[$coockie_name]);
				$array_cook->data = 'COOCKIE';
				SetCookie($coockie_name, serialize($array_cook), time() + 31536000, '/', $coockie_domaine);
			}
		}
		return $array_cook;
	}

//	public function updateCookie($zip = 0){
////		utl::p($zip);
//
//		$coockie_name = 'geoip_'.implode('_', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2));
//		$coockie_domaine = implode('.', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2));
//
//		$sphinxRegions = sphinx::createInstance('i_get_regions_all', 10000, 10000, 0);
//
//		//для dev обрезаем выборку до тестовых записей
//		if(strripos($_SERVER['SERVER_NAME'], 'dev') !== false){
//			$sphinxRegions->addFilter(array('item_id'=>array(928, 929, 4502)));
//		}
//
//		$result = $sphinxRegions->execute('');
//
//		if(isset($result['matches'])) {
//			foreach ($result['matches'] as $key=>$val){
//				if($val['attrs']['zip_from'] <= $zip && $val['attrs']['zip_to'] >= $zip){
//
//					$this->_geoDataLocal->countryId = intval($val['attrs']['shop_country_id']);
//					//		$this->_geoDataLocal->countryName =$val['attrs']['url'];
////							$this->_geoDataLocal->locationId = 0;
//					$this->_geoDataLocal->locationName = $val['attrs']['regions'];
////							$this->_geoDataLocal->cityId = intval($val['attrs']['url']);
////							$this->_geoDataLocal->cityName = $val['attrs']['url'];
//					$this->_geoDataLocal->centerMaps = $val['attrs']['center_maps'];
//					$this->_geoDataLocal->url = $val['attrs']['url'];
//					$this->_geoDataLocal->zoom = intval($val['attrs']['zoom_map']);
//					$this->_geoDataLocal->zip = intval($zip);
//					$this->_geoDataLocal->zip_from = intval($val['attrs']['zip_from']);
//					$this->_geoDataLocal->zip_to = intval($val['attrs']['zip_to']);
//					$this->_geoDataLocal->data = 'COOCKIE';
//					break;
//				}
//			}
//		}
//
////utl::p($this->_geoDataLocal);
//		SetCookie($coockie_name, serialize($this->_geoDataLocal) , time() + 31536000, '/', $coockie_domaine);
//
//		return 1;
//	}

	public function getDataIP($ip = ''){
		$cookieName = 'geoips_'.implode('_', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2));
		$cookieDomain = implode('.', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2));
		$sCacheKey = 'geoips' . $ip;

		// короткое run-time кеширование, для повторных запросов того же ip в рамках одного выполнения скрипта
		if (isset($this->_geodata[$sCacheKey])) {
			return $this->_geodata[$sCacheKey];
		}

		// длительное системное кеширование
		if (Core::moduleIsActive('cache')) {
			$oCore_Cache = Core_Cache::instance(Core::$mainConfig['defaultCache']);
			$inCache = $oCore_Cache->get($sCacheKey, $this->_cacheName);

			if (!is_null($inCache)) {
				$this->_geodata[$sCacheKey] = $inCache === false ? null : $inCache;
				return $inCache;
			}
		}

		// запишем в локальный кеш пустое значение
		// если что-то найдется для данного ip, информация в локальном кеше будет обновлена
		$this->_geodata[$sCacheKey] = null;

		//определим zip по ip
		$db = new \IP2Location\Database(CMS_FOLDER.'libs/ip2location/base.bin', \IP2Location\Database::FILE_IO);
		$records = $db->lookup($ip, \IP2Location\Database::ALL);
//		utl::p($records);
		//если zipCode пустой, ищем его по координатам в Google
		if (!isset($records['zipCode']) || $records['zipCode']*1 <=0 || is_null($records['zipCode'])) {
			if(!is_null($records['longitude']) && !is_null($records['latitude']) && $records['longitude']*1 >0 && $records['latitude']*1 >0 ){
				$response = Unirest\Request::get("http://maps.googleapis.com/maps/api/geocode/json", $headers = array(
					"Accept" => "application/json",
					"Accept-Encoding" => "gzip, deflate, sdch",
					"Accept-Language" => "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
				), $parameters = array(
					'latlng' => $records['latitude'].','.$records['longitude'],
					'sensor' => 'false',
					'language' => 'en',
				));

				if (isset($response->body->results[0]->address_components)){
					foreach($response->body->results[0]->address_components as $key=>$val){
						if ($val->types[0] == 'postal_code'){
							$records['zipCode'] = $val->long_name;
						}
					}
				}
			}
		}

		if (isset($records['zipCode']) && !is_null($records['zipCode'])) {
			$zip = $records['zipCode']*1;
//			utl::p($zip);
			//сначала проверим таблицу городов
			$oCities = Core_Entity::factory('Farm_Shop_Country_Location_City_Zip');
			$oCity = $oCities->getAllByZip($zip);
//			utl::p($oCity);
//			utl::p($oCity[0]->toArray());
//			utl::p($oCity[1]->toArray());
			//нашли переписываем объект в город
			if(isset($oCity) && $oCity != null){

				//Для dev берем 2 элемент
				if(strripos($_SERVER['HTTP_HOST'], 'dev.') !== false){
					$aCity = $oCity[1]->shop_country_location_city->findAll()[0]->toArray();
				}else{
					$aCity = $oCity[0]->shop_country_location_city->findAll()[0]->toArray();
				}
//				utl::p($aCity);

				$oAliases = Core_Entity::factory('Site_Alias');
				$oAlias = $oAliases->getById(intval($aCity['aliace_id']));
				$aliasParentId = $oAlias->toArray()['parent_id'];
				$aliace_meta = $oAlias->meta;
//				utl::p($aliasParentId);

				$this->_geoDataLocal->locationName = $aCity['name'];
				$this->_geoDataLocal->centerMaps = $aCity['center_maps'];
				$this->_geoDataLocal->url = $aCity['aliace'];
				$this->_geoDataLocal->zoom = intval($aCity['zoom_map']);
//				$this->_geoDataLocal->zip = intval($zip);
//				$this->_geoDataLocal->zip_from = intval($zip);
//				$this->_geoDataLocal->zip_to = intval($zip);
				$this->_geoDataLocal->aliace_id = intval($aCity['aliace_id']);
				$this->_geoDataLocal->aliace_parent_id = $aliasParentId;
				$this->_geoDataLocal->data = 'SELECT';
				$this->_geoDataLocal->aliace_type = 2;
				$this->_geoDataLocal->aliace_meta = $aliace_meta;

			//не нашли переписываем объект в область
			} else{
				$sphinxRegions = sphinx::createInstance('i_get_regions_all', 10000, 10000, 0);

				//для dev обрезаем выборку до тестовых записей
				if(strripos($_SERVER['SERVER_NAME'], 'dev') !== false){
					$sphinxRegions->addFilter(array('item_id'=>array(928, 929, 4502)));
				}

				$result = $sphinxRegions->execute('');

//				utl::p($result['matches']);

				//ДЕЛАЕМ объект и куку
				if(isset($result['matches'])) {
					foreach ($result['matches'] as $key=>$val){
						if($val['attrs']['zip_from'] <= $zip && $val['attrs']['zip_to'] >= $zip){
							$oAliases = Core_Entity::factory('Site_Alias');
							$oAlias = $oAliases->getByName('*.'.$val['attrs']['url']);
							$aliasId = $oAlias->toArray()['id']*1;
							$aliasParentId = $oAlias->toArray()['parent_id']*1;
							$aliace_meta = $oAlias->meta;
//							utl::p($aliasParentId);

//							$this->_geoDataLocal->countryId = intval($val['attrs']['shop_country_id']);
							//		$this->_geoDataLocal->countryName =$val['attrs']['url'];
//							$this->_geoDataLocal->locationId = 0;
							$this->_geoDataLocal->locationName = $val['attrs']['regions'];
//							$this->_geoDataLocal->cityId = intval($val['attrs']['url']);
//							$this->_geoDataLocal->cityName = $val['attrs']['url'];
							$this->_geoDataLocal->centerMaps = $val['attrs']['center_maps'];
							$this->_geoDataLocal->url = $val['attrs']['url'];
							$this->_geoDataLocal->zoom = intval($val['attrs']['zoom_map']);
//							$this->_geoDataLocal->zip = intval($zip);
//							$this->_geoDataLocal->zip_from = intval($val['attrs']['zip_from']);
//							$this->_geoDataLocal->zip_to = intval($val['attrs']['zip_to']);
							$this->_geoDataLocal->aliace_id = $aliasId;
							$this->_geoDataLocal->aliace_parent_id = $aliasParentId;
							$this->_geoDataLocal->data = 'SELECT';
							$this->_geoDataLocal->aliace_type = 1;
							$this->_geoDataLocal->aliace_meta = $aliace_meta;
							break;
						}
					}
				}
			}
		}
		//$this->_geoDataLocal->show_confirm_window = 1;
		//показывать или нет окно подтверждения выбора города
		SetCookie('geoconfirm_'.implode('_', array_slice(explode('.',$_SERVER['HTTP_HOST']), -2)), 1 , time() + 31536000, '/', $cookieDomain);
		$this->_geodata[$sCacheKey] = $this->_geoDataLocal;

		if (Core::moduleIsActive('cache')) {
			// вместо null в системный кеш сохраняем false, иначе при чтении из кеша нельзя будет различить
			// отсутствие данных в кеше и отсутствие данных для запрошенного ip
			$cacheContent = is_null($this->_geodata[$sCacheKey]) ? false : $this->_geodata[$sCacheKey];
			$oCore_Cache->set($sCacheKey, $cacheContent, $this->_cacheName);
		}

		$_COOKIE[$cookieName] = serialize($this->_geodata[$sCacheKey]);
		SetCookie($cookieName, $_COOKIE[$cookieName] , time() + 31536000, '/', $cookieDomain);

		return unserialize($_COOKIE[$cookieName]);
	}
}