<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Xml\Arr;

//use LSS\Array2XML as axml;
use HCMS\Utils as utl;

class Entity {
	protected $_arConverts;
	protected $_rootPath;

	function __construct($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '') {
		$this->_rootPath = $rootPath;

		if($key != '') {
			if($attrkey != '') {
				foreach ($inArray as $arKey=>$arVal){
					$inArray[$arKey]['@attributes'] = array($attrkey => $arVal[$attrkey]);
				}
			}
			$this->_arConverts[$key] = $inArray;
		} else {
			$this->_arConverts = $inArray;
		}
	}

	/**
	 * Build entity XML
	 *
	 * @return string
	 */
	public function getXml()
	{
/*		$xml = str_replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "", axml::createXML($this->_rootPath, $this->_arConverts)->saveXML());*/
		$xml = preg_replace("/<\?xml version=\"1.0\"( encoding=\"UTF-8\")?\?>/ui", "", \Spatie\ArrayToXml\ArrayToXml::convert(
			$this->_arConverts,
			$this->_rootPath,
			false,
			"UTF-8",
			"1.0",
			[]
		)
		);

		return $xml;
	}
}