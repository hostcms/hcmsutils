<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Informationsystem;

use Core_Entity;
use Core_File;
use Core_QueryBuilder;
use \HCMS\Utils as utl;
use \LSS\Array2XML as axml;

class Groups {
    protected $_groupModel = null;
    protected $_isId = 0;
    protected $_is = null;
	protected $_treeAll = array();
	protected $_treeIds = array();
	protected $_addWhereLists = array();
	protected $_incLevel = 0;
	protected $_showInActive = FALSE;

	function __construct($isId, $is=null, $shop_id=null) {
		$this->_isId = $isId;
		(is_null($is) || is_null($this->_isId)) && $this->_is = Core_Entity::factory('Informationsystem', $isId);
		$this->_shop = Core_Entity::factory('Shop', $shop_id);
		$this->_groupModel = Core_Entity::factory('Informationsystem_Group');
		$this->_groupModel
			->queryBuilder()
			->where('informationsystem_id', '=', $this->_isId)
			->where('deleted', '=', 0)
			->where('active', '=', 1)
		;
	}

	public static function createInstance($isId, $is=null) {
		if(is_null($is)) {
			$is =  Core_Entity::factory('Informationsystem', $isId);
		}
		return new self($isId, $is);
	}

	public function getGroupObjectByPath($groupPath, $shopID=0)
	{
		$returnes = NULL;
		$groups = array_values(array_diff(explode('/', $groupPath), array('')));
		$shop = NULL;

		if(count($groups)>0 && $shopID*1 > 0) {
			$shop = Core_Entity::factory('Shop')->getById($shopID, FALSE);
			if($shop->id > 0) {
				$parentId = 0;
				$parentGroup = NULL;
				foreach($groups as $key => $subGroupPath) {
					$group = Core_Entity::factory('Shop_Group');
					$group
						->queryBuilder()
						->where('deleted', '=', 0)
						->where('active', '=', 1)
						->where('shop_id', '=', $shop->id*1)
						->where('parent_id', '=', (is_null($parentGroup) ? 0 : $parentGroup->id))
					;
					$group = $group->getByPath($subGroupPath, FALSE);

					if(is_null($group)) {
						$itemModel = Core_Entity::factory('Shop_Item');
						$itemModel
							->queryBuilder()
							->where('deleted', '=', 0)
							->where('active', '=', 1)
							->where('shop_id', '=', $shop->id)
							->where('shop_group_id', '=', (is_null($parentGroup) ? 0 : $parentGroup->id));
						$group = $itemModel->getByPath($subGroupPath, FALSE);
					}
					$parentGroup = $group;
					if(is_null($parentGroup)) return false;

					if($key==0) {
						$this->_groupModel->queryBuilder()->where('parent_id', '=', $parentId);
						$returnes = $this->_groupModel->getByPath($group->path, FALSE);
					} else {
						$returnes = $returnes->informationsystem_groups->getByPath($group->path, FALSE);
					}

					if (is_null($returnes) && $group->name!='') {
						$returnesModel=Core_Entity::factory('Informationsystem_Group');
						$returnesModel->parent_id = $parentId;
						$returnesModel->name = $group->name;
						$returnesModel->path = $group->path;
						$returnesModel->informationsystem_id = $this->_isId;
						$returnes = $returnesModel->save();
					}
					$parentId = $returnes->id;
				}
			}
		}
		return $returnes;
	}

	public static function getGroupByPath($groupPath, $parentGroupID = false, $isId = 3)
	{
		$mGroups = Core_Entity::factory('Informationsystem_Group');
		$mGroups
				->queryBuilder()
				->where('informationsystem_id', '=', $isId)
				->where('deleted', '=', 0)
				->where('active', '=', 1)
		;
		if ($parentGroupID !== false) {
				$mGroups
						->queryBuilder()
						->where('parent_id', '=', $parentGroupID);
		}
		$oGroup = NULL;
		(trim($groupPath) != "") && $oGroup = $mGroups->getByPath($groupPath);

		return $oGroup;
	}

	public function getGroupsTree($groupIDs=array(-1)) {
		$this->_treeAll = array();
		$arWhere = array(-1);
		$addQBPath = '';
		if(count($this->_addWhereLists)>0) {
			$addQBArray = array();
			$addQBPath = '/';
			foreach($this->_addWhereLists as $addWhereListItem) {
				$addQBArray[] = $addWhereListItem->id*1;
				$addQBPath .= $addWhereListItem->path.'/';
				$this->_incLevel++;
			}
			if(count($addQBArray)>0) {
				$cqbWhere = Core_QueryBuilder::select(array(Core_QueryBuilder::expression("CONCAT(si.informationsystem_group_id, ',')"), 'gid'))
					->from(array('property_value_ints', 'pvi'))
					->join(array('informationsystem_items', 'si'), 'pvi.entity_id', '=', 'si.id')
					->join(array('list_items', 'li'), 'li.id', '=', 'pvi.value', array(
						array('AND' => array('li.id', 'IN', $addQBArray))
					))
					->groupBy('si.informationsystem_group_id')
				;
				$arWhere += utl::getArrayValuesFromArrays($cqbWhere->execute()->asAssoc()->result(), 'gid');
			}
		}
		$coreQueryBuilderTreeUp = Core_QueryBuilder::select(array('g.path', 'path'))
			->select(array('g.url', 'url'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 1)*1"), 'g1'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 2)*1"), 'g2'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 3)*1"), 'g3'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 4)*1"), 'g4'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 5)*1"), 'g5'))
			->from(array('getIsGroupsIerarchyDown', 'g'))
			->where('g.id', 'IN', $groupIDs)
		;
		count($this->_addWhereLists)>0 && $coreQueryBuilderTreeUp->where('split_string(g.back_path, \'/\', 1)*1', 'IN', $arWhere);
		$coreQueryBuilderTree = Core_QueryBuilder::select('gr.path')
//            ->select('gr.url')
			->select(array( $addQBPath=='' ? 'gr.url' : Core_QueryBuilder::expression("REPLACE(`gr`.`url`, CONCAT('/',split_string(gr.url, '/', 2),'/'), CONCAT('/', split_string(gr.url, '/', 2),'".$addQBPath."'))"), 'url'))
			->select(array("sg1.id", "sg1_id"))
			->select(array("sg1.parent_id", "sg1_parent_id"))
			->select(array("sg1.name", "sg1_name"))
			->select(array("sg1.items_count", "sg1_items_count"))
			->select(array("sg1.subgroups_total_count", "sg1_subgroups_total_count"))
			->select(array("sg1.items_total_count", "sg1_items_total_count"))
			->select(array("sg2.id", "sg2_id"))
			->select(array("sg2.parent_id", "sg2_parent_id"))
			->select(array("sg2.name", "sg2_name"))
			->select(array("sg2.items_count", "sg2_items_count"))
			->select(array("sg2.subgroups_total_count", "sg2_subgroups_total_count"))
			->select(array("sg2.items_total_count", "sg2_items_total_count"))
			->select(array("sg3.id", "sg3_id"))
			->select(array("sg3.parent_id", "sg3_parent_id"))
			->select(array("sg3.name", "sg3_name"))
			->select(array("sg3.items_count", "sg3_items_count"))
			->select(array("sg3.subgroups_total_count", "sg3_subgroups_total_count"))
			->select(array("sg3.items_total_count", "sg3_items_total_count"))
			->select(array("sg4.id", "sg4_id"))
			->select(array("sg4.parent_id", "sg4_parent_id"))
			->select(array("sg4.name", "sg4_name"))
			->select(array("sg4.items_count", "sg4_items_count"))
			->select(array("sg4.subgroups_total_count", "sg4_subgroups_total_count"))
			->select(array("sg4.items_total_count", "sg4_items_total_count"))
			->select(array("sg5.id", "sg5_id"))
			->select(array("sg5.parent_id", "sg5_parent_id"))
			->select(array("sg5.name", "sg5_name"))
			->select(array("sg5.items_count", "sg5_items_count"))
			->select(array("sg5.subgroups_total_count", "sg5_subgroups_total_count"))
			->select(array("sg5.items_total_count", "sg5_items_total_count"))
			->from(array($coreQueryBuilderTreeUp, 'gr'))
			->leftJoin(array('informationsystem_groups', 'sg1'), 'sg1.id', '=', 'gr.g1')
			->leftJoin(array('informationsystem_groups', 'sg2'), 'sg2.id', '=', 'gr.g2')
			->leftJoin(array('informationsystem_groups', 'sg3'), 'sg3.id', '=', 'gr.g3')
			->leftJoin(array('informationsystem_groups', 'sg4'), 'sg4.id', '=', 'gr.g4')
			->leftJoin(array('informationsystem_groups', 'sg5'), 'sg5.id', '=', 'gr.g5')
		;
		if(!$this->_showInActive) {
			$coreQueryBuilderTree
				->where(Core_QueryBuilder::expression("COALESCE(sg1.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg1.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg2.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg2.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg3.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg3.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg4.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg4.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg5.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg5.deleted, 0)"), '=', 0)
			;
		}
		$coreQueryBuilderTree
			->orderBy('sg1.sorting', 'ASC')
			->orderBy('sg2.sorting', 'ASC')
			->orderBy('sg3.sorting', 'ASC')
			->orderBy('sg4.sorting', 'ASC')
			->orderBy('sg5.sorting', 'ASC')
		;
//		utl::p($coreQueryBuilderTree->build());

		$groups = $coreQueryBuilderTree->execute()->asAssoc()->result();

		$tree = array();
		foreach($groups as $group) {
			$tree['category'][$group['sg1_id']]['@attributes'] = array(
				'id' => $group['sg1_id'],
				'parent_id' => $group['sg1_parent_id']
			);
			$tree['category'][$group['sg1_id']]['name'] = $group['sg1_name'];
			$tree['category'][$group['sg1_id']]['url'] = $this->_makeUrlField($group['url'], 1);
			$tree['category'][$group['sg1_id']]['items_count'] = $group['sg1_items_count'];
			$tree['category'][$group['sg1_id']]['subgroups_total_count'] = $group['sg1_subgroups_total_count'];
			$tree['category'][$group['sg1_id']]['items_total_count'] = $group['sg1_items_total_count'];
			if(!is_null($group['sg2_name'])) {
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['@attributes'] = array(
					'id' => $group['sg2_id'],
					'parent_id' => $group['sg2_parent_id']
				);
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['name'] = $group['sg2_name'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['url'] = $this->_makeUrlField($group['url'], 2);
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['items_count'] = $group['sg2_items_count'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['subgroups_total_count'] = $group['sg2_subgroups_total_count'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['items_total_count'] = $group['sg2_items_total_count'];
				if(!is_null($group['sg3_name'])) {
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['@attributes'] = array(
						'id' => $group['sg3_id'],
						'parent_id' => $group['sg3_parent_id']
					);
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['name'] = $group['sg3_name'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['url'] = $this->_makeUrlField($group['url'], 3);
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['items_count'] = $group['sg3_items_count'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['subgroups_total_count'] = $group['sg3_subgroups_total_count'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['items_total_count'] = $group['sg3_items_total_count'];
					if(!is_null($group['sg4_name'])) {
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['@attributes'] = array(
							'id' => $group['sg4_id'],
							'parent_id' => $group['sg4_parent_id']
						);
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['name'] = $group['sg4_name'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['url'] = $this->_makeUrlField($group['url'], 4);
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['items_count'] = $group['sg4_items_count'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['subgroups_total_count'] = $group['sg4_subgroups_total_count'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['items_total_count'] = $group['sg4_items_total_count'];
						if(!is_null($group['sg5_name'])) {
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['@attributes'] = array(
								'id' => $group['sg5_id'],
								'parent_id' => $group['sg5_parent_id']
							);
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['name'] = $group['sg5_name'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['url'] = $this->_makeUrlField($group['url'], 5);
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['items_count'] = $group['sg5_items_count'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['subgroups_total_count'] = $group['sg5_subgroups_total_count'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['items_total_count'] = $group['sg5_items_total_count'];
						}
					}
				}
			}
		}
		$treeForXML = array();
		if(isset($tree['category'])) {
			$index1 = 0;
			foreach($tree['category'] as $list1) {
				$treeForXML['category'][$index1]['@attributes'] = $list1['@attributes'];
				$treeForXML['category'][$index1]['name'] = $list1['name'];
				$treeForXML['category'][$index1]['url'] = $list1['url'];
				$treeForXML['category'][$index1]['items_count'] = $list1['items_count'];
				$treeForXML['category'][$index1]['subgroups_total_count'] = $list1['subgroups_total_count'];
				$treeForXML['category'][$index1]['items_total_count'] = $list1['items_total_count'];
				$index2 = 0;
				if(isset($list1['category'])) {
					foreach($list1['category'] as $list2) {
						$treeForXML['category'][$index1]['category'][$index2]['@attributes'] = $list2['@attributes'];
						$treeForXML['category'][$index1]['category'][$index2]['name'] = $list2['name'];
						$treeForXML['category'][$index1]['category'][$index2]['url'] = $list2['url'];
						$treeForXML['category'][$index1]['category'][$index2]['items_count'] = $list2['items_count'];
						$treeForXML['category'][$index1]['category'][$index2]['subgroups_total_count'] = $list2['subgroups_total_count'];
						$treeForXML['category'][$index1]['category'][$index2]['items_total_count'] = $list2['items_total_count'];
						if(isset($list2['category'])) {
							$index3 = 0;
							foreach ($list2['category'] as $list3) {
								$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['@attributes'] = $list3['@attributes'];
								$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['name'] = $list3['name'];
								$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['url'] = $list3['url'];
								$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['items_count'] = $list3['items_count'];
								$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['subgroups_total_count'] = $list3['subgroups_total_count'];
								$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['items_total_count'] = $list3['items_total_count'];
								if(isset($list3['category'])) {
									$index4 = 0;
									foreach ($list3['category'] as $list4) {
										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['@attributes'] = $list4['@attributes'];
										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['name'] = $list4['name'];
										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['url'] = $list4['url'];
										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['items_count'] = $list4['items_count'];
										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['subgroups_total_count'] = $list4['subgroups_total_count'];
										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['items_total_count'] = $list4['items_total_count'];
										if(isset($list4['category'])) {
											$index5 = 0;
											foreach ($list4['category'] as $list5) {
												$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['@attributes'] = $list5['@attributes'];
												$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['name'] = $list5['name'];
												$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['url'] = $list5['url'];
												$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['items_count'] = $list5['items_count'];
												$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['subgroups_total_count'] = $list5['subgroups_total_count'];
												$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['items_total_count'] = $list5['items_total_count'];
												$index5++;
											}
										}
										$index4++;
									}
								}
								$index3++;
							}
						}
						$index2++;
					}
				}
				$index1++;
			}
		}
		$this->_treeAll = $treeForXML;
		return $treeForXML;
	}

	public function fillTreeIDs($groupIDs=array(-1)) {
		if(!is_array($groupIDs)) {
			$groupIDs = array($groupIDs);
		}
		$this->getGroupsTree($groupIDs);
		return $this;
	}

	public function getTreeIds() {
		$this->_treeIds = $this->_processTreeIDs($this->_treeAll);
		return $this->_treeIds;
	}

	private function _processTreeIDs($mas)
	{
		$result = array();
		if (isset($mas['category'])) {
			foreach ($mas['category'] as $value) {
				array_push($result, $value['@attributes']['id']*1);
				if (isset($value['category'])) {
					$dop_mas = $this->_processTreeIDs($value);
					foreach ($dop_mas as $val) {
						array_push($result, $val*1);
					}
				}
			}
		}
		return $result;
	}

	private function _makeUrlField($url, $level) {
		$arURL = explode('/', $url);
		$returnsUrl = '/';
		foreach($arURL as $key=>$urlPart) {
			if(strlen($urlPart)>0) {
				if($key<$level+1+$this->_incLevel) {
					$returnsUrl.=$urlPart.'/';
				} else {
					break;
				}
			}
		}
		return $returnsUrl;
	}

	/**
	 * @return boolean
	 */
	public function isShowInActive()
	{
		return $this->_showInActive;
	}

	/**
	 * @param boolean $showInActive
	 */
	public function showInActive($showInActive=FALSE)
	{
		$this->_showInActive = $showInActive;
		return $this;
	}
}