<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Informationsystem;

use Core_Entity;
use Core_File;
use Core_QueryBuilder;
use HCMS\Utils as utl;

class Items {
	protected $_itemModel = null;
	protected $_informationsystemId = 0;
	protected $_informationsystem = null;

	function __construct($informationsystemId=1, $informationsystem=null) {
		$this->_informationsystemId = $informationsystemId;
		(is_null($informationsystem) || is_null($this->_informationsystemId)) && $this->_is = Core_Entity::factory('Informationsystem_Item', $informationsystem);
		$this->_itemModel = Core_Entity::factory('Informationsystem_Item');
		$this->_itemModel
			->queryBuilder()
			->where('informationsystem_id', '=', $this->_informationsystemId)
			->where('deleted', '=', 0)
			->where('active', '=', 1)
		;
	}

	public static function createInstance($informationsystemId, $informationsystem=null) {
		if(is_null($informationsystem)) {
			$informationsystem =  Core_Entity::factory('Informationsystem', $informationsystemId);
		}
		return new self($informationsystemId, $informationsystem);
	}

	public function getByPath($itemPath, $group_id=NULL)
	{
		(!is_null($group_id)) && $this->_itemModel->queryBuilder()->where('informationsystem_group_id', '=', $group_id);
		$oInformationsystemItem = NULL;
		(trim($itemPath) != "") && $oInformationsystemItem = $this->_itemModel->getByPath($itemPath);

		return $oInformationsystemItem;
	}
}