<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\VK;

use HCMS\Utils as utl;
use HCMS\Images\XResize as resize;
use BW\Vkontakte as Vk;

class Post {
	protected $_user_id;
	protected $_token;
	protected $_comment = '';
	protected $_hashtags = array();
	protected $_vk;
	protected $_http_link = '';
	protected $_item;

	function __construct($userID, $token) {
		$this->_user_id = $userID;
		$this->_token = $token;
		$this->_vk = new Vk();
		$accessToken = array (
			'access_token' => $token,
			'expires_in' => 0,
			'user_id' => $this->_user_id,
		);
		$this->_vk->setAccessToken($accessToken);
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->_item;
	}

	/**
	 * @param mixed $item
	 */
	public function setItem($item)
	{
		$this->_item = $item;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHttpLink()
	{
		return $this->_http_link;
	}

	/**
	 * @param string $http_link
	 */
	public function setHttpLink($http_link)
	{
		$this->_http_link = $http_link;
		return $this;
	}

	public static function createInstance($userID, $token) {
		return new self($userID, $token);
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->_comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment($comment)
	{
		$this->_comment = $comment;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getHashtags()
	{
		return $this->_hashtags;
	}

	public function addHashTag($hTag) {
		$this->_hashtags[] = str_replace(array(' ', '-'), '', $hTag);
		return $this;
	}

	public function addHashTags($hTags) {
		foreach($hTags as $hTag) {
			$this->addHashTag($hTag);
		}
		return $this;
	}

	public function sendPhoto($filename, $filepath) {
		$rs = new resize();
//		utl::p($filename);
//		utl::p($filepath);
//		$fResult = $rs->Resize($filename, $filepath, 640, 640);
		$fResult = array (
//			'sourceFilePath' => CMS_FOLDER.'upload/shop_3/2/9/4/item_294816/shop_items_catalog_image294816.jpg',
			'destinationFilePath' => $filepath.'/'.$filename
		);
		if(is_array($fResult) && isset($fResult) && is_file($fResult['destinationFilePath'])) {
			try {
				$photoUploadServer = $this->_vk->api('photos.getWallUploadServer', [
					'user_id' => $this->_user_id,
				]);
				if(isset($photoUploadServer['error'])) throw new Exception( $photoUploadServer['error']['error_msg'] );
				//-- получим сервер для загрузки фото --
				$uploadUrl = $photoUploadServer['upload_url'];
				$uploadedFile = new \CurlFile($fResult['destinationFilePath'],'file/exgpd','');
				$post_params = array("photo" => $uploadedFile);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $uploadUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_TIMEOUT,0);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)");
				$result = curl_exec($ch);
				$resp = json_decode($result, true);
				$resp['user_id'] = $this->_user_id;
				//-- Загрузим фото --
				$wallUploadPhoto = $this->_vk->api('photos.saveWallPhoto', $resp);
				if(isset($wallUploadPhoto['error'])) throw new Exception( $wallUploadPhoto['error']['error_msg'] );
				$mediaID = $wallUploadPhoto[0]['id'];
				$wallPost = $this->_vk->api('wall.post', [
					'owner_id' => $this->_user_id,
					'message' => $this->_getText(),
					'attachments' => 'photo'.$this->_user_id.'_'.$mediaID,
				]);
				if(isset($wallUploadPhoto['error'])) throw new Exception( $wallUploadPhoto['error']['error_msg'] );
				utl::p($wallPost);
			} catch (Exception $e) {
				utl::p($e->getMessage());
			}

		}
	}

	private function _getText() {
		$oText = strip_tags($this->_comment);
		foreach($this->_hashtags as $hTag) {
			$oText .= ' #'.$hTag;
		}
		(is_object($this->_item) && get_class($this->_item)=='Shop_Item_Model' && $this->_item->price>0) && $oText .= "\nЦена: ".number_format($this->_item->price, 0, '.',' ')."р.";
		$this->_http_link!='' && $oText .= "\nПосмотреть подробнее и купить данный товар Вы можете по адресу\nhttps://universalmotors.ru/".$this->_http_link;
		return $oText;
	}
}