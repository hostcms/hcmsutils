<?php
/**
 * Created by PhpStorm.
 * User: Tasha
 * Date: 06.04.2015
 * Time: 13:23
 */
namespace HCMS\Shop;

use HCMS\Utils as utl;
use Core_Entity;
use Core_QueryBuilder;
use Core_Array;
use LSS\Array2XML as axml;
use HCMS\Core\Xml\Arr\Entity as entity;

class Properties {
	protected $_itemModel = null;
	protected $_itemIDs = null;
	protected $_shopId = 0;
	protected $_shop = null;
	protected $_groups = array(-1);
	protected $_showToXML = null;
	protected $_showAllProperties = false;

	/**
	 * @return boolean
	 */
	public function isShowAllProperties()
	{
		return $this->_showAllProperties;
	}

	/**
	 * @param boolean $showAllProperties
	 */
	public function setShowAllProperties($showAllProperties)
	{
		$this->_showAllProperties = $showAllProperties;
	}

	/**
	 * @return null
	 */
	public function getShowToXML()
	{
		return $this->_showToXML;
	}

	function __construct($itemId=array(-1), $shopId=3, $shop=null, $groups=array(-1)) {
		$itemIDs = $itemId;
		if(!is_array($itemId)) {
			$itemIDs = array($itemId);
		}
		if(!is_array($groups)) {
			$groups = array($groups);
		}
		foreach($groups as $group) {
			$this->_groups[] = $group*1;
		}
		$this->_itemIDs = $itemIDs;
		if(!is_array($this->_itemIDs) || (is_array($this->_itemIDs) && count($this->_itemIDs)==0)) {
			$this->_itemIDs = array(-1);
		}
	}

	public static function createInstance($itemId=array(-1), $shopId=3, $shop=null, $groups=array(-1)) {
		if(is_null($shop)) {
			$shop =  Core_Entity::factory('Shop', $shopId);
		}
		return new self($itemId, $shopId, $shop, $groups);
	}

	public function getProperties($excludes=[]) {
		if(!isset($excludes[0]) || (isset($excludes[0]) && !is_array($excludes[0]))) {
			$excludes = [$excludes];
		}
		// JOIN shop_item_properties sip ON sip.property_id = ptexts.property_id AND sip.show_in_item=1
		$selectInts = Core_QueryBuilder::select('pints.*')->from(array('properties_tech_ints', 'pints'))
			->join(['shop_item_properties', 'sip'], 'sip.property_id', '=', 'pints.property_id', [
				['AND' => ['sip.show_in_item', '=', 1]]
			])
			->where('pints.entity_id', 'IN', $this->_itemIDs)
			->where('pints.property_l1_dir_id', 'IN', $this->_groups)
			->where('pints.visible', '=', 1)
			->where(Core_QueryBuilder::expression('NOT `pints`.`value`'), 'IS', NULL)
			->where('pints.value', '!=', '0')
		;
		if(isset($excludes[0]) && is_array($excludes[0]) && count($excludes[0])) {
			foreach ($excludes as $exclude) {
				$selectInts->where($exclude[0], $exclude[1], Core_QueryBuilder::expression($exclude[2])->build());
			}
		}
		$selectStrings = Core_QueryBuilder::select('pstrings.*')->from(array('properties_tech_strings', 'pstrings'))
			->join(['shop_item_properties', 'sip'], 'sip.property_id', '=', 'pstrings.property_id', [
				['AND' => ['sip.show_in_item', '=', 1]]
			])
			->where('pstrings.entity_id', 'IN', $this->_itemIDs)
			->where('pstrings.property_l1_dir_id', 'IN', $this->_groups)
			->where('pstrings.visible', '=', 1)
			->where(Core_QueryBuilder::expression('NOT `pstrings`.`value`'), 'IS', NULL)
			->where('pstrings.value', '!=', '0')
		;
		if(isset($excludes[0]) && is_array($excludes[0]) && count($excludes[0])) {
			foreach ($excludes as $exclude) {
				$selectStrings->where($exclude[0], $exclude[1], Core_QueryBuilder::expression($exclude[2])->build());
			}
		}
		$selectFloats = Core_QueryBuilder::select('pfloats.*')->from(array('properties_tech_floats', 'pfloats'))
			->join(['shop_item_properties', 'sip'], 'sip.property_id', '=', 'pfloats.property_id', [
				['AND' => ['sip.show_in_item', '=', 1]]
			])
			->where('pfloats.entity_id', 'IN', $this->_itemIDs)
			->where('pfloats.property_l1_dir_id', 'IN', $this->_groups)
			->where('pfloats.visible', '=', 1)
			->where(Core_QueryBuilder::expression('NOT `pfloats`.`value`'), 'IS', NULL)
			->where('pfloats.value', '!=', '0')
		;
		if(isset($excludes[0]) && is_array($excludes[0]) && count($excludes[0])) {
			foreach ($excludes as $exclude) {
				$selectFloats->where($exclude[0], $exclude[1], Core_QueryBuilder::expression($exclude[2])->build());
			}
		}
		$selectTexts = Core_QueryBuilder::select('ptexts.*')->from(array('properties_tech_texts', 'ptexts'))
			->join(['shop_item_properties', 'sip'], 'sip.property_id', '=', 'ptexts.property_id', [
				['AND' => ['sip.show_in_item', '=', 1]]
			])
			->where('ptexts.entity_id', 'IN', $this->_itemIDs)
			->where('ptexts.property_l1_dir_id', 'IN', $this->_groups)
			->where('ptexts.visible', '=', 1)
			->where(Core_QueryBuilder::expression('NOT `ptexts`.`value`'), 'IS', NULL)
			->where('ptexts.value', '!=', '0')
		;
		if(isset($excludes[0]) && is_array($excludes[0]) && count($excludes[0])) {
			foreach ($excludes as $exclude) {
				$selectTexts->where($exclude[0], $exclude[1], Core_QueryBuilder::expression($exclude[2])->build());
			}
		}
// ---------
		$selectTexts
			->union($selectInts)
			->union($selectStrings)
			->union($selectFloats)
		;
		$selectAll = Core_QueryBuilder::select()
						->select('property_path')
						->select('instant_table')
						->select('instant_id')
						->select('entity_id')
//						->select('instant_value')
						->select([Core_QueryBuilder::expression("GROUP_CONCAT(properties.instant_value ORDER BY properties.property_sorting SEPARATOR '~')"), 'instant_value'])
						->select('property_l1_dir_id')
						->select('property_parent_dir_name')
						->select('property_parent_dir_sorting')
						->select('property_parent_dir_column_num')
						->select([Core_QueryBuilder::expression('CASE WHEN property_l1_dir_id!=1000 THEN property_l2_dir_id ELSE par_property_l2_dir_id END'), 'property_l2_dir_id'])
						->select([Core_QueryBuilder::expression('CASE WHEN property_l1_dir_id!=1000 THEN property_l2_dir_name ELSE par_property_l2_dir_name END'), 'property_l2_dir_name'])
						->select([Core_QueryBuilder::expression('CASE WHEN property_l1_dir_id!=1000 THEN property_l2_dir_sorting ELSE par_property_l2_dir_sorting END'), 'property_l2_dir_sorting'])
						->select([Core_QueryBuilder::expression('CASE WHEN property_l1_dir_id!=1000 THEN property_l2_dir_column_num ELSE par_property_l2_dir_column_num END'), 'property_l2_dir_column_num'])
						->select('par_property_l2_dir_id')
						->select('par_property_l2_dir_name')
						->select('par_property_l2_dir_sorting')
						->select('par_property_l2_dir_column_num')
						->select('property_parent_property_dir_id')
						->select('property_parent_property_dir_name')
						->select('property_id')
						->select('property_prefix')
//						->select('property_name')
						->select([Core_QueryBuilder::expression("split_string(GROUP_CONCAT(`property_name` ORDER BY properties.property_sorting), ',', 1)"), 'property_name'])
						->select('property_description')
						->select('property_tagname')
						->select('property_type_id')
						->select('property_sorting')
						->select('property_multiple')
						->select('property_priority')
						->select('property_link')
//						->select('value')
						->select([Core_QueryBuilder::expression("GROUP_CONCAT(properties.value ORDER BY properties.property_sorting SEPARATOR '; ')"), 'value'])
						->select('list_item_id')
						->select('list_id')
						->select('list_path')
						->select('list_linkaddr')
						->select('list_item_description')
						->select('property_type')
						->select('visible')
						->select('measure_id')
						->select('measure_name')
						->select('measure_description')
						->select('property_value_id')
					->from(array($selectTexts, 'properties'))
					->groupBy(Core_QueryBuilder::expression("CASE WHEN TRIM(properties.property_prefix)='' THEN CONCAT(properties.instant_table, '_', properties.property_value_id) ELSE properties.property_prefix END"))
					->orderBy('properties.property_parent_dir_sorting')
					->orderBy('properties.property_l2_dir_column_num')
					->orderBy('properties.property_l2_dir_sorting')
					->orderBy('properties.property_parent_dir_name')
					->orderBy('properties.property_sorting')
					->orderBy('properties.property_name')
				;
		$properties = $selectAll->execute()->asAssoc()->result();
//		utl::tp($selectAll->build());
		foreach ($properties as $propertyKey => $property) {
			if($property['property_type_id']==5) {
				/** @var \Informationsystem_Item_Model $ii */
				$ii = Core_Entity::factory('Informationsystem_Item')->getById($property['list_item_id']);
				$properties[$propertyKey]['property_link'] = $ii->getPath();
			}
		}

		$arProcessed = array();
		foreach($properties as &$property) {
			if(isset($property['property_path'])) {
				$aTmpPropertyPath = explode('/', $property['property_path']);
				$pp = [
					'@attributes' => ['src' => $property['property_path']],
					'item' => [],
				];
				foreach ($aTmpPropertyPath as $tmpPropertyPath) {
					$tmpPropertyPathSplit = explode(';', $tmpPropertyPath);
					if(isset($tmpPropertyPathSplit[1])) {
						$pp['item'][] = [ '@attributes' => ['type' => $tmpPropertyPathSplit[1]], '@value' => $tmpPropertyPathSplit[0]];
					} else {
						$pp['item'][] = $tmpPropertyPath;
					}
				}
				$property['property_path'] = $pp;
//				\Skynetcore_Utils::p($tmpPropertyPath);
			}
			$arProcessed['columns'][$property['property_l2_dir_column_num']]['@attributes'] = array(
				'num' => $property['property_l2_dir_column_num']
			);
			$arProcessed['columns'][$property['property_l2_dir_column_num']]['column'][$property['property_l2_dir_id']]['@attributes'] = array(
				'dirHeader' => $property['property_l2_dir_name'],
				'dirHeaderID' => $property['property_l2_dir_id'],
			);
			$arProcessed['columns'][$property['property_l2_dir_column_num']]['column'][$property['property_l2_dir_id']]['row'][] = $property;
		}
//		utl::p($arProcessed);
		return $arProcessed;
	}

	public function getPropertyByValue($searchValue, $propertyId, $type='ints') {
		$select = Core_QueryBuilder::select()
			->from('property_value_' . $type)
			// ->columns('*')
			->where('property_id', (is_array($propertyId) ? 'IN' : '='), $propertyId)
			->where('value', '=', $searchValue);
		$select = $select
			->execute()
			->asAssoc()
			->result();
		if(is_array($select) && isset($select[0])) {
			return $select;
		} else {
			return null;
		}
	}

	public function getPropertiesByGroup($likeparam = '', $propertyTypes = array(0, 3, 7, 11)) {
		$linkedProperties = Core_QueryBuilder::select(array('p.id', 'property_id'))
			->select(array('p.name', 'property_prefix'))
			->select(array('p.name', 'property_name'))
			->select(array('p.descr2', 'property_short_name'))
			->select(array('p.tag_name', 'property_tag_name'))
			->select(array('p.type', 'property_type'))
			->select(array('sip.filter', 'filter_type'))
			->select(array('p.sorting', 'property_order'))
			->select(array(Core_QueryBuilder::expression('get_type(p.type)'), 'property_type_name'))
			->select(array('pdiu.path', 'property_dir_ids'))
			->select(array('pdiu.url', 'property_dir_path'))
			->from(array('shop_item_property_for_groups', 'sipfg'))
			->join(array('shop_item_properties', 'sip'), 'sip.id', '=', 'sipfg.shop_item_property_id')
			->join(array('properties', 'p'), 'p.id', '=', 'sip.property_id')
			->join(array('getPropertyDirsIerarchyUp', 'pdiu'), 'pdiu.id', '=', 'p.id')
			->where('sipfg.shop_group_id', 'IN', $this->_groups)
			->orderby('property_dir_path')
			->orderby('property_order')
		;
		if(!$this->_showAllProperties) {
			$linkedProperties
				->where('sip.filter', '>', 0);
		}
		(is_array($propertyTypes) && count($propertyTypes)>0) && $linkedProperties->where('p.type', 'IN', $propertyTypes);
		(trim($likeparam)!='') && $linkedProperties->where('pdiu.path', 'LIKE', $likeparam);

		$propertiesList = $linkedProperties->execute()->asAssoc()->result();

		if(is_array($propertiesList) && count($propertiesList)>0) {
			$propertiesListIds = array();
			foreach($propertiesList as $property) {
				$propertiesListIds[$property['property_type']][] = $property['property_id']*1;
			}
			foreach($propertiesListIds as $propertiesListIdKey=>$propertiesIds) {
				$listValuesQuery = Core_QueryBuilder::select(array('p.id', 'property_id'))
					->select(array('li.id', 'list_item_id'))
					->select(array('li.value', 'list_item_value'))
					->select(array('li.sorting', 'list_item_sort'))
					->select(array('li.path', 'list_item_path'))
					->from(array('list_items', 'li'))
					->join(array('properties', 'p'), 'p.list_id', '=', 'li.list_id')
					->join(array('property_value_ints', 'pvi'), 'pvi.value', '=', 'li.id')
//					->join(array('shop_items', 'si'), 'si.id', '=', 'pvi.entity_id')
					->where('p.id', 'IN', $propertiesIds)
					->groupBy('p.id')
					->groupBy('li.id')
					->groupBy('li.value')
					->groupBy('li.sorting')
					;
				$listValuesTmp = $listValuesQuery->execute()->asAssoc()->result();
				$listValues = utl::getArrayKeyValuesFromArrays($listValuesTmp, 'property_id');
			}

			foreach($propertiesList as &$property) {
				if(($getValue=Core_Array::getGet("property_{$property['property_id']}", FALSE)) != FALSE
					&& isset($listValues[$property['property_id']])) {
					foreach ($listValues[$property['property_id']] as &$listValueTmp) {
						if(array_search($listValueTmp['list_item_id'], $getValue)!==FALSE ||
							array_search($listValueTmp['list_item_path'], $getValue)!==FALSE
							) {
							$listValueTmp['selected'] = 1;
						}
					}
					$property['property_values']['value'] = $listValues[$property['property_id']];
				}
			}
		}
		$this->_showToXML = $propertiesList;
	}

	/**
	 * Build entity XML
	 *
	 * @return string
	 */
	public function getXml()
	{
		return entity::createInstance($this->_showToXML, '', 'property', '')->getXml();
	}

}