<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Shop;

use Core_Entity;
use Core_File;
use Core_QueryBuilder;
use Core_Exception;
use HCMS\Utils as utl;
use HCMS\Core\Xml\Arr as axml;

class Items {
	protected $_itemModel = null;
	protected $_shopId = 0;
	protected $_shop = null;

	function __construct($shopId=3, $shop=null) {
		$this->_shopId = $shopId;
		(is_null($shop) || is_null($this->_shopId)) && $this->_shop = Core_Entity::factory('Shop_Item', $shop);
		$this->_itemModel = Core_Entity::factory('Shop_Item');
		$this->_itemModel
			->queryBuilder()
			->where('shop_id', '=', $this->_shopId)
			->where('deleted', '=', 0)
			->where('active', '=', 1)
		;
	}

	public static function createInstance($shopId=3, $shop=null) {
		if(is_null($shop)) {
			$shop =  Core_Entity::factory('Shop', $shopId);
		}
		return new self($shopId, $shop);
	}

	public function getDirectoryPathByID($itemId) {
		return $this->_shop->getPath() . '/' . Core_File::getNestingDirPath($itemId, $this->_shop->Site->nesting_level) . '/item_' . $itemId . '/';
	}

	public function getByPath($itemPath, $group_id=NULL)
	{
		(!is_null($group_id)) && $this->_itemModel->queryBuilder()->where('shop_group_id', '=', $group_id);
		$oShopItem = NULL;
		(trim($itemPath) != "") && $oShopItem = $this->_itemModel->getByPath($itemPath);

		return $oShopItem;
	}

	public function getByMarking($marking)
	{
		$foundedItem = NULL;
		(trim($marking) != "") && $foundedItems = $this->_itemModel->getAllByMarking($marking, FALSE);
		if( isset($foundedItems) && !is_null($foundedItems) ) {
			if( is_array( $foundedItems ) ) {
				foreach($foundedItems as $item) {
					$groups = array();
					utl::getAllParenGroups($item->shop_group_id, $groups);
					$groups = array_reverse($groups);
					if((isset($groups[0]->id) && !is_null($groups[0]->id) && ($groups[0]->parent_id == 0)) ||
						(isset($item->modification_id) && $item->modification_id>0)) {
						$foundedItem = $item;
						break;
					}
				}
			} else {
				$foundedItem = $foundedItems;
			}
		}
		return $foundedItem;
	}

	public function getEntityByPropertyValue($propertyId, $value, $type = 'int')
	{
		$rq = Core_QueryBuilder::select('entity_id')
			->from('property_value_' . $type)
			->where('property_id', '=', $propertyId);
		switch($type){
			case 'strings':
				$rq->where('value', 'LIKE', '%'. $value.'%');
				break;
			default:
				$rq->where('value', '=', $value);
		}
		$select = $rq
			->execute()
			->asAssoc()
			->result();
		if (isset($select)) {
			$linkedEntitiyIDs = utl::getArrayValuesFromArrays($select, "entity_id");
			$oShopItems = utl::getShopItemByIDs($linkedEntitiyIDs);
			return $oShopItems;
		}
		return NULL;
	}

	public function getItemsWithGroups($itemIds, $toArray = true) {
		$outArray = array();
		if(is_array($itemIds) && count($itemIds)>0) {
			$arResult = utl::getModelItemsByArrayIds('Shop_Item', $itemIds, 'id', $toArray);
			$subArray = \CUtil::getArrayKeyValuesFromArrays($arResult, 'marking');
			foreach($subArray as $oSubKey => $subItem) {
				$outArray[$oSubKey] = $subItem[0];
			}
			if ($toArray) {
				foreach($outArray as $oKey => $item) {
					(!isset($outArray[$oKey]['groups'])) && $outArray[$oKey]['groups'] = array();
					$parents = array();
					utl::getAllParenGroups($item['shop_group_id'], $parents);
					$parents = array_reverse($parents);
					$outArray[$oKey]['groups'][] = utl::setArrayOfObjectsToArray($parents);
				}
			}
		}
		return $outArray;
	}

	public function setWarehouseCount($itemObject, $warehouseGUID, $count) {
		$wh = Core_Entity::factory('Shop_Warehouse')->getByGuid($warehouseGUID);
		if(!is_null($wh)) {
			if(is_null($whValue = $itemObject->shop_warehouse_items->getByWarehouseId($wh->id, false))) {
				$whValue=Core_Entity::factory('Shop_Warehouse_Item');
				$whValue->shop_item_id = $itemObject->id;
				$whValue->shop_warehouse_id = $wh->id;
				$whValue->save();
			}
			if($whValue->count != $count) {
				$whValue->count = $count;
				$whValue->save();
			}
		} else {
			throw new Core_Exception("Склад '{$warehouseGUID}' не существует");
		}
	}

	public function setPriceValue($itemObject, $priceGUID, $value) {
		trigger_error("Метод setPriceValue устарел. Используйте Utils_Shop_Item_Price_Model::setPriceValue", E_USER_DEPRECATED);
		$pr = Core_Entity::factory('Shop_Price')->getByGuid($priceGUID);
		if(!is_null($pr)) {
			if(is_null($prValue = $itemObject->shop_item_prices->getByPriceId($pr->id, false))) {
				$prValue=Core_Entity::factory('Shop_Item_Price');
				$prValue->shop_item_id = $itemObject->id;
				$prValue->shop_price_id = $pr->id;
				$prValue->save();
			}
			if($prValue->value != $value) {
				$prValue->value = $value;
				$prValue->save();
			}
		} else {
			throw new Core_Exception("Склад '{$priceGUID}' не существует");
		}
	}

	public function getItemObjectByPath($itemPath, $shopID=0)
	{
		$returnes = FALSE;
		$groups = array_values(array_diff(explode('/', $itemPath), array('')));
		$shop = NULL;
		if(count($groups)>0 && $shopID*1 > 0) {
			$shop = Core_Entity::factory('Shop')->getById($shopID, FALSE);
			$this->_shopId = $shopID;
			$this->_shop = $shop;
			if($shop->id > 0) {
				$parentId = 0;
				$parentGroup = NULL;
				foreach($groups as $key => $subGroupPath) {
					$group = Core_Entity::factory('Shop_Group');
					$group
						->queryBuilder()
						->where('deleted', '=', 0)
						->where('active', '=', 1)
						->where('shop_id', '=', $shop->id)
						->where('parent_id', '=', (is_null($parentGroup) ? 0 : $parentGroup->id))
					;
					$group = $group->getByPath($subGroupPath, FALSE);
					if(is_null($group)) {
						$itemModel = Core_Entity::factory('Shop_Item');
						$itemModel
							->queryBuilder()
							->where('deleted', '=', 0)
							->where('active', '=', 1)
							->where('shop_id', '=', $shop->id)
							->where('shop_group_id', '=', (is_null($parentGroup) ? 0 : $parentGroup->id));
						$group = $itemModel->getByPath($subGroupPath, FALSE);
					}
					$parentGroup = $group;
				}
				$returnes = $parentGroup;
			}
		}
		return $returnes;
	}

	public function getItemsMatrixByPropertyAndListValues($part, $property_id, $list_values, $filters=array(), $limit=10, $offset=0) {
		$mListItemValues = Core_Entity::factory('List_Item');
		$mListItemValues
			->queryBuilder()
			->where('id', 'IN', $list_values)
		;
		$aListItemValues[] = array('value'=>'');
		$aListItemValues[] = array('value'=>'Название');
//		$aListItemValues[] = array('value'=>'Артикул');
		$aListItemValues = array_merge($aListItemValues, utl::setArrayOfObjectsToArray($mListItemValues->findAll(FALSE)));

		$sparesQuerySUB = Core_QueryBuilder::select(array('si.id', 'item_id'))
				->select(array('si.shop_group_id', 'group_id'))
				->select(array('si.marking', 'marking'))
				->select(array('si.name', 'name'))
				->select(array('giu.path', 'path'))
				->select(array('giu.dirpath', 'dirpath'))
				->select(array('giu.url', 'url'))
			->from(array('getGroupsIerarchyUp', 'giu'))
				->join(array('shop_items', 'si'), 'si.id', '=', 'giu.id')
				->where("si.modification_id", "=", 0)
				->where("si.active", "=", 1)
				->where("si.deleted", "=", 0)
				->where("url", "LIKE", "{$part}%")
			;
		foreach ($list_values as $list_value) {
			$sparesQuerySUB
				->select(array(Core_QueryBuilder::expression("COALESCE(pvi{$list_value}.value, 0)"), "v{$list_value}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(pvi{$list_value}.id, 0)"), "vid{$list_value}"))
				->leftJoin(array('property_value_ints', "pvi{$list_value}"), "pvi{$list_value}.entity_id", '=', 'si.id', array(array('AND' => array("pvi{$list_value}.property_id", '=', $property_id), 'AND' => array("pvi{$list_value}.value", '=', $list_value))))
//				if(isset($filters['zeroOnly'])) {
//					$sparesQuerySUB
//						->where(Core_QueryBuilder::expression("COALESCE(pvi{$list_value}.value, 0)"), '>', 0);
//				}
			;
		}

		$sparesQuery = Core_QueryBuilder::select('sq.item_id')
				->select('sq.group_id')
				->select('sq.marking')
				->select('sq.name')
				->select('sq.path')
				->select('sq.dirpath')
				->select('sq.url')
				->select(array(Core_QueryBuilder::expression($property_id),'property_id'))
				->select(array('sg1.id', 'g1_id'))
				->select(array('sg1.parent_id', 'g1_parent_id'))
				->select(array('sg1.name', 'g1_name'))
				->select(array('sg2.id', 'g2_id'))
				->select(array('sg2.parent_id', 'g2_parent_id'))
				->select(array('sg2.name', 'g2_name'))
				->select(array('sg3.id', 'g3_id'))
				->select(array('sg3.parent_id', 'g3_parent_id'))
				->select(array('sg3.name', 'g3_name'))
				->select(array('sg4.id', 'g4_id'))
				->select(array('sg4.parent_id', 'g4_parent_id'))
				->select(array('sg4.name', 'g4_name'))
				->select(array('sg5.id', 'g5_id'))
				->select(array('sg5.parent_id', 'g5_parent_id'))
				->select(array('sg5.name', 'g5_name'))
			->from(array($sparesQuerySUB, 'sq'))
				->leftJoin(array('shop_groups', 'sg1'), 'sg1.id', '=', Core_QueryBuilder::expression("split_string(sq.path, '/', 1)*1"))
				->leftJoin(array('shop_groups', 'sg2'), 'sg2.id', '=', Core_QueryBuilder::expression("split_string(sq.path, '/', 2)*1"))
				->leftJoin(array('shop_groups', 'sg3'), 'sg3.id', '=', Core_QueryBuilder::expression("split_string(sq.path, '/', 3)*1"))
				->leftJoin(array('shop_groups', 'sg4'), 'sg4.id', '=', Core_QueryBuilder::expression("split_string(sq.path, '/', 4)*1"))
				->leftJoin(array('shop_groups', 'sg5'), 'sg5.id', '=', Core_QueryBuilder::expression("split_string(sq.path, '/', 5)*1"))
				->where(Core_QueryBuilder::expression("COALESCE(sg1.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg1.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg2.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg2.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg3.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg3.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg4.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg4.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg5.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(sg5.deleted, 0)"), '=', 0)
			->orderBy('g1_name')
			->orderBy('g2_name')
			->orderBy('g3_name')
			->orderBy('g4_name')
			->orderBy('g5_name')
			->limit($limit)
			->offset($offset)
			;
		foreach ($list_values as $list_value) {
			$sparesQuery
				->select("sq.v{$list_value}")
				->select("sq.vid{$list_value}");
		}
		$aResult = $sparesQuery->sqlCalcFoundRows(true)
			->execute()
			->asAssoc()
			->result();

		$sparesQuery
			->clear()
			->select(array('FOUND_ROWS()', 'count'));
		$querySelectCount = $sparesQuery->execute()->asAssoc()->result();

		$tmpGroups = array();
		$allGroups = array();
		foreach ($aResult as &$aItem) {
			$itemInfo = Core_Entity::factory('Shop_Item')->getById($aItem['item_id']);
			if($itemInfo->image_large!='') {
				$aItem['images']['large'] = $itemInfo->getLargeFileHref();
			}
			if($itemInfo->image_small!='') {
				$aItem['images']['small'] = $itemInfo->getSmallFileHref();
			}

			$aGroups = array();
			for($i=1; $i<6; $i++) {
				if( strlen($aItem["g{$i}_id"])>0 ) {
					$aGroupItem = array('id'=>$aItem["g{$i}_id"], 'parent_id'=>$aItem["g{$i}_parent_id"], 'name'=>$aItem["g{$i}_name"]);
					if(array_search($aGroupItem, $allGroups)===false) {
						$allGroups[] = $aGroupItem;
					}
//					$tmpGroups[$aItem["g{$i}_parent_id"]][$aItem["g{$i}_id"]] = $aItem["g{$i}_name"];
//					$aGroups['group'][] = array('@attributes'=>array('id'=>$aItem["g{$i}_id"]), '@value'=>$aItem["g{$i}_name"]);
				}
				unset($aItem["g{$i}_id"]);
				unset($aItem["g{$i}_name"]);
			}
			$aItem['groups'] = $aGroups;

			$aValues = array();
			foreach ($list_values as $list_value) {
				$aValues['value'][] = array('@attributes'=>array('list_value_id'=>$list_value, 'property_value_id'=>$aItem["vid{$list_value}"], 'property_id'=>$property_id), '@value'=>$aItem["v{$list_value}"]);
				unset($aItem["v{$list_value}"]);
				unset($aItem["vid{$list_value}"]);
			}
			$aItem['values'] = $aValues;
		}
		$aResult = array_merge(array(array('groups'=>array('group'=>utl::buildTree($allGroups)), 'headers'=>array('header'=>$aListItemValues), 'settings'=>array('total'=>$querySelectCount[0]['count']))), $aResult);

//		utl::p(array(array('groups'=>utl::buildTree($allGroups))));
//		array('groups'=>utl::buildTree($allGroups))
//		foreach ($tmpGroups as $tmpKey=>$tmpGroup) {
//			$allGroups[]
//			utl::p( utl::buildTree($allGroups) );
//		}

		$aXML = axml\Entity::createInstance($aResult, 'item', 'items', array('item_id', 'group_id'));
		return $aXML;
	}
}