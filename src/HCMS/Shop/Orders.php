<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Shop;

use Core_Entity;
use Core_File;
use HCMS\Utils as utl;
use HCMS\Shop\Items as itm;

class Orders {
	protected $_order = null;
	protected $_order_items = null;
	protected $_order_shop_items = null;

	function __construct($orderInvoice) {
		$this->_order = Core_Entity::factory('Shop_Order');
		$this->_order = $this->_order->getByInvoice($orderInvoice, false);
		if(is_null($this->_order)) {
			$this->_order = Core_Entity::factory('Shop_Order')->getById($orderInvoice, false);
		}
		if(isset($this->_order->Shop_Order_Items) && !is_null($this->_order->Shop_Order_Items)) {
			$this->_order_items = $this->_order->Shop_Order_Items->findAll();
		}
	}

	public static function createInstance($orderInvoice) {
		return new self($orderInvoice);
	}

	/**
	 * @return null
	 */
	public function getOrder($toArray = true)
	{
		if($toArray && is_object($this->_order)) {
			$outArray = $this->_order->toArray();
			$outArray['opportunuty'] = $this->_order->getAmount();

			$aAddr = [];
			if($this->_order->shop_country_id > 0) {
				$aAddr[] = $this->_order->Shop_Country->name;
			}
			if($this->_order->shop_country_location_id > 0) {
				$aAddr[] = $this->_order->Shop_Country_Location->name;
			}
			if($this->_order->shop_country_location_city_id > 0) {
				$aAddr[] = $this->_order->Shop_Country_Location_City->name;
			}
			$aAddr[] = $this->_order->address;
			$outArray['address'] = implode(', ', $aAddr);

			$aPropertyValues = $this->_order->getPropertyValues();
			$vPropertyValues = [];
			foreach ($aPropertyValues as $oPropertyValue) {
				$vPropertyValues[$oPropertyValue->property->tag_name][] = $oPropertyValue->toArray();
			}
			$outArray['properties'] = $vPropertyValues;
//			\Skynetcore_Utils::p($this->_order->getPropertyValues());
			return $outArray;
		}
		is_null($this->_order) && $this->_order=array();
		return $this->_order;
	}

	/**
	 * @return null
	 */
	public function getOrderItems($toArray = true)
	{
		$outArray = $this->_order_items;
		if($toArray && is_array($this->_order_items) && count($this->_order_items)>0) {
			$outArray = array();
			foreach($this->_order_items as $orderItem) {
				$outArrayItem = $orderItem->toArray();
				$outArrayItem['umcode'] = '';
				if(isset($orderItem->shop_item->marking) && $orderItem->shop_item->marking != '') {
					$outArrayItem['umcode'] = $orderItem->shop_item->marking;
				}
				$outArray[] = $outArrayItem;
			}
//			\Skynetcore_Utils::p($outArray);
		}
		is_null($outArray) && $outArray=array();
		return $outArray;
	}

	/**
	 * @return null
	 */
	public function getOrderShopItems($toArray = true)
	{
		$outArray = array();
		if(is_array($this->_order_items) && count($this->_order_items)>0) {
			$itemIds = array_keys(utl::getArrayKeyValuesFromArrays(utl::setArrayOfObjectsToArray($this->_order_items), 'shop_item_id'));
			$outArray = itm::createInstance()->getItemsWithGroups($itemIds);
//			utl::p($outArray);
//            $arResult = utl::getModelItemsByArrayIds('Shop_Item', $itemIds, 'id', $toArray);
//            $subArray = \CUtil::getArrayKeyValuesFromArrays($arResult, 'marking');
//            foreach($subArray as $oSubKey => $subItem) {
//                $outArray[$oSubKey] = $subItem[0];
//            }
//            if ($toArray) {
//                foreach($outArray as $oKey => $item) {
//                    (!isset($outArray[$oKey]['groups'])) && $outArray[$oKey]['groups'] = array();
//                    $parents = array();
//                    utl::getAllParenGroups($item['shop_group_id'], $parents);
//                    $parents = array_reverse($parents);
//                    $outArray[$oKey]['groups'][] = utl::setArrayOfObjectsToArray($parents);
//                }
//            }
		}
		return $outArray;
	}
}