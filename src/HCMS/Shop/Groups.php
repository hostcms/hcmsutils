<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 11.02.2015
 * Time: 10:44
 */
namespace HCMS\Shop;

use Core_Entity;
use Core_File;
use Core_QueryBuilder;
use HCMS\Utils as utl;
use LSS\Array2XML as axml;

class Groups {
	protected $_itemModel = null;
	protected $_shopId = 0;
	protected $_addBaseDistinct = FALSE;
	protected $_addItemRestriction = FALSE;
	protected $_shop = null;
	protected $_treeAll = array();
	protected $_treeIds = array();
	protected $_addWhereLists = array();
	protected $_addWhere = array();
	protected $_addOrderBy = array();
	protected $_incLevel = 0;
	protected $_showInActive = FALSE;
	protected $_addItemsToJS = FALSE;

	function __construct($shopId=3, $shop=null) {
	}

	public static function createInstance($shopId=3, $shop=null) {
		if(is_null($shop)) {
			$shop =  Core_Entity::factory('Shop', $shopId);
		}
		return new self($shopId, $shop);
	}

	public function getGroupsTree($groupIDs=array(-1), $parentURL='', $propertyToShows=[], $groupByLevel=false, $aLimitIds=[]) {
		$instance = \Core_Page::instance();
		if(!is_array($groupIDs)) {
			$groupIDs = array($groupIDs);
		}
		$localInstanceShopControllerShow = $localInstanceShopControllerQueryBuilder = false;
		if(isset($instance->object)) {
			if(is_array($instance->object)
				&& isset($instance->object['Shop_Controller_Show'])
				&& $instance->object['Shop_Controller_Show'] instanceof \Shop_Controller_Show
			) {
				$localInstanceShopControllerShow = $instance->object['Shop_Controller_Show'];
			} else {
				if($instance->object instanceof \Shop_Controller_Show) {
					$localInstanceShopControllerShow = $instance->object;
				}
			}
			if ($localInstanceShopControllerShow !== false) {
				$localInstanceShopControllerQueryBuilder = clone $localInstanceShopControllerShow->shopItems()->queryBuilder();
				$localInstanceShopControllerQueryBuilder
					->sqlCalcFoundRows(false)
					->clearSelect()
					->clearLimit()
					->clearOffset()
					->clearGroupBy()
					->clearOrderBy()
					->clearHaving()
					->select('shop_items.shop_group_id')
					->groupBy('shop_items.shop_group_id')
				;
			}
//			\Skynetcore_Utils::tp($localInstanceShopControllerQueryBuilder->build());
//			\Skynetcore_Utils::tp($localInstanceShopControllerQueryBuilder->build());
		}
//		$localInstanceShopControllerShow = $instance->object;
//		if(isset($aLimitIds)) {
//
//		}
//		\Skynetcore_Utils::tp($localInstanceShopControllerShow);
//		\Skynetcore_Utils::tp(
//			\Core_Page::instance()->object['Shop_Controller_Show']->shopItems()->queryBuilder()->build()
//		);

//		\Skynetcore_Utils::tp(
//			\Core_Page::instance()->object['Shop_Controller_Show']->shopItems()->queryBuilder()->build()
//		);
		$this->_treeAll = array();
		$arWhere = array(-1);
		$addQBPath = '';
		if(count($this->_addWhereLists)>0) {
			$addQBArray = array();
			$addQBPath = '/';
			foreach($this->_addWhereLists as $addWhereListItem) {
				$addQBArray[] = $addWhereListItem->id*1;
				$addQBPath .= $addWhereListItem->path.'/';
				$this->_incLevel++;
			}
			if(count($addQBArray)>0) {
				$cqbWhere = Core_QueryBuilder::select(array(Core_QueryBuilder::expression("CONCAT(si.shop_group_id, ',')"), 'gid'))
					->from(array('property_value_ints', 'pvi'))
					->join(array('shop_items', 'si'), 'pvi.entity_id', '=', 'si.id')
					->join(array('list_items', 'li'), 'li.id', '=', 'pvi.value', array(
						array('AND' => array('li.id', 'IN', $addQBArray))
					))
					->groupBy('si.shop_group_id')
				;
				$dopWhere = utl::getArrayValuesFromArrays($cqbWhere->execute()->asAssoc()->result(), 'gid');
				$arWhere = array_merge($arWhere, $dopWhere);
			}
		}
		$coreQueryBuilderTreeUp = Core_QueryBuilder::select(array('g.path', 'path'))
			->select(array('g.back_path', 'back_path'))
			->select(array('g.url', 'gurl'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 1)*1"), 'g1'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 2)*1"), 'g2'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 3)*1"), 'g3'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 4)*1"), 'g4'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 5)*1"), 'g5'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 6)*1"), 'g6'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 7)*1"), 'g7'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 8)*1"), 'g8'))
			->select(array(Core_QueryBuilder::expression("split_string(g.path, '/', 9)*1"), 'g9'))
			->select(array(Core_QueryBuilder::expression("split_string(g.back_path, '/', 1)*1"), 'gl'))
			->from(array('getGroupsIerarchyDown', 'g'))
			->where('g.id', 'IN', $groupIDs)
		;
		if(\Core_Array::getRequest('tests', false) !== false && $localInstanceShopControllerQueryBuilder !== false) {
			$coreQueryBuilderTreeUp
				->join([$localInstanceShopControllerQueryBuilder, 'itemRests'], 'itemRests.shop_group_id', '=',
					Core_QueryBuilder::expression("split_string(g.back_path, '/', 1)"))
			;
		}
		if(count($this->_addWhereLists)>0) {
			$coreQueryBuilderTreeUp
				->open()
					->where('split_string(g.back_path, \'/\', 1)*1', 'IN', $arWhere)
					->setOr()
					->where('split_string(g.back_path, \'/\', 2)*1', 'IN', $arWhere)
				->close()
			;
		}
		$coreQueryBuilderTree = Core_QueryBuilder::select('gr.path')
			->select('gr.back_path')
			->select(array( $addQBPath=='' ? 'gr.gurl' : Core_QueryBuilder::expression("REPLACE(`gr`.`gurl`, CONCAT('/',split_string(gr.gurl, '/', 2),'/'), CONCAT('/', split_string(gr.gurl, '/', 2),'".$addQBPath."'))"), 'url'))
			->select(array("sg1.id", "sg1_id"))
			->select(array("sg1.parent_id", "sg1_parent_id"))
			->select(array("sg1.name", "sg1_name"))
			->select(array("sg1.path", "sg1_path"))
			->select(array("sg1.image_large", "sg1_image_large"))
			->select(array("sg1.image_small", "sg1_image_small"))
			->select(array("sg1.items_count", "sg1_items_count"))
			->select(array("sg1.subgroups_total_count", "sg1_subgroups_total_count"))
			->select(array("sg1.items_total_count", "sg1_items_total_count"))
			->select(array("sg1.sorting", "sg1_sorting"))
			->select(array("sg1.active", "sg1_active"))
			->select(array("sg1.deleted", "sg1_deleted"))
			->select(array("sg2.id", "sg2_id"))
			->select(array("sg2.parent_id", "sg2_parent_id"))
			->select(array("sg2.name", "sg2_name"))
			->select(array("sg2.path", "sg2_path"))
			->select(array("sg2.image_large", "sg2_image_large"))
			->select(array("sg2.image_small", "sg2_image_small"))
			->select(array("sg2.items_count", "sg2_items_count"))
			->select(array("sg2.subgroups_total_count", "sg2_subgroups_total_count"))
			->select(array("sg2.items_total_count", "sg2_items_total_count"))
			->select(array("sg2.sorting", "sg2_sorting"))
			->select(array("sg2.active", "sg2_active"))
			->select(array("sg2.deleted", "sg2_deleted"))
			->select(array("sg3.id", "sg3_id"))
			->select(array("sg3.parent_id", "sg3_parent_id"))
			->select(array("sg3.name", "sg3_name"))
			->select(array("sg3.path", "sg3_path"))
			->select(array("sg3.image_large", "sg3_image_large"))
			->select(array("sg3.image_small", "sg3_image_small"))
			->select(array("sg3.items_count", "sg3_items_count"))
			->select(array("sg3.subgroups_total_count", "sg3_subgroups_total_count"))
			->select(array("sg3.items_total_count", "sg3_items_total_count"))
			->select(array("sg3.sorting", "sg3_sorting"))
			->select(array("sg3.active", "sg3_active"))
			->select(array("sg3.deleted", "sg3_deleted"))
			->select(array("sg4.id", "sg4_id"))
			->select(array("sg4.parent_id", "sg4_parent_id"))
			->select(array("sg4.name", "sg4_name"))
			->select(array("sg4.path", "sg4_path"))
			->select(array("sg4.image_large", "sg4_image_large"))
			->select(array("sg4.image_small", "sg4_image_small"))
			->select(array("sg4.items_count", "sg4_items_count"))
			->select(array("sg4.subgroups_total_count", "sg4_subgroups_total_count"))
			->select(array("sg4.items_total_count", "sg4_items_total_count"))
			->select(array("sg4.sorting", "sg4_sorting"))
			->select(array("sg4.active", "sg4_active"))
			->select(array("sg4.deleted", "sg4_deleted"))
			->select(array("sg5.id", "sg5_id"))
			->select(array("sg5.parent_id", "sg5_parent_id"))
			->select(array("sg5.name", "sg5_name"))
			->select(array("sg5.path", "sg5_path"))
			->select(array("sg5.image_large", "sg5_image_large"))
			->select(array("sg5.image_small", "sg5_image_small"))
			->select(array("sg5.items_count", "sg5_items_count"))
			->select(array("sg5.subgroups_total_count", "sg5_subgroups_total_count"))
			->select(array("sg5.items_total_count", "sg5_items_total_count"))
			->select(array("sg5.sorting", "sg5_sorting"))
			->select(array("sg5.active", "sg5_active"))
			->select(array("sg5.deleted", "sg5_deleted"))
			->select(array("sg6.id", "sg6_id"))
			->select(array("sg6.parent_id", "sg6_parent_id"))
			->select(array("sg6.name", "sg6_name"))
			->select(array("sg6.path", "sg6_path"))
			->select(array("sg6.image_large", "sg6_image_large"))
			->select(array("sg6.image_small", "sg6_image_small"))
			->select(array("sg6.items_count", "sg6_items_count"))
			->select(array("sg6.subgroups_total_count", "sg6_subgroups_total_count"))
			->select(array("sg6.items_total_count", "sg6_items_total_count"))
			->select(array("sg6.sorting", "sg6_sorting"))
			->select(array("sg6.active", "sg6_active"))
			->select(array("sg6.deleted", "sg6_deleted"))
			->select(array("sg7.id", "sg7_id"))
			->select(array("sg7.parent_id", "sg7_parent_id"))
			->select(array("sg7.name", "sg7_name"))
			->select(array("sg7.path", "sg7_path"))
			->select(array("sg7.image_large", "sg7_image_large"))
			->select(array("sg7.image_small", "sg7_image_small"))
			->select(array("sg7.items_count", "sg7_items_count"))
			->select(array("sg7.subgroups_total_count", "sg7_subgroups_total_count"))
			->select(array("sg7.items_total_count", "sg7_items_total_count"))
			->select(array("sg7.sorting", "sg7_sorting"))
			->select(array("sg7.active", "sg7_active"))
			->select(array("sg7.deleted", "sg7_deleted"))
			->select(array("sg8.id", "sg8_id"))
			->select(array("sg8.parent_id", "sg8_parent_id"))
			->select(array("sg8.name", "sg8_name"))
			->select(array("sg8.path", "sg8_path"))
			->select(array("sg8.image_large", "sg8_image_large"))
			->select(array("sg8.image_small", "sg8_image_small"))
			->select(array("sg8.items_count", "sg8_items_count"))
			->select(array("sg8.subgroups_total_count", "sg8_subgroups_total_count"))
			->select(array("sg8.items_total_count", "sg8_items_total_count"))
			->select(array("sg8.sorting", "sg8_sorting"))
			->select(array("sg8.active", "sg8_active"))
			->select(array("sg8.deleted", "sg8_deleted"))
			->select(array("sg9.id", "sg9_id"))
			->select(array("sg9.parent_id", "sg9_parent_id"))
			->select(array("sg9.name", "sg9_name"))
			->select(array("sg9.path", "sg9_path"))
			->select(array("sg9.image_large", "sg9_image_large"))
			->select(array("sg9.image_small", "sg9_image_small"))
			->select(array("sg9.items_count", "sg9_items_count"))
			->select(array("sg9.subgroups_total_count", "sg9_subgroups_total_count"))
			->select(array("sg9.items_total_count", "sg9_items_total_count"))
			->select(array("sg9.sorting", "sg9_sorting"))
			->select(array("sg9.active", "sg9_active"))
			->select(array("sg9.deleted", "sg9_deleted"))
			->from(array($coreQueryBuilderTreeUp, 'gr'))
			->leftJoin(array('shop_groups', 'sg1'), 'sg1.id', '=', 'gr.g1')
			->leftJoin(array('shop_groups', 'sg2'), 'sg2.id', '=', 'gr.g2')
			->leftJoin(array('shop_groups', 'sg3'), 'sg3.id', '=', 'gr.g3')
			->leftJoin(array('shop_groups', 'sg4'), 'sg4.id', '=', 'gr.g4')
			->leftJoin(array('shop_groups', 'sg5'), 'sg5.id', '=', 'gr.g5')
			->leftJoin(array('shop_groups', 'sg6'), 'sg6.id', '=', 'gr.g6')
			->leftJoin(array('shop_groups', 'sg7'), 'sg7.id', '=', 'gr.g7')
			->leftJoin(array('shop_groups', 'sg8'), 'sg8.id', '=', 'gr.g8')
			->leftJoin(array('shop_groups', 'sg9'), 'sg9.id', '=', 'gr.g9')

		;
		if(count($propertyToShows)>0) {
			foreach ($propertyToShows as $propertyToShow => $propertyToShowDefaultValue)
			$coreQueryBuilderTree
				->select([Core_QueryBuilder::expression("gr.gl"), "pvi{$propertyToShow}gid"])
				->select([Core_QueryBuilder::expression("COALESCE(pvi{$propertyToShow}.value, {$propertyToShowDefaultValue})"), "pvi{$propertyToShow}value"])
				->leftJoin(['property_value_ints', "pvi{$propertyToShow}"], "pvi{$propertyToShow}.entity_id", '=', 'gr.gl', [
					['AND' => ["pvi{$propertyToShow}.property_id", '=', $propertyToShow]]
				]);
		}
		if(count($this->_addOrderBy)) {
			foreach ($this->_addOrderBy as $orderBy) {
				$coreQueryBuilderTree
					->orderBy($orderBy['order'], $orderBy['direction'])
				;
			}
		} else {
			$coreQueryBuilderTree
				->orderBy('sg1.sorting', 'ASC')
				->orderBy('sg2.sorting', 'ASC')
				->orderBy('sg3.sorting', 'ASC')
				->orderBy('sg4.sorting', 'ASC')
				->orderBy('sg5.sorting', 'ASC')
				->orderBy('sg6.sorting', 'ASC')
				->orderBy('sg7.sorting', 'ASC')
				->orderBy('sg8.sorting', 'ASC')
				->orderBy('sg9.sorting', 'ASC')
			;
		}
		if(is_array($this->_addWhere) && count($this->_addWhere)>0) {
			foreach ($this->_addWhere as $addWhere) {
				$coreQueryBuilderTreeUp
					->where($addWhere[0], $addWhere[1], $addWhere[2]);
			}
		}
		$this->_addBaseDistinct && $coreQueryBuilderTree->distinct();
		if($this->_addItemRestriction!==FALSE) {
			$coreQueryBuilderTree->distinct();
			$coreQueryBuilderTree
				->join(array('shop_items', 'si'), 'si.shop_group_id', '=', Core_QueryBuilder::expression("split_string(gr.back_path, '/', 1)*1"))
				->join(array('property_value_ints', 'pvi'), 'pvi.entity_id', '=', 'si.id', array(
					array('AND' => array('pvi.property_id', '=', $this->_addItemRestriction['property_id']*1))
				))
				->join(array('getGroupsIerarchyUp', 'pgiu'), 'pgiu.id', '=', 'pvi.value', array(
					array('AND' => array('pgiu.url', 'LIKE', $this->_addItemRestriction['restrictPath'].'%'))
				))
			;
		}
		$coreQueryBuilderTree
			->select(['mcg1.all_cnt', 'sg1_all_cnt'])
			->select(['mcg1.archive_items_cnt', 'sg1_archive_items_cnt'])
			->select(['mcg1.site_show_cnt', 'sg1_site_show_cnt'])
			->select(['mcg2.all_cnt', 'sg2_all_cnt'])
			->select(['mcg2.archive_items_cnt', 'sg2_archive_items_cnt'])
			->select(['mcg2.site_show_cnt', 'sg2_site_show_cnt'])
			->select(['mcg3.all_cnt', 'sg3_all_cnt'])
			->select(['mcg3.archive_items_cnt', 'sg3_archive_items_cnt'])
			->select(['mcg3.site_show_cnt', 'sg3_site_show_cnt'])
			->select(['mcg4.all_cnt', 'sg4_all_cnt'])
			->select(['mcg4.archive_items_cnt', 'sg4_archive_items_cnt'])
			->select(['mcg4.site_show_cnt', 'sg4_site_show_cnt'])
			->select(['mcg5.all_cnt', 'sg5_all_cnt'])
			->select(['mcg5.archive_items_cnt', 'sg5_archive_items_cnt'])
			->select(['mcg5.site_show_cnt', 'sg5_site_show_cnt'])
			->select(['mcg6.archive_items_cnt', 'sg6_archive_items_cnt'])
			->select(['mcg6.site_show_cnt', 'sg6_site_show_cnt'])
			->select(['mcg6.all_cnt', 'sg6_all_cnt'])
			->select(['mcg7.archive_items_cnt', 'sg7_archive_items_cnt'])
			->select(['mcg7.site_show_cnt', 'sg7_site_show_cnt'])
			->select(['mcg7.all_cnt', 'sg7_all_cnt'])
			->select(['mcg8.archive_items_cnt', 'sg8_archive_items_cnt'])
			->select(['mcg8.site_show_cnt', 'sg8_site_show_cnt'])
			->select(['mcg8.all_cnt', 'sg8_all_cnt'])
			->select(['mcg9.archive_items_cnt', 'sg9_archive_items_cnt'])
			->select(['mcg9.site_show_cnt', 'sg9_site_show_cnt'])
			->select(['mcg9.all_cnt', 'sg9_all_cnt'])
			->leftJoin(['moto_cache_item_group_archives', 'mcg1'], 'mcg1.id', '=', 'sg1.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg2'], 'mcg2.id', '=', 'sg2.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg3'], 'mcg3.id', '=', 'sg3.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg4'], 'mcg4.id', '=', 'sg4.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg5'], 'mcg5.id', '=', 'sg5.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg6'], 'mcg6.id', '=', 'sg6.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg7'], 'mcg7.id', '=', 'sg7.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg8'], 'mcg8.id', '=', 'sg8.id')
			->leftJoin(['moto_cache_item_group_archives', 'mcg9'], 'mcg9.id', '=', 'sg9.id')
			->where(Core_QueryBuilder::expression('COALESCE(mcg9.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg9.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg9.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg8.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg8.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg8.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg7.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg7.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg7.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg6.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg6.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg6.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg5.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg5.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg5.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg4.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg4.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg4.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg3.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg3.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg3.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg2.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg2.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg2.all_cnt, 1)'), '>', 0)
			->where(Core_QueryBuilder::expression('COALESCE(mcg1.site_show_removed, 0)'), '<>', Core_QueryBuilder::expression('COALESCE(mcg1.all_count_items, 1)'))
			->where(Core_QueryBuilder::expression('COALESCE(mcg1.all_cnt, 1)'), '>', 0)
		;
		if($groupByLevel !== false) {
			$coreQueryBuilderTree->
				groupBy("sg{$groupByLevel}.id");
		}
		if(count($aLimitIds)) {
			$coreQueryBuilderTree->
				where("gr.path", 'IN', $aLimitIds);
		}
//\Skynetcore_Utils::tp($coreQueryBuilderTree->build());
		$groups = $coreQueryBuilderTree->execute()->asAssoc()->result();
//		$groups=[];
		$path = \Core_Page::instance()->srcrequest->pathNoPage;
		foreach($groups as $group) {
//			$tree['category']['@attributes'] = [
//				'Hello' => 'Mike'
//			];
//			$group['sg1_dir'] = '1';
//			$group['sg2_dir'] = '12';
//			$group['sg3_dir'] = '123';
//			$group['sg4_dir'] = '1234';
//			$group['sg5_dir'] = '12345';
			$topPropertyAttributes1 = [
				'id' => $group['sg1_id'],
				'parent_id' => $group['sg1_parent_id'],
				'active' => 1
			];
			if(count($propertyToShows) > 0) {
				foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
					if($group["pvi{$propertyToShowId}gid"]==$group['sg1_id']) {
						$topPropertyAttributes1["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
						$topPropertyAttributes1["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
					}
				}
			}
			$tree['category'][$group['sg1_id']]['@attributes'] = $topPropertyAttributes1;
			$tree['category'][$group['sg1_id']]['id'] = $group['sg1_id'];
			$tree['category'][$group['sg1_id']]['name'] = $group['sg1_name'];
			$tree['category'][$group['sg1_id']]['path'] = $group['sg1_path'];
			$tree['category'][$group['sg1_id']]['all_cnt'] = $group['sg1_all_cnt'];
			$tree['category'][$group['sg1_id']]['archive_items_cnt'] = $group['sg1_archive_items_cnt'];
			$tree['category'][$group['sg1_id']]['site_show_cnt'] = $group['sg1_site_show_cnt'];
			$tree['category'][$group['sg1_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 1);
			$tree['category'][$group['sg1_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg1_id'], 3).'/group_'.$group['sg1_id'].'/';
			$tree['category'][$group['sg1_id']]['image_large'] = $group['sg1_image_large'];
			$tree['category'][$group['sg1_id']]['image_small'] = $group['sg1_image_small'];
			$tree['category'][$group['sg1_id']]['items_count'] = $group['sg1_items_count'];
			$tree['category'][$group['sg1_id']]['subgroups_total_count'] = $group['sg1_subgroups_total_count'];
			$tree['category'][$group['sg1_id']]['items_total_count'] = $group['sg1_items_total_count'];
			$tree['category'][$group['sg1_id']]['sorting'] = $group['sg1_sorting'];
			$tree['category'][$group['sg1_id']]['active'] = $group['sg1_active'];
			$tree['category'][$group['sg1_id']]['deleted'] = $group['sg1_deleted'];
			if(!is_null($group['sg2_name'])) {
				$topPropertyAttributes2 = [
					'id' => $group['sg2_id'],
					'parent_id' => $group['sg2_parent_id'],
					'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 2))!==false)*1
				];
				if(count($propertyToShows) > 0) {
					foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
						if($group["pvi{$propertyToShowId}gid"]==$group['sg2_id']) {
							$topPropertyAttributes2["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
							$topPropertyAttributes2["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
						}
					}
				}
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['@attributes'] = $topPropertyAttributes2;
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['id'] = $group['sg2_id'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['name'] = $group['sg2_name'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['path'] = $group['sg2_path'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['all_cnt'] = $group['sg2_all_cnt'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['archive_items_cnt'] = $group['sg2_archive_items_cnt'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['site_show_cnt'] = $group['sg2_site_show_cnt'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 2);
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg2_id'], 3).'/group_'.$group['sg2_id'].'/';
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['image_large'] = $group['sg2_image_large'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['image_small'] = $group['sg2_image_small'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['items_count'] = $group['sg2_items_count'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['subgroups_total_count'] = $group['sg2_subgroups_total_count'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['items_total_count'] = $group['sg2_items_total_count'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['sorting'] = $group['sg2_sorting'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['active'] = $group['sg2_active'];
				$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['deleted'] = $group['sg2_deleted'];
				if(!is_null($group['sg3_name'])) {
					$topPropertyAttributes3 = [
						'id' => $group['sg3_id'],
						'parent_id' => $group['sg3_parent_id'],
						'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 3))!==false)*1
					];
					if(count($propertyToShows) > 0) {
						foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
							if($group["pvi{$propertyToShowId}gid"]==$group['sg3_id']) {
								$topPropertyAttributes3["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
								$topPropertyAttributes3["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
							}
						}
					}
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['@attributes'] = $topPropertyAttributes3;
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['id'] = $group['sg3_id'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['name'] = $group['sg3_name'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['path'] = $group['sg3_path'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['all_cnt'] = $group['sg3_all_cnt'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['archive_items_cnt'] = $group['sg3_archive_items_cnt'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['site_show_cnt'] = $group['sg3_site_show_cnt'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 3);
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg3_id'], 3).'/group_'.$group['sg3_id'].'/';
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['image_large'] = $group['sg3_image_large'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['image_small'] = $group['sg3_image_small'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['items_count'] = $group['sg3_items_count'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['subgroups_total_count'] = $group['sg3_subgroups_total_count'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['items_total_count'] = $group['sg3_items_total_count'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['sorting'] = $group['sg3_sorting'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['active'] = $group['sg3_active'];
					$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['deleted'] = $group['sg3_deleted'];
					if(!is_null($group['sg4_name'])) {
						$topPropertyAttributes4 = [
							'id' => $group['sg4_id'],
							'parent_id' => $group['sg4_parent_id'],
							'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 4))!==false)*1
						];
						if(count($propertyToShows) > 0) {
							foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
								if($group["pvi{$propertyToShowId}gid"]==$group['sg4_id']) {
									$topPropertyAttributes4["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
									$topPropertyAttributes4["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
								}
							}
						}
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['@attributes'] = $topPropertyAttributes4;
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['id'] = $group['sg4_id'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['name'] = $group['sg4_name'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['path'] = $group['sg4_path'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['all_cnt'] = $group['sg4_all_cnt'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['archive_items_cnt'] = $group['sg4_archive_items_cnt'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['site_show_cnt'] = $group['sg4_site_show_cnt'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 4);
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg4_id'], 3).'/group_'.$group['sg4_id'].'/';
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['image_large'] = $group['sg4_image_large'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['image_small'] = $group['sg4_image_small'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['items_count'] = $group['sg4_items_count'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['subgroups_total_count'] = $group['sg4_subgroups_total_count'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['items_total_count'] = $group['sg4_items_total_count'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['sorting'] = $group['sg4_sorting'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['active'] = $group['sg4_active'];
						$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['deleted'] = $group['sg4_deleted'];
						if(!is_null($group['sg5_name'])) {
							$topPropertyAttributes5 = [
								'id' => $group['sg5_id'],
								'parent_id' => $group['sg5_parent_id'],
								'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 5))!==false)*1
							];
							if(count($propertyToShows) > 0) {
								foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
									if($group["pvi{$propertyToShowId}gid"]==$group['sg5_id']) {
										$topPropertyAttributes5["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
										$topPropertyAttributes5["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
									}
								}
							}
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['@attributes'] = $topPropertyAttributes5;
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['id'] = $group['sg5_id'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['name'] = $group['sg5_name'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['path'] = $group['sg5_path'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['all_cnt'] = $group['sg5_all_cnt'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['archive_items_cnt'] = $group['sg5_archive_items_cnt'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['site_show_cnt'] = $group['sg5_site_show_cnt'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 5);
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg5_id'], 3).'/group_'.$group['sg5_id'].'/';
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['image_large'] = $group['sg5_image_large'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['image_small'] = $group['sg5_image_small'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['items_count'] = $group['sg5_items_count'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['subgroups_total_count'] = $group['sg5_subgroups_total_count'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['items_total_count'] = $group['sg5_items_total_count'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['sorting'] = $group['sg5_sorting'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['active'] = $group['sg5_active'];
							$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['deleted'] = $group['sg5_deleted'];
							if(!is_null($group['sg6_name'])) {
								$topPropertyAttributes6 = [
									'id' => $group['sg6_id'],
									'parent_id' => $group['sg6_parent_id'],
									'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 6))!==false)*1
								];
								if(count($propertyToShows) > 0) {
									foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
										if($group["pvi{$propertyToShowId}gid"]==$group['sg6_id']) {
											$topPropertyAttributes6["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
											$topPropertyAttributes6["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
										}
									}
								}
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['@attributes'] = $topPropertyAttributes6;
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['id'] = $group['sg6_id'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['name'] = $group['sg6_name'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['path'] = $group['sg6_path'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['all_cnt'] = $group['sg6_all_cnt'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['archive_items_cnt'] = $group['sg6_archive_items_cnt'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['site_show_cnt'] = $group['sg6_site_show_cnt'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 6);
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg6_id'], 3).'/group_'.$group['sg6_id'].'/';
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['image_large'] = $group['sg6_image_large'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['image_small'] = $group['sg6_image_small'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['items_count'] = $group['sg6_items_count'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['subgroups_total_count'] = $group['sg6_subgroups_total_count'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['items_total_count'] = $group['sg6_items_total_count'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['sorting'] = $group['sg6_sorting'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['active'] = $group['sg6_active'];
								$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['deleted'] = $group['sg6_deleted'];
								if(!is_null($group['sg7_name'])) {
									$topPropertyAttributes7 = [
										'id' => $group['sg7_id'],
										'parent_id' => $group['sg7_parent_id'],
										'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 7))!==false)*1
									];
									if(count($propertyToShows) > 0) {
										foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
											if($group["pvi{$propertyToShowId}gid"]==$group['sg7_id']) {
												$topPropertyAttributes7["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
												$topPropertyAttributes7["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
											}
										}
									}
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['@attributes'] = $topPropertyAttributes7;
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['id'] = $group['sg7_id'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['name'] = $group['sg7_name'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['path'] = $group['sg7_path'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['all_cnt'] = $group['sg7_all_cnt'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['archive_items_cnt'] = $group['sg7_archive_items_cnt'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['site_show_cnt'] = $group['sg7_site_show_cnt'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 7);
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg7_id'], 3).'/group_'.$group['sg7_id'].'/';
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['image_large'] = $group['sg7_image_large'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['image_small'] = $group['sg7_image_small'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['items_count'] = $group['sg7_items_count'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['subgroups_total_count'] = $group['sg7_subgroups_total_count'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['items_total_count'] = $group['sg7_items_total_count'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['sorting'] = $group['sg7_sorting'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['active'] = $group['sg7_active'];
									$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['deleted'] = $group['sg7_deleted'];
									if(!is_null($group['sg8_name'])) {
										$topPropertyAttributes8 = [
											'id' => $group['sg8_id'],
											'parent_id' => $group['sg8_parent_id'],
											'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 8))!==false)*1
										];
										if(count($propertyToShows) > 0) {
											foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
												if($group["pvi{$propertyToShowId}gid"]==$group['sg8_id']) {
													$topPropertyAttributes8["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
													$topPropertyAttributes8["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
												}
											}
										}
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['@attributes'] = $topPropertyAttributes8;
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['id'] = $group['sg8_id'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['name'] = $group['sg7_name'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['path'] = $group['sg7_path'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['all_cnt'] = $group['sg7_all_cnt'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['archive_items_cnt'] = $group['sg7_archive_items_cnt'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['site_show_cnt'] = $group['sg7_site_show_cnt'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 8);
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg8_id'], 3).'/group_'.$group['sg8_id'].'/';
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['image_large'] = $group['sg8_image_large'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['image_small'] = $group['sg8_image_small'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['items_count'] = $group['sg8_items_count'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['subgroups_total_count'] = $group['sg7_subgroups_total_count'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['items_total_count'] = $group['sg8_items_total_count'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['sorting'] = $group['sg8_sorting'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['active'] = $group['sg8_active'];
										$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7_id']]['category'][$group['sg8_id']]['deleted'] = $group['sg8_deleted'];
										if(!is_null($group['sg9_name'])) {
											$topPropertyAttributes9 = [
												'id' => $group['sg9_id'],
												'parent_id' => $group['sg9_parent_id'],
												'active' => (stristr($path, $parentURL.$this->_makeUrlField($group['url'], 9))!==false)*1
											];
											if(count($propertyToShows) > 0) {
												foreach ($propertyToShows as $propertyToShowId => $propertyToShowDefault) {
													if($group["pvi{$propertyToShowId}gid"]==$group['sg9_id']) {
														$topPropertyAttributes9["pvi{$propertyToShowId}gid"] = $group["pvi{$propertyToShowId}gid"];
														$topPropertyAttributes9["pvi{$propertyToShowId}value"] = $group["pvi{$propertyToShowId}value"];
													}
												}
											}
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['@attributes'] = $topPropertyAttributes9;
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['id'] = $group['sg9_id'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['name'] = $group['sg9_name'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['path'] = $group['sg9_path'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['all_cnt'] = $group['sg9_all_cnt'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['archive_items_cnt'] = $group['sg9_archive_items_cnt'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['site_show_cnt'] = $group['sg9_site_show_cnt'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['url'] = $parentURL.$this->_makeUrlField($group['url'], 9);
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['dir'] = '/upload/shop_3/'.Core_File::getNestingDirPath($group['sg9_id'], 3).'/group_'.$group['sg9_id'].'/';
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['image_large'] = $group['sg9_image_large'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['image_small'] = $group['sg9_image_small'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['items_count'] = $group['sg9_items_count'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['subgroups_total_count'] = $group['sg9_subgroups_total_count'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['items_total_count'] = $group['sg9_items_total_count'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['sorting'] = $group['sg9_sorting'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['active'] = $group['sg9_active'];
											$tree['category'][$group['sg1_id']]['category'][$group['sg2_id']]['category'][$group['sg3_id']]['category'][$group['sg4_id']]['category'][$group['sg5_id']]['category'][$group['sg6_id']]['category'][$group['sg7XXX_id']]['category'][$group['sg8_id']]['category'][$group['sg9_id']]['deleted'] = $group['sg9_deleted'];
										}
									}
								}
							}
						}
					}
				}
			}
		}
		$treeForXML = array();
		if(isset($tree['category'])) {
			$index1 = 0;
//			<dir>/upload/shop_3/2/9/2/item_292625/</dir>
//			<image_large>shop_items_catalog_image292625.jpg</image_large>
//			<image_small>small_shop_items_catalog_image292625.jpg</image_small>
//			\Skynetcore_Utils::p($tree);
			foreach($tree['category'] as $list1) {
				if( ($list1['active']==1 && $list1['deleted']==0) || $this->_showInActive) {
					$treeForXML['category'][$index1]['@attributes'] = $list1['@attributes'];
					$treeForXML['category'][$index1]['id'] = $list1['id'];
					$treeForXML['category'][$index1]['name'] = $list1['name'];
					$treeForXML['category'][$index1]['path'] = $list1['path'];
					$treeForXML['category'][$index1]['all_cnt'] = $list1['all_cnt'];
					$treeForXML['category'][$index1]['archive_items_cnt'] = $list1['archive_items_cnt'];
					$treeForXML['category'][$index1]['site_show_cnt'] = $list1['site_show_cnt'];
					$treeForXML['category'][$index1]['url'] = $list1['url'];
					$treeForXML['category'][$index1]['dir'] = $list1['dir'];
					$treeForXML['category'][$index1]['image_large'] = $list1['image_large'];
					$treeForXML['category'][$index1]['image_small'] = $list1['image_small'];
					$treeForXML['category'][$index1]['items_count'] = $list1['items_count'];
					$treeForXML['category'][$index1]['subgroups_total_count'] = $list1['subgroups_total_count'];
					$treeForXML['category'][$index1]['items_total_count'] = $list1['items_total_count'];
					$treeForXML['category'][$index1]['sorting'] = $list1['sorting'];
					$treeForXML['category'][$index1]['active'] = $list1['active'];
					$treeForXML['category'][$index1]['deleted'] = $list1['deleted'];
					$index2 = 0;
					if (isset($list1['category'])) {
						foreach ($list1['category'] as $list2) {
							if( ($list2['active']==1 && $list2['deleted']==0) || $this->_showInActive) {
								$treeForXML['category'][$index1]['category'][$index2]['@attributes'] = $list2['@attributes'];
								$treeForXML['category'][$index1]['category'][$index2]['id'] = $list2['id'];
								$treeForXML['category'][$index1]['category'][$index2]['name'] = $list2['name'];
								$treeForXML['category'][$index1]['category'][$index2]['path'] = $list2['path'];
								$treeForXML['category'][$index1]['category'][$index2]['all_cnt'] = $list2['all_cnt'];
								$treeForXML['category'][$index1]['category'][$index2]['archive_items_cnt'] = $list2['archive_items_cnt'];
								$treeForXML['category'][$index1]['category'][$index2]['site_show_cnt'] = $list2['site_show_cnt'];
								$treeForXML['category'][$index1]['category'][$index2]['url'] = $list2['url'];
								$treeForXML['category'][$index1]['category'][$index2]['dir'] = $list2['dir'];
								$treeForXML['category'][$index1]['category'][$index2]['image_large'] = $list2['image_large'];
								$treeForXML['category'][$index1]['category'][$index2]['image_small'] = $list2['image_small'];
								$treeForXML['category'][$index1]['category'][$index2]['items_count'] = $list2['items_count'];
								$treeForXML['category'][$index1]['category'][$index2]['subgroups_total_count'] = $list2['subgroups_total_count'];
								$treeForXML['category'][$index1]['category'][$index2]['items_total_count'] = $list2['items_total_count'];
								$treeForXML['category'][$index1]['category'][$index2]['sorting'] = $list2['sorting'];
								$treeForXML['category'][$index1]['category'][$index2]['active'] = $list2['active'];
								$treeForXML['category'][$index1]['category'][$index2]['deleted'] = $list2['deleted'];
								if (isset($list2['category'])) {
									$index3 = 0;
									foreach ($list2['category'] as $list3) {
										if( ($list3['active']==1 && $list3['deleted']==0) || $this->_showInActive) {
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['@attributes'] = $list3['@attributes'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['id'] = $list3['id'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['name'] = $list3['name'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['path'] = $list3['path'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['all_cnt'] = $list3['all_cnt'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['archive_items_cnt'] = $list3['archive_items_cnt'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['site_show_cnt'] = $list3['site_show_cnt'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['url'] = $list3['url'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['dir'] = $list3['dir'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['image_large'] = $list3['image_large'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['image_small'] = $list3['image_small'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['items_count'] = $list3['items_count'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['subgroups_total_count'] = $list3['subgroups_total_count'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['items_total_count'] = $list3['items_total_count'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['sorting'] = $list3['sorting'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['active'] = $list3['active'];
											$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['deleted'] = $list3['deleted'];
											if (isset($list3['category'])) {
												$index4 = 0;
												foreach ($list3['category'] as $list4) {
													if( ($list4['active']==1 && $list4['deleted']==0) || $this->_showInActive) {
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['@attributes'] = $list4['@attributes'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['id'] = $list4['id'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['name'] = $list4['name'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['path'] = $list4['path'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['all_cnt'] = $list4['all_cnt'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['archive_items_cnt'] = $list4['archive_items_cnt'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['site_show_cnt'] = $list4['site_show_cnt'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['url'] = $list4['url'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['dir'] = $list4['dir'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['image_large'] = $list4['image_large'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['image_small'] = $list4['image_small'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['items_count'] = $list4['items_count'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['subgroups_total_count'] = $list4['subgroups_total_count'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['items_total_count'] = $list4['items_total_count'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['sorting'] = $list4['sorting'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['active'] = $list4['active'];
														$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['deleted'] = $list4['deleted'];
														if (isset($list4['category'])) {
															$index5 = 0;
															foreach ($list4['category'] as $list5) {
																if( ($list5['active']==1 && $list5['deleted']==0) || $this->_showInActive) {
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['@attributes'] = $list5['@attributes'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['id'] = $list5['id'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['name'] = $list5['name'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['path'] = $list5['path'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['all_cnt'] = $list5['all_cnt'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['archive_items_cnt'] = $list5['archive_items_cnt'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['site_show_cnt'] = $list5['site_show_cnt'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['url'] = $list5['url'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['dir'] = $list5['dir'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['image_large'] = $list5['image_large'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['image_small'] = $list5['image_small'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['items_count'] = $list5['items_count'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['subgroups_total_count'] = $list5['subgroups_total_count'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['items_total_count'] = $list5['items_total_count'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['sorting'] = $list5['sorting'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['active'] = $list5['active'];
																	$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['deleted'] = $list5['deleted'];
																	if (isset($list5['category'])) {
																		$index6 = 0;
																		foreach ($list5['category'] as $list6) {
																			if( ($list6['active']==1 && $list6['deleted']==0) || $this->_showInActive) {
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['@attributes'] = $list6['@attributes'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['id'] = $list6['id'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['name'] = $list6['name'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['path'] = $list6['path'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['all_cnt'] = $list6['all_cnt'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['archive_items_cnt'] = $list6['archive_items_cnt'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['site_show_cnt'] = $list6['site_show_cnt'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['url'] = $list6['url'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['dir'] = $list6['dir'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['image_large'] = $list6['image_large'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['image_small'] = $list6['image_small'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['items_count'] = $list6['items_count'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['subgroups_total_count'] = $list6['subgroups_total_count'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['items_total_count'] = $list6['items_total_count'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['sorting'] = $list6['sorting'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['active'] = $list6['active'];
																				$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['deleted'] = $list6['deleted'];
																				if (isset($list6['category'])) {
																					$index7 = 0;
																					foreach ($list6['category'] as $list7) {
																						if( ($list7['active']==1 && $list7['deleted']==0) || $this->_showInActive) {
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['@attributes'] = $list7['@attributes'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['id'] = $list7['id'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['name'] = $list7['name'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['path'] = $list7['path'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['all_cnt'] = $list7['all_cnt'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['archive_items_cnt'] = $list7['archive_items_cnt'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['site_show_cnt'] = $list7['site_show_cnt'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['url'] = $list7['url'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['dir'] = $list7['dir'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['image_large'] = $list7['image_large'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['image_small'] = $list7['image_small'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['items_count'] = $list7['items_count'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['subgroups_total_count'] = $list7['subgroups_total_count'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['items_total_count'] = $list7['items_total_count'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['sorting'] = $list7['sorting'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['active'] = $list7['active'];
																							$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['deleted'] = $list7['deleted'];
																							if (isset($list8['category'])) {
																								$index8 = 0;
																								foreach ($list7['category'] as $list8) {
																									if( ($list8['active']==1 && $list8['deleted']==0) || $this->_showInActive) {
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['@attributes'] = $list8['@attributes'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['id'] = $list8['id'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['name'] = $list8['name'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['path'] = $list8['path'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['all_cnt'] = $list8['all_cnt'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['archive_items_cnt'] = $list8['archive_items_cnt'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['site_show_cnt'] = $list8['site_show_cnt'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['url'] = $list8['url'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['dir'] = $list8['dir'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['image_large'] = $list8['image_large'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['image_small'] = $list8['image_small'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['items_count'] = $list8['items_count'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['subgroups_total_count'] = $list8['subgroups_total_count'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['items_total_count'] = $list8['items_total_count'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['sorting'] = $list8['sorting'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['active'] = $list8['active'];
																										$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['deleted'] = $list8['deleted'];
																										if (isset($list8['category'])) {
																											$index9 = 0;
																											foreach ($list8['category'] as $list9) {
																												if( ($list9['active']==1 && $list9['deleted']==0) || $this->_showInActive) {
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['@attributes'] = $list8['@attributes'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['id'] = $list9['id'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['name'] = $list9['name'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['path'] = $list9['path'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['all_cnt'] = $list9['all_cnt'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['archive_items_cnt'] = $list9['archive_items_cnt'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['site_show_cnt'] = $list9['site_show_cnt'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['url'] = $list9['url'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['dir'] = $list9['dir'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['image_large'] = $list9['image_large'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['image_small'] = $list9['image_small'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['items_count'] = $list9['items_count'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['subgroups_total_count'] = $list9['subgroups_total_count'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['items_total_count'] = $list9['items_total_count'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['sorting'] = $list9['sorting'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['active'] = $list9['active'];
																													$treeForXML['category'][$index1]['category'][$index2]['category'][$index3]['category'][$index4]['category'][$index5]['category'][$index6]['category'][$index7]['category'][$index8]['category'][$index9]['deleted'] = $list9['deleted'];
//																													if (isset($list9['category'])) {
//																														$index = 0;
//																														foreach ($list9['category'] as $list10) {
//																															if( ($list10['active']==1 && $list10['deleted']==0) || $this->_showInActive) {
//																																...........
//																																$index10++;
//																															}
//																														}
//																													}
																													$index9++;
																												}
																											}
																										}
																										$index8++;
																									}
																								}
																							}
																							$index7++;
																						}
																					}
																				}
																				$index6++;
																			}
																		}
																	}
																	$index5++;
																}
															}
														}
														$index4++;
													}
												}
											}
											$index3++;
										}
									}
								}
								$index2++;
							}
						}
					}
				}
				$index1++;
			}
		}
		$this->_treeAll = $treeForXML;
		return $treeForXML;
	}

	public function fillTreeIDs($groupIDs=array(-1)) {
		if(!is_array($groupIDs)) {
			$groupIDs = array($groupIDs);
		}
		$this->getGroupsTree($groupIDs);
		return $this;
	}

	public function getTreeIds() {
		$this->_treeIds = $this->_processTreeIDs($this->_treeAll);
		return $this->_treeIds;
	}

	private function _processTreeIDs($mas)
	{
		$result = array();
		if (isset($mas['category'])) {
			foreach ($mas['category'] as $value) {
				array_push($result, $value['@attributes']['id']*1);
				if (isset($value['category'])) {
					$dop_mas = $this->_processTreeIDs($value);
					foreach ($dop_mas as $val) {
						array_push($result, $val*1);
					}
				}
			}
		}
		return $result;
	}

	private function _makeUrlField($url, $level) {
		$arURL = explode('/', $url);
		$returnsUrl = '/';
		foreach($arURL as $key=>$urlPart) {
			if(strlen($urlPart)>0) {
				if($key<$level+1+$this->_incLevel) {
					$returnsUrl.=$urlPart.'/';
				} else {
					break;
				}
			}
		}
		return $returnsUrl;
	}

//    private function _recurseGroupsArray($inArray, $outArray, $current=0, $deep=5) {
//        if ($current < $deep) {
//            $this->_recurseGroupsArray($current+1, $deep);
//        } else {
//            return;
//        }
//    }

	/**
	 * @return array
	 */
	public function getAddWhereLists()
	{
		return $this->_addWhereLists;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function setAddWhereLists($addWhereLists)
	{
		$this->_addWhereLists = $addWhereLists;
		return $this;
	}

	public function setBaseDistinct($status=FALSE)
	{
		$this->_addBaseDistinct = $status;
		return $this;
	}

	public function setItemsRestriction($property_id=0, $restrictPath='')
	{
		$this->_addItemRestriction = array('property_id'=>$property_id, 'restrictPath'=>$restrictPath);
		return $this;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function addWhereLists($addWhereListItem)
	{
		$this->_addWhereLists[] = $addWhereListItem;
		return $this;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function clearWhereLists()
	{
		$this->_addWhereLists = array();
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAddWhere()
	{
		return $this->_addWhere;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function setAddWhere($addWhere)
	{
		$this->_addWhere = $addWhere;
		return $this;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function addWhere($addWhereItem)
	{
		$this->_addWhere[] = $addWhereItem;
		return $this;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function clearWhere()
	{
		$this->_addWhere = array();
		return $this;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function addOrderBy($addOrderByItem)
	{
		$this->_addOrderBy[] = $addOrderByItem;
		return $this;
	}

	/**
	 * @param array $addWhereLists
	 */
	public function clearOrderBy()
	{
		$this->_addOrderBy = array();
		return $this;
	}

	public static function getAllParenGroups($gid, $reverse=true, $model='Shop_Group', $step=1)
	{
		$group = Core_Entity::factory($model, $gid);
		$groups[$group->id] = $group;
		$parent = $group->getParent();

		if ($parent) {
			$groupsLocal = self::getAllParenGroups($parent->id, $reverse, $model, $step+1);
		} else {
			return array($group->id => $group);
		}
		$results = $groups + $groupsLocal;
		if($step==1 && $reverse) {
			$results = array_reverse($results, true);
		}
		return $results;
	}

	public static function getGroupByPath($groupPath, $parentGroupID = false, $shopId = 3)
	{
			$mGroups = Core_Entity::factory('Shop_Group');
			$mGroups
					->queryBuilder()
					->where('shop_id', '=', $shopId)
					->where('deleted', '=', 0)
					->where('active', '=', 1)
			;
			if ($parentGroupID !== false) {
					$mGroups
							->queryBuilder()
							->where('parent_id', '=', $parentGroupID);
			}
			$oGroup = NULL;
			(trim($groupPath) != "") && $oGroup = $mGroups->getByPath($groupPath);

			return $oGroup;
	}

	public static function getGroupsByTwoLevels($shop_id=3)
	{
		$rq2 = Core_QueryBuilder::select('id')
			->from('shop_groups')
			->where('parent_id', '=', 0)
			->where('shop_id', '=', $shop_id)
			->where('active', '=', 1)
			->where('deleted', '=', 0);


		$rq = Core_QueryBuilder::select('id')
			->from('shop_groups')
			->where('parent_id', '=', 0)
			->where('shop_id', '=', $shop_id)
			->where('active', '=', 1)
			->where('deleted', '=', 0)
			->setOr()
			->where('parent_id', 'IN', $rq2);
		$selectTwoLevels = $rq
			->execute()
			->asAssoc()
			->result();
		$groups_id = array();
		if (isset($selectTwoLevels)) {
			foreach ($selectTwoLevels as $key => $data) {
				$groups_id[] = $data['id'];
			}
			return $groups_id;
		}
		return NULL;
	}

	public function fillJStree(&$items=array(), $itemsIdsSelected=array(), $disabledStatus=true) {
		if(count($items)==0 && $this->_addItemsToJS) {
			$groupIDs = $this->getTreeIds();
			$itemsQuery = Core_QueryBuilder::select(array(Core_QueryBuilder::expression("CONCAT('i', si.id)"), 'id'))
				->select('si.shop_group_id')
				->select(array(Core_QueryBuilder::expression("CONCAT('(', si.marking,') ', si.name)"), 'text'))
				->from(array('shop_items', 'si'))
				->where('si.shop_group_id', 'IN', $groupIDs)
				->where('si.active', '=', 1)
				->where('si.deleted', '=', 0)
				->orderBy('si.shop_group_id')
				->orderBy('si.name')
			;
			$items = $itemsQuery->execute()->asAssoc()->result();
			$items = utl::getArrayKeyValuesFromArrays($items, 'shop_group_id');

		}
		$processedTree = $this->_processTreeJS($this->_treeAll, 0, $items, $itemsIdsSelected, $disabledStatus);

		return $processedTree;
	}

	public function addItemsToJS($status) {
		$this->_addItemsToJS = $status;
		return $this;
	}

	private function _processTreeJS($mas, $level=0, &$items=array(), $itemsIdsSelected=array(), $bottomOnly=true) {
		$result = array();
		if (isset($mas['category'])) {
			foreach ($mas['category'] as $value) {
				$resultItem = array(
					'id' => 'g'.$value['id'],
					'icon' => 'fa fa-folder-o',
					'text' => $value['name'],
					'state' => array(
						'disabled'=>$bottomOnly,
					),
				);
				if (isset($value['category'])) {
					$dop_mas = $this->_processTreeJS($value, $level+1, $items, $itemsIdsSelected, $bottomOnly);
					if(count($dop_mas)>0) {
//						foreach ($dop_mas as $dopms) {
//							in_array($dopms['id'], $itemsIdsSelected) && $dopms['state']['selected']=true;
//						}
//						$resultItem['state'] = array(
//							'disabled'=>$disabledStatus,
//						);
//						in_array($resultItem['id'], $itemsIdsSelected) && $resultItem['state']['selected']=true;
						$resultItem['children'] = $dop_mas;
					}
				}
				if(isset($items[$value['id']]) && count($items[$value['id']])>0) {
					foreach ($items[$value['id']] as $eItem) {
						$eItem['icon'] = 'fa fa-file-o';
						$eItem['type'] = 'file';
						$eItem['children'] = false;
//						$eItem['state'] = array(
//							'disabled'=>true,
//						);
//						in_array($eItem['id'], $itemsIdsSelected) && $eItem['state']['selected']=true;
						$resultItem['children'][] = $eItem;
					}
				}
				if($bottomOnly && !isset($resultItem['children'])) {
					$resultItem['state'] = array(
						'disabled'=>false,
					);
				}
				if(isset($resultItem['children'])) {
					foreach ($resultItem['children'] as &$resItem) {
						in_array($resItem['id'], $itemsIdsSelected) && $resItem['state']['selected']=true;
					}
				} elseif(in_array($resultItem['id'], $itemsIdsSelected)) {
					$resultItem['state']['selected']=true;
				}
//				utl::p($resultItem);
				$result[] = $resultItem;
			}
		}
		return $result;
	}

	/**
	 * @return boolean
	 */
	public function isShowInActive()
	{
		return $this->_showInActive;
	}

	/**
	 * @param boolean $showInActive
	 */
	public function showInActive($showInActive=FALSE)
	{
		$this->_showInActive = $showInActive;
		return $this;
	}
}