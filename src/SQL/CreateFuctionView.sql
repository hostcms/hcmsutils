--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.3.358.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 31.08.2015 20:31:45
-- Версия сервера: 5.5.35-0+wheezy1
-- Версия клиента: 4.1
--

-- Выполнить для создания таблицы с REST сервисами --
/*
DROP TABLE IF EXISTS rest_tokens;
CREATE TABLE rest_tokens (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  token varchar(255) DEFAULT NULL,
  siteuser_id int(11) DEFAULT NULL,
  create_date datetime DEFAULT NULL,
  expired_date datetime DEFAULT NULL,
  deleted tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица пользовательских токенов для REST сервиса';
*/
DROP TABLE IF EXISTS all_active_items;
DROP TABLE IF EXISTS getGroupsIerarchyDown;
DROP TABLE IF EXISTS getGroupsIerarchyDownToUp;
DROP TABLE IF EXISTS getGroupsIerarchyUp;
DROP TABLE IF EXISTS getPropertyDirsIerarchyUp;
DROP TABLE IF EXISTS getIsGroupsIerarchyDown;
DROP TABLE IF EXISTS properties_tech_floats;
DROP TABLE IF EXISTS properties_tech_ints;
DROP TABLE IF EXISTS properties_tech_strings;
DROP TABLE IF EXISTS properties_tech_texts;

DROP VIEW IF EXISTS all_active_items CASCADE;
DROP VIEW IF EXISTS getGroupsIerarchyDown CASCADE;
DROP VIEW IF EXISTS getGroupsIerarchyDownToUp CASCADE;
DROP VIEW IF EXISTS getGroupsIerarchyUp CASCADE;
DROP VIEW IF EXISTS getPropertyDirsIerarchyUp CASCADE;
DROP VIEW IF EXISTS getIsGroupsIerarchyDown CASCADE;
DROP VIEW IF EXISTS properties_tech_floats CASCADE;
DROP VIEW IF EXISTS properties_tech_ints CASCADE;
DROP VIEW IF EXISTS properties_tech_strings CASCADE;
DROP VIEW IF EXISTS properties_tech_texts CASCADE;

--
-- Изменить таблицу "property_dirs"
--
ALTER TABLE property_dirs ADD COLUMN column_num TINYINT(4) NOT NULL DEFAULT 0;
--
-- Изменить таблицу "list_items"
--
ALTER TABLE list_items ADD COLUMN linkaddr VARCHAR(255) DEFAULT NULL;
ALTER TABLE list_items ADD COLUMN `path` VARCHAR(255) DEFAULT NULL;

DELIMITER $$

CREATE FUNCTION get_type (itype int)
RETURNS varchar(255) charset utf8
DETERMINISTIC
SQL SECURITY INVOKER
BEGIN
  RETURN CASE WHEN itype = 0 THEN 'Целое число (INT)' WHEN itype = 1 THEN 'Строка (STRING)' WHEN itype = 3 THEN 'Список (INT, целое число)' WHEN itype = 4 THEN 'Большое текстовое поле (STRING, строка)' WHEN itype = 7 THEN 'Флажок(INT, целое число, 0-FALSE, 1-TRUE)' WHEN itype = 11 THEN 'Число с плавающей запятой (FLOAT)' ELSE itype END
  ;
END
$$

CREATE FUNCTION select_shop_groups (value int)
RETURNS int(11)
SQL SECURITY INVOKER
READS SQL DATA
BEGIN
  DECLARE _id int;
  DECLARE _parent int;
  DECLARE _next int;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;

  SET _parent = @id;
  SET _id = -1;

  IF @id IS NULL THEN
    RETURN NULL;
  END IF;

  LOOP
    SELECT
      MIN(id) INTO @id
    FROM shop_groups
    WHERE parent_id = _parent
    AND id > _id;
    IF @id IS NOT NULL
      OR _parent = @start_with THEN
      SET @level = @level + 1;
      RETURN @id;
    END IF;
    SET @level := @level - 1;
    SELECT
      id,
      parent_id INTO _id, _parent
    FROM shop_groups sg
    WHERE id = _parent;
  END LOOP;
END
$$

CREATE FUNCTION split_string (str text, delim varchar(12), pos int)
RETURNS text charset utf8
DETERMINISTIC
SQL SECURITY INVOKER
  RETURN
  REPLACE(
  SUBSTRING(
  SUBSTRING_INDEX(str, delim, pos),
  CHAR_LENGTH(
  SUBSTRING_INDEX(str, delim, pos - 1)
  ) + 1
  ),
  delim,
  ''
  )
$$

DELIMITER ;

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW all_active_items
AS
SELECT
  `si`.`id` AS `IE_XML_ID`,
  `si`.`marking` AS `IP_PROP110`,
  COALESCE(COALESCE((CASE WHEN ((`sg1`.`name` IS NOT NULL) AND
      (`sg2`.`name` IS NOT NULL) AND
      (`sg3`.`name` IS NOT NULL)) THEN `sg1`.`name` WHEN (ISNULL(`sg1`.`name`) AND
      ISNULL(`sg2`.`name`)) THEN `sg3`.`name` ELSE NULL END), `sg2`.`name`), '') AS `IC_GROUP0`,
  COALESCE(COALESCE((CASE WHEN ((`sg1`.`name` IS NOT NULL) AND
      (`sg2`.`name` IS NOT NULL) AND
      (`sg3`.`name` IS NOT NULL)) THEN `sg2`.`name` ELSE NULL END), (CASE WHEN (ISNULL(`sg1`.`name`) AND
      ISNULL(`sg2`.`name`)) THEN NULL ELSE `sg3`.`name` END)), '') AS `IC_GROUP1`,
  COALESCE(COALESCE((CASE WHEN ((`sg1`.`name` IS NOT NULL) AND
      (`sg2`.`name` IS NOT NULL) AND
      (`sg3`.`name` IS NOT NULL)) THEN `sg3`.`name` ELSE NULL END), `sg1`.`name`), '') AS `IC_GROUP2`,
  REPLACE(TRIM(`si`.`name`), '\\"', '""') AS `IE_NAME`,
  `si`.`price` AS `CV_PRICE_1`,
  'RUB' AS `CV_CURRENCY_1`
FROM (((`shop_items` `si`
  JOIN `shop_groups` `sg3`
    ON ((`sg3`.`id` = `si`.`shop_group_id`)))
  LEFT JOIN `shop_groups` `sg2`
    ON ((`sg2`.`id` = `sg3`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg1`
    ON ((`sg1`.`id` = `sg2`.`parent_id`)))
WHERE ((`si`.`active` = 1)
AND (`si`.`deleted` = 0)
AND (`si`.`modification_id` = 0)
AND (`si`.`shortcut_id` = 0)
AND (`sg3`.`path` <> 'pages')
AND (LENGTH(`si`.`marking`) = 13)
AND (COALESCE(`sg1`.`active`, 1) = 1)
AND (COALESCE(`sg2`.`active`, 1) = 1)
AND (COALESCE(`sg3`.`active`, 1) = 1));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW getGroupsIerarchyDown
AS
SELECT
  `sg1`.`id` AS `id`,
  CONCAT_WS('/', `sg1`.`id`, `sg2`.`id`, `sg3`.`id`, `sg4`.`id`, `sg5`.`id`, `sg6`.`id`, `sg7`.`id`, `sg8`.`id`, `sg9`.`id`) AS `path`,
  CONCAT('/', CONCAT_WS('/', `sg1`.`path`, `sg2`.`path`, `sg3`.`path`, `sg4`.`path`, `sg5`.`path`, `sg6`.`path`, `sg7`.`path`, `sg8`.`path`, `sg9`.`path`), '/') AS `url`
FROM ((((((((`shop_groups` `sg1`
  LEFT JOIN `shop_groups` `sg2`
    ON ((`sg1`.`id` = `sg2`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg3`
    ON ((`sg2`.`id` = `sg3`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg4`
    ON ((`sg3`.`id` = `sg4`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg5`
    ON ((`sg4`.`id` = `sg5`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg6`
    ON ((`sg5`.`id` = `sg6`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg7`
    ON ((`sg6`.`id` = `sg7`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg8`
    ON ((`sg7`.`id` = `sg8`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg9`
    ON ((`sg8`.`id` = `sg9`.`parent_id`)));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW getGroupsIerarchyDownToUp
AS
SELECT
  `sg1`.`id` AS `downDirectoryID`,
  `sg1`.`name` AS `downDirectoryName`,
  CONCAT_WS('/', `sg9`.`id`, `sg8`.`id`, `sg7`.`id`, `sg6`.`id`, `sg5`.`id`, `sg4`.`id`, `sg3`.`id`, `sg2`.`id`, `sg1`.`id`) AS `path`,
  CONCAT('/', CONCAT_WS('/', `sg9`.`path`, `sg8`.`path`, `sg7`.`path`, `sg6`.`path`, `sg5`.`path`, `sg4`.`path`, `sg3`.`path`, `sg2`.`path`, `sg1`.`path`), '/') AS `url`,
  CONCAT_WS('/', `sg1`.`id`, `sg2`.`id`, `sg3`.`id`, `sg4`.`id`, `sg5`.`id`, `sg6`.`id`, `sg7`.`id`, `sg8`.`id`, `sg9`.`id`) AS `back_path`,
  CONCAT('/', CONCAT_WS('/', `sg1`.`path`, `sg2`.`path`, `sg3`.`path`, `sg4`.`path`, `sg5`.`path`, `sg6`.`path`, `sg7`.`path`, `sg8`.`path`, `sg9`.`path`), '/') AS `back_url`,
  `sg1`.`shop_id` AS `shop_id`
FROM ((((((((`shop_groups` `sg1`
  LEFT JOIN `shop_groups` `sg2`
    ON (((`sg2`.`id` = `sg1`.`parent_id`)
    AND (`sg1`.`active` = 1)
    AND (`sg1`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg3`
    ON (((`sg3`.`id` = `sg2`.`parent_id`)
    AND (`sg2`.`active` = 1)
    AND (`sg2`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg4`
    ON (((`sg4`.`id` = `sg3`.`parent_id`)
    AND (`sg3`.`active` = 1)
    AND (`sg3`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg5`
    ON (((`sg5`.`id` = `sg4`.`parent_id`)
    AND (`sg4`.`active` = 1)
    AND (`sg4`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg6`
    ON (((`sg6`.`id` = `sg5`.`parent_id`)
    AND (`sg5`.`active` = 1)
    AND (`sg5`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg7`
    ON (((`sg7`.`id` = `sg6`.`parent_id`)
    AND (`sg6`.`active` = 1)
    AND (`sg6`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg8`
    ON (((`sg8`.`id` = `sg7`.`parent_id`)
    AND (`sg7`.`active` = 1)
    AND (`sg7`.`deleted` = 0))))
  LEFT JOIN `shop_groups` `sg9`
    ON (((`sg9`.`id` = `sg8`.`parent_id`)
    AND (`sg8`.`active` = 1)
    AND (`sg8`.`deleted` = 0))));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW getGroupsIerarchyUp
AS
SELECT
  `si`.`id` AS `id`,
  CONCAT_WS('/', `sg9`.`id`, `sg8`.`id`, `sg7`.`id`, `sg6`.`id`, `sg5`.`id`, `sg4`.`id`, `sg3`.`id`, `sg2`.`id`, `sg1`.`id`) AS `path`,
  CONCAT('/', CONCAT_WS('/', `sg9`.`path`, `sg8`.`path`, `sg7`.`path`, `sg6`.`path`, `sg5`.`path`, `sg4`.`path`, `sg3`.`path`, `sg2`.`path`, `sg1`.`path`, `si`.`path`), '/') AS `url`
FROM (((((((((`shop_items` `si`
  JOIN `shop_groups` `sg1`
    ON ((`sg1`.`id` = `si`.`shop_group_id`)))
  LEFT JOIN `shop_groups` `sg2`
    ON ((`sg2`.`id` = `sg1`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg3`
    ON ((`sg3`.`id` = `sg2`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg4`
    ON ((`sg4`.`id` = `sg3`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg5`
    ON ((`sg5`.`id` = `sg4`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg6`
    ON ((`sg6`.`id` = `sg5`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg7`
    ON ((`sg7`.`id` = `sg6`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg8`
    ON ((`sg8`.`id` = `sg7`.`parent_id`)))
  LEFT JOIN `shop_groups` `sg9`
    ON ((`sg9`.`id` = `sg8`.`parent_id`)));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW getPropertyDirsIerarchyUp
AS
SELECT
  `p`.`id` AS `id`,
  CONCAT_WS('/', `sg9`.`id`, `sg8`.`id`, `sg7`.`id`, `sg6`.`id`, `sg5`.`id`, `sg4`.`id`, `sg3`.`id`, `sg2`.`id`, `sg1`.`id`) AS `path`,
  CONCAT('/', CONCAT_WS('/', `sg9`.`name`, `sg8`.`name`, `sg7`.`name`, `sg6`.`name`, `sg5`.`name`, `sg4`.`name`, `sg3`.`name`, `sg2`.`name`, `sg1`.`name`), '/') AS `url`
FROM (((((((((`properties` `p`
  JOIN `property_dirs` `sg1`
    ON ((`sg1`.`id` = `p`.`property_dir_id`)))
  LEFT JOIN `property_dirs` `sg2`
    ON ((`sg2`.`id` = `sg1`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg3`
    ON ((`sg3`.`id` = `sg2`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg4`
    ON ((`sg4`.`id` = `sg3`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg5`
    ON ((`sg5`.`id` = `sg4`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg6`
    ON ((`sg6`.`id` = `sg5`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg7`
    ON ((`sg7`.`id` = `sg6`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg8`
    ON ((`sg8`.`id` = `sg7`.`parent_id`)))
  LEFT JOIN `property_dirs` `sg9`
    ON ((`sg9`.`id` = `sg8`.`parent_id`)));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW properties_tech_floats
AS
SELECT
  `pv`.`entity_id` AS `entity_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 1), '') AS `property_l1_dir_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 1), '') AS `property_parent_dir_name`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 1), '') AS `property_parent_dir_sorting`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 1), '') AS `property_parent_dir_column_num`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 2), ''), 1000000000000) AS `property_l2_dir_id`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 2), ''), 'Основные') AS `property_l2_dir_name`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 2), ''), 10000) AS `property_l2_dir_sorting`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 2), ''), 1) AS `property_l2_dir_column_num`,
  `p`.`id` AS `property_id`,
  `p`.`name` AS `property_name`,
  `p`.`type` AS `property_type_id`,
  `p`.`sorting` AS `property_sorting`,
  `li`.`linkaddr` AS `property_link`,
  `pv`.`value` AS `value`,
  NULL AS `list_item_id`,
  NULL AS `list_id`,
  NULL AS `list_path`,
  NULL AS `list_linkaddr`,
  `get_type`(`p`.`type`) AS `property_type`,
  (CASE WHEN ((`p`.`type` = 11) AND
      (TRIM(`pv`.`value`) = '')) THEN -(1) ELSE 1 END) AS `visible`
FROM ((((((((((((`property_value_floats` `pv`
  JOIN `properties` `p`
    ON (((`pv`.`property_id` = `p`.`id`)
    AND (`p`.`type` = 11))))
  LEFT JOIN `property_dirs` `pd1`
    ON ((`pd1`.`id` = `p`.`property_dir_id`)))
  LEFT JOIN `property_dirs` `pd2`
    ON ((`pd2`.`id` = `pd1`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd3`
    ON ((`pd3`.`id` = `pd2`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd4`
    ON ((`pd4`.`id` = `pd3`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd5`
    ON ((`pd5`.`id` = `pd4`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd6`
    ON ((`pd6`.`id` = `pd5`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd7`
    ON ((`pd7`.`id` = `pd6`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd8`
    ON ((`pd8`.`id` = `pd7`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd9`
    ON ((`pd9`.`id` = `pd8`.`parent_id`)))
  LEFT JOIN `lists` `l`
    ON ((`l`.`id` = `p`.`list_id`)))
  LEFT JOIN `list_items` `li`
    ON ((`li`.`id` = `pv`.`value`)));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW properties_tech_ints
AS
SELECT
  `pv`.`entity_id` AS `entity_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 1), '') AS `property_l1_dir_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 1), '') AS `property_parent_dir_name`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 1), '') AS `property_parent_dir_sorting`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 1), '') AS `property_parent_dir_column_num`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 2), ''), 1000000000000) AS `property_l2_dir_id`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 2), ''), 'Основные') AS `property_l2_dir_name`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 2), ''), 10000) AS `property_l2_dir_sorting`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 2), ''), 1) AS `property_l2_dir_column_num`,
  `p`.`id` AS `property_id`,
  `p`.`name` AS `property_name`,
  `p`.`type` AS `property_type_id`,
  `p`.`sorting` AS `property_sorting`,
  `li`.`linkaddr` AS `property_link`,
  (CASE WHEN (`p`.`type` = 3) THEN `li`.`value` ELSE `pv`.`value` END) AS `value`,
  `li`.`id` AS `list_item_id`,
  `li`.`list_id` AS `list_id`,
  `li`.`path` AS `list_path`,
  `li`.`linkaddr` AS `list_linkaddr`,
  `get_type`(`p`.`type`) AS `property_type`,
  (CASE WHEN ((`p`.`type` = 1) AND
      (TRIM(`pv`.`value`) = '')) THEN -(1) ELSE 1 END) AS `visible`
FROM ((((((((((((`property_value_ints` `pv`
  JOIN `properties` `p`
    ON (((`pv`.`property_id` = `p`.`id`)
    AND (`p`.`type` IN (0, 3, 7)))))
  LEFT JOIN `property_dirs` `pd1`
    ON ((`pd1`.`id` = `p`.`property_dir_id`)))
  LEFT JOIN `property_dirs` `pd2`
    ON ((`pd2`.`id` = `pd1`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd3`
    ON ((`pd3`.`id` = `pd2`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd4`
    ON ((`pd4`.`id` = `pd3`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd5`
    ON ((`pd5`.`id` = `pd4`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd6`
    ON ((`pd6`.`id` = `pd5`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd7`
    ON ((`pd7`.`id` = `pd6`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd8`
    ON ((`pd8`.`id` = `pd7`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd9`
    ON ((`pd9`.`id` = `pd8`.`parent_id`)))
  LEFT JOIN `lists` `l`
    ON ((`l`.`id` = `p`.`list_id`)))
  LEFT JOIN `list_items` `li`
    ON ((`li`.`id` = `pv`.`value`)));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW properties_tech_strings
AS
SELECT
  `pv`.`entity_id` AS `entity_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 1), '') AS `property_l1_dir_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 1), '') AS `property_parent_dir_name`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 1), '') AS `property_parent_dir_sorting`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 1), '') AS `property_parent_dir_column_num`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 2), ''), 1000000000000) AS `property_l2_dir_id`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 2), ''), 'Основные') AS `property_l2_dir_name`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 2), ''), 10000) AS `property_l2_dir_sorting`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 2), ''), 1) AS `property_l2_dir_column_num`,
  `p`.`id` AS `property_id`,
  `p`.`name` AS `property_name`,
  `p`.`type` AS `property_type_id`,
  `p`.`sorting` AS `property_sorting`,
  `li`.`linkaddr` AS `property_link`,
  (CASE WHEN (`p`.`type` = 3) THEN `li`.`value` ELSE `pv`.`value` END) AS `value`,
  NULL AS `list_item_id`,
  NULL AS `list_id`,
  NULL AS `list_path`,
  NULL AS `list_linkaddr`,
  `get_type`(`p`.`type`) AS `property_type`,
  (CASE WHEN ((`p`.`type` = 1) AND
      (TRIM(`pv`.`value`) = '')) THEN -(1) ELSE 1 END) AS `visible`
FROM ((((((((((((`property_value_strings` `pv`
  JOIN `properties` `p`
    ON (((`pv`.`property_id` = `p`.`id`)
    AND (`p`.`type` = 1))))
  LEFT JOIN `property_dirs` `pd1`
    ON ((`pd1`.`id` = `p`.`property_dir_id`)))
  LEFT JOIN `property_dirs` `pd2`
    ON ((`pd2`.`id` = `pd1`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd3`
    ON ((`pd3`.`id` = `pd2`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd4`
    ON ((`pd4`.`id` = `pd3`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd5`
    ON ((`pd5`.`id` = `pd4`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd6`
    ON ((`pd6`.`id` = `pd5`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd7`
    ON ((`pd7`.`id` = `pd6`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd8`
    ON ((`pd8`.`id` = `pd7`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd9`
    ON ((`pd9`.`id` = `pd8`.`parent_id`)))
  LEFT JOIN `lists` `l`
    ON ((`l`.`id` = `p`.`list_id`)))
  LEFT JOIN `list_items` `li`
    ON ((`li`.`id` = `pv`.`value`)));

CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW properties_tech_texts
AS
SELECT
  `pv`.`entity_id` AS `entity_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 1), '') AS `property_l1_dir_id`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 1), '') AS `property_parent_dir_name`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 1), '') AS `property_parent_dir_sorting`,
  NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 1), '') AS `property_parent_dir_column_num`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`id`, `pd8`.`id`, `pd7`.`id`, `pd6`.`id`, `pd5`.`id`, `pd4`.`id`, `pd3`.`id`, `pd2`.`id`, `pd1`.`id`), CHAR(0), 2), ''), 1000000000000) AS `property_l2_dir_id`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`name`, `pd8`.`name`, `pd7`.`name`, `pd6`.`name`, `pd5`.`name`, `pd4`.`name`, `pd3`.`name`, `pd2`.`name`, `pd1`.`name`), CHAR(0), 2), ''), 'Основные') AS `property_l2_dir_name`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`sorting`, `pd8`.`sorting`, `pd7`.`sorting`, `pd6`.`sorting`, `pd5`.`sorting`, `pd4`.`sorting`, `pd3`.`sorting`, `pd2`.`sorting`, `pd1`.`sorting`), CHAR(0), 2), ''), 10000) AS `property_l2_dir_sorting`,
  COALESCE(NULLIF(`split_string`(CONCAT_WS(CHAR(0), `pd9`.`column_num`, `pd8`.`column_num`, `pd7`.`column_num`, `pd6`.`column_num`, `pd5`.`column_num`, `pd4`.`column_num`, `pd3`.`column_num`, `pd2`.`column_num`, `pd1`.`column_num`), CHAR(0), 2), ''), 1) AS `property_l2_dir_column_num`,
  `p`.`id` AS `property_id`,
  `p`.`name` AS `property_name`,
  `p`.`type` AS `property_type_id`,
  `p`.`sorting` AS `property_sorting`,
  `li`.`linkaddr` AS `property_link`,
  `pv`.`value` AS `value`,
  NULL AS `list_item_id`,
  NULL AS `list_id`,
  NULL AS `list_path`,
  NULL AS `list_linkaddr`,
  `get_type`(`p`.`type`) AS `property_type`,
  (CASE WHEN ((`p`.`type` = 1) AND
      (TRIM(`pv`.`value`) = '')) THEN -(1) ELSE 1 END) AS `visible`
FROM ((((((((((((`property_value_texts` `pv`
  JOIN `properties` `p`
    ON (((`pv`.`property_id` = `p`.`id`)
    AND (`p`.`type` = 4))))
  LEFT JOIN `property_dirs` `pd1`
    ON ((`pd1`.`id` = `p`.`property_dir_id`)))
  LEFT JOIN `property_dirs` `pd2`
    ON ((`pd2`.`id` = `pd1`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd3`
    ON ((`pd3`.`id` = `pd2`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd4`
    ON ((`pd4`.`id` = `pd3`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd5`
    ON ((`pd5`.`id` = `pd4`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd6`
    ON ((`pd6`.`id` = `pd5`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd7`
    ON ((`pd7`.`id` = `pd6`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd8`
    ON ((`pd8`.`id` = `pd7`.`parent_id`)))
  LEFT JOIN `property_dirs` `pd9`
    ON ((`pd9`.`id` = `pd8`.`parent_id`)))
  LEFT JOIN `lists` `l`
    ON ((`l`.`id` = `p`.`list_id`)))
  LEFT JOIN `list_items` `li`
    ON ((`li`.`id` = `pv`.`value`)));


CREATE OR REPLACE
SQL SECURITY INVOKER
VIEW getIsGroupsIerarchyDown
AS
SELECT
  `sg1`.`id` AS `id`,
  CONCAT_WS('/', `sg1`.`id`, `sg2`.`id`, `sg3`.`id`, `sg4`.`id`, `sg5`.`id`, `sg6`.`id`, `sg7`.`id`, `sg8`.`id`, `sg9`.`id`) AS `path`,
  CONCAT('/', CONCAT_WS('/', `sg1`.`path`, `sg2`.`path`, `sg3`.`path`, `sg4`.`path`, `sg5`.`path`, `sg6`.`path`, `sg7`.`path`, `sg8`.`path`, `sg9`.`path`), '/') AS `url`
FROM ((((((((`informationsystem_groups` `sg1`
  LEFT JOIN `informationsystem_groups` `sg2`
    ON ((`sg1`.`id` = `sg2`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg3`
    ON ((`sg2`.`id` = `sg3`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg4`
    ON ((`sg3`.`id` = `sg4`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg5`
    ON ((`sg4`.`id` = `sg5`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg6`
    ON ((`sg5`.`id` = `sg6`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg7`
    ON ((`sg6`.`id` = `sg7`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg8`
    ON ((`sg7`.`id` = `sg8`.`parent_id`)))
  LEFT JOIN `informationsystem_groups` `sg9`
    ON ((`sg8`.`id` = `sg9`.`parent_id`)));