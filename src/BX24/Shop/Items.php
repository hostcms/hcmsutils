<?php
namespace BX24\Shop;

use BX24\Service;
use Core_Entity;
use HCMS\Shop\Groups as gr;
use HCMS\Utils as utl;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 26.10.2015
 * Time: 22:11
 */

class Items extends Base
{

    private function _bxAddItem($section_id, $xml_id, $name, $price) {
        $resp = $this->_serviceClient->restPost('crm.product.add', array(
            'fields[SECTION_ID]' => $section_id,
            'fields[XML_ID]' => $xml_id,
            'fields[NAME]' => $name,
            'fields[PRICE]' => $price,
            'fields[CURRENCY_ID]' => 'RUB',
        ));
//        utl::p($resp);
        return $resp;
    }

    public function bxReadAllItems() {
        $returnes = NULL;
        $postArrayParams = array(
            'filter[CATALOG_ID]' => $this->_catalogID,
        );
        for($i=0; $i<1000000; $i++) {
            $resp = $this->_serviceClient->restPost('crm.product.list', $postArrayParams);
//            utl::p($resp->next);
            if(!isset($resp->next)) {
                if (isset($returnes->result) && is_array($returnes->result)) {
                    $returnes->result = array_merge($returnes->result, $resp->result);
                } else {
                    $returnes = $resp;
                }
                break;
            } else {
                if (isset($returnes->result) && is_array($returnes->result)) {
                    $returnes->result = array_merge($returnes->result, $resp->result);
                } else {
                    $returnes = $resp;
                }
                $postArrayParams['start'] = $resp->next;
            }
        }
//        utl::p($returnes);
        return $returnes;
    }

    public function execute($hcmsParentGroup)
    {
        $hcmsTopGroups = Core_Entity::factory('Shop_Group');
        $hcmsTopGroups
            ->queryBuilder()
            ->where('shop_id', '=', $this->_hcmsShopID)
            ->where('parent_id', '=', $hcmsParentGroup)
            ->where('active', '=', 1)
            ->where('deleted', '=', 0)
//            ->where('name', '=', '_Test')
//            ->limit(1)
        ;
        $groupIDs = array();
        foreach($hcmsTopGroups->findAll() as $group) {
            $groupIDs[] = $group->id;
        }
        $getCatalogItems = $this->bxReadAllItems();
        $bxCatalogItems = array();
        utl::p($getCatalogItems);
        if($getCatalogItems->error==0) {
            foreach($getCatalogItems->result as $bxCatalogItem) {
                $bxCatalogItems[$bxCatalogItem->XML_ID] = $bxCatalogItem;
            }
        }
//        utl::p($groupIDs);
        if(true && count($groupIDs)>0 ) {
            $groups = gr::createInstance();
            $allGroups = $groups->fillTreeIDs($groupIDs)->getTreeIds();
            $hcmsItemsModel = Core_Entity::factory('Shop_Item');
            utl::p($allGroups);
            $hcmsItemsModel
                ->queryBuilder()
                ->where('shop_id', '=', $this->_hcmsShopID)
                ->where('shop_group_id', 'IN', $allGroups)
                ->where('active', '=', 1)
                ->where('deleted', '=', 0)
//                ->limit(5)
            ;
            $hcmsItems = $hcmsItemsModel->findAll();
            $prevGroupID = -1;
            $bxPrevGroupID = -1;
            $syncGroups = new Groups($this->_serviceClient, BX_CATALOG_ID, HCMS_SHOP_ID, BX_SECTION_ID);
            foreach($hcmsItems as $key => $hcmsItem) {
                utl::p($key);
                utl::p($hcmsItem->name);
            }
            if( false ) foreach($hcmsItems as $hcmsItem) {
                if(!isset($bxCatalogItems[trim(sprintf($this->_groupTemplate, $this->_projectPrefix, $this->_hcmsShopID, $hcmsItem->ID))])) {
                    $bxGroups = $syncGroups->bxReadGroup(false, $hcmsItem->shop_group_id);
                    if (isset($bxGroups->total)) {
                        if ($bxGroups->total > 0) {
                            $bxPrevGroupID = $bxGroups->result[0]->ID;
                        }
                        $returnes = $this->_bxAddItem($bxPrevGroupID, trim(sprintf($this->_groupTemplate, $this->_projectPrefix, $this->_hcmsShopID, $hcmsItem->id)), $hcmsItem->name, $hcmsItem->price * 1);
                    }
                } else {
                    $bxItem = $bxCatalogItems[trim(sprintf($this->_groupTemplate, $this->_projectPrefix, $this->_hcmsShopID, $hcmsItem->ID))];
                    //-- Редактирование товара по условию --
//                    utl::p($bxItem);
                }
//                utl::p('-'.$hcmsItem->marking.'-');
//                utl::p($hcmsItem->toArray());
//                utl::p($hcmsItem->name.'-'.$hcmsItem->marking.'');
//                if($hcmsItem->shop_group_id!=$prevGroupID) {
//                    $prevGroupID = $hcmsItem->shop_group_id;
//                    $bxGroups = $syncGroups->bxReadGroup(false, $prevGroupID);
//                    if($bxGroups->total>0) {
//                        $bxPrevGroupID = $bxGroups->result[0]->ID;
//                    }
//                    utl::p('-'.$prevGroupID.'-'.$bxPrevGroupID);
//                }
//                utl::p($prevGroupID.'-'.$bxPrevGroupID);
//                usleep(10);
            }
        } else {
            return "Не выбрано ни одной категории";
        }
    }
}