<?php
namespace BX24\Shop;

use BX24\Service;
use Core_Entity;
use HCMS\Shop\Groups as gr;
use HCMS\Utils as utl;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 26.10.2015
 * Time: 22:11
 */

class Groups extends Base
{
    private function _bxAddGroup($bxParentGroup, $bxGroupName) {
        $resp = $this->_serviceClient->restPost('crm.productsection.add', array(
            'fields[CATALOG_ID]' => $this->_catalogID,
            'fields[SECTION_ID]' => $bxParentGroup,
            'fields[NAME]' => $bxGroupName,
        ));
        return $resp;
    }

    private function _bxUpdateGroup($bxGroupID, $bxGroupName) {
        $resp = $this->_serviceClient->restPost('crm.productsection.update', array(
            'id' => $bxGroupID,
            'fields[NAME]' => $bxGroupName,
        ));
        return $resp;
    }

    public function bxReadGroup($parentGroup=0, $filterID=0) {
        $postArrayParams = array(
            'filter[CATALOG_ID]' => $this->_catalogID,
        );
        $filterID*1==0 && $postArrayParams['filter[SECTION_ID]'] = $parentGroup;
        $filterID*1>0 && $postArrayParams['filter[NAME]'] = "%".sprintf($this->_groupTemplate, $this->_projectPrefix, $this->_hcmsShopID, $filterID);
        $resp = $this->_serviceClient->restPost('crm.productsection.list', $postArrayParams);
        return $resp;
    }

    private function _syncHcmsCatalog($topCategories, $parentGroup=0) {
        foreach($topCategories as $topCategory) {
            $bxGroups = $this->bxReadGroup(false, $topCategory['@attributes']['id']);
            $bxSectionID = NULL;
            $newCategoryName = $topCategory['name'].sprintf($this->_groupTemplate, $this->_projectPrefix, $this->_hcmsShopID, $topCategory['@attributes']['id']);
            if(isset($bxGroups->total) && $bxGroups->total==0) {
                $bxNewGroup = $this->_bxAddGroup($parentGroup, $newCategoryName);
                if(is_object($bxNewGroup) && isset($bxNewGroup->error) && $bxNewGroup->error*1==0) {
                    $bxSectionID = $bxNewGroup->result*1;
                }
            } else {
                $bxSectionID = $bxGroups->result[0]->ID;
                if($bxGroups->result[0]->NAME != $newCategoryName) {
                    $this->_bxUpdateGroup($bxSectionID, $newCategoryName);
                }
            }
//            utl::p($bxGroups);
//            usleep(10);
            if(isset($topCategory['category']) && $bxSectionID>0) {
                $this->_syncHcmsCatalog($topCategory['category'], $bxSectionID);
//                return true;
            }
        }
    }

    public function execute($hcmsParentGroup) {
        $hcmsTopGroups = Core_Entity::factory('Shop_Group');
        $hcmsTopGroups
            ->queryBuilder()
            ->where('shop_id', '=', $this->_hcmsShopID)
            ->where('parent_id', '=', $hcmsParentGroup)
            ->where('active', '=', 1)
            ->where('deleted', '=', 0)
//            ->where('name', '=', '_Test')
//            ->limit(2)
        ;
        $groupIDs = array();
        foreach($hcmsTopGroups->findAll() as $group) {
            $groupIDs[] = $group->id;
        }
        if(count($groupIDs)>0) {
            $groups = gr::createInstance();
            $allGroups = $groups->getGroupsTree($groupIDs);
            $syncResult = $this->_syncHcmsCatalog($allGroups['category'], $this->_bxParentGroup);
//            utl::p($syncResult);
        } else {
            return "Не выбрано ни одной категории";
        }
    }
}