<?php
namespace BX24\Shop;

use BX24\Service;
use Core_Entity;
use HCMS\Shop\Groups as gr;
use HCMS\Utils as utl;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 26.10.2015
 * Time: 22:11
 */

class Base
{
    protected $_serviceClient = NULL;
    protected $_catalogID = NULL;
    protected $_hcmsShopID = NULL;
    protected $_projectPrefix = NULL;
    protected $_bxParentGroup = NULL;
    protected $_groupTemplate = " (%s_%s_%s)";

    /**
     * Groups constructor.
     */
    public function __construct($serviceClient, $catalogID, $shopID, $bxParentGroup)
    {
        $this->_serviceClient = $serviceClient;
        $this->_catalogID = $catalogID;
        $this->_hcmsShopID = $shopID;
        $this->_bxParentGroup = $bxParentGroup;
        $this->_projectPrefix = $serviceClient->getProjectPrefix();
    }
}