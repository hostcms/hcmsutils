<?php

namespace BX24;

use Guzzle\Http as restClient;
use HCMS\Utils as utl;
//use Core_Entity;
//use Core_File;
//use Core_Image;
//use Core_QueryBuilder;
//use Core_Str;

class Service {
    protected $_PHPSESSID='';
    protected $_rest=NULL;
    protected $_bx24domain='';
    protected $_appClientID='';
    protected $_appRedirectURL='';
    protected $_token='';
    protected $_authorized;
    protected $_project_prefix='';

    /**
     * @return boolean
     */
    public function isAuthorized()
    {
        return $this->_authorized;
    }

    /**
     * @return string
     */
    public function getProjectPrefix()
    {
        return $this->_project_prefix;
    }

    /**
     * Service constructor.
     */
    public function __construct($bx24domain, $appClientID, $appRedirectURL, $phpSessID='', $project_prefix='')
    {
        $this->_authorized = false;
        $this->_PHPSESSID = $phpSessID;
        $this->_bx24domain = $bx24domain;
        $this->_appClientID = $appClientID;
        $this->_appRedirectURL = $appRedirectURL;
        $this->_project_prefix = $project_prefix;

        $this->_rest = new restClient\Client($bx24domain);
        $this->getToken(false);
    }

    public function getToken($cached=TRUE) {
        if($cached && $this->_token!='') {
            return $this->_token;
        } else {
            try{
                $res = $this->_rest->get("http://{$this->_bx24domain}/oauth/authorize/", array(),
                    array( 'query' => array(
                        'client_id' => $this->_appClientID,
                        'response_type' => 'code',
                        'redirect_uri' => $this->_appRedirectURL,
                    )
                    )
                );
                $res->addCookie('PHPSESSID', $this->_PHPSESSID);
                $res->send();
                $resp = $res->getResponse();
                $response = json_decode($resp->getBody(true));
                if(json_last_error()==0) {
                    $this->_authorized = true;
                    $this->_token = $response->token;
                    return $response->token;
                } else {
                    return 'Пользователь не авторизован';
                }
            } catch( \Exception $e ) {
                return $e->getMessage();
            }
        }
    }

    public function restPost($requestName, $queryParams=array()) {
//        $postParameters = array(
//
//        );
        $queryString = ''; //implode('&', $queryParams);
        foreach($queryParams as $queryParamKey=>$queryParam) {
            $queryString.='&'.$queryParamKey.'='.$queryParam;
        }
//        (trim($queryString)!='') && $queryString = '&'.$queryString;
        utl::p($queryString);
        $res=$this->_rest->post("https://dr4site.bitrix24.ru/rest/{$requestName}.json?auth={$this->_token}{$queryString}");
        $returnes = new \stdClass();
        try {
            $send=$res->send();
            if($send->getStatusCode()*1==200) {
                $returnes = json_decode($res->getResponse()->getBody(true));
                if(json_last_error()==0) {
                    $returnes->error = 0;
                } else {
                    $returnes->error = -1;
                }
//                utl::p($returnes);
                usleep(20);
            } else {
                $returnes->message = 'Неверный код ответа';
                $returnes->error = -2;
            }
        } catch (\Exception $e ) {
            $returnes->message = $e->getMessage();
            $returnes->error = -3;
        }
        return $returnes;
    }
}